import * as React from 'react';
import { PostJobContainer }
  from '@app/src/views/postingJob/indexPost';
import { EditJobContainer }
  from '@app/src/views/postingJob/indexEdit';

const NotFound:React.SFC<{}> = () => <div>Not found</div>;

const Routes = [
  {
    path: '/empresa/publica-aviso-web',
    exact: true,
    component: PostJobContainer,
  },
  {
    path: '/empresa/publica-aviso-web/edit/id/:id',
    exact: true,
    component: EditJobContainer,
  },
  {
    path: '/empresa/publica-aviso-web/extiende/id/:id',
    exact: true,
    component: EditJobContainer,
  },
  {
    path: '*',
    component: NotFound,
  },
];

export { Routes };
