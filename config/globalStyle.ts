import styledNormalize from 'styled-normalize';
import { injectGlobal } from 'styled-components';
import { styledDefault, styledFonts } from 'aptitus-components/Utils/theme/default.style';
import 'aptitus-components/Assets/styles/02_elements/fonts.scss';
import 'aptitus-components/Assets/styles/common.scss';

const globalStyle = () => injectGlobal`
  ${styledNormalize}
  ${styledDefault}
`;

export { globalStyle };
