export const validateSection = ({ schema, nodes, isValid }) => {
  if (!nodes.length) return false;
  const objValidate = {};
  for (const node of nodes) {
    if (!node) continue;
    const nodeType = node.nodeName;
    const nodeName = (node.name || node.id);
    let value = '';

    if (nodeType === 'SELECT') {
      value = node.options[node.selectedIndex].value;
    } else if (nodeType === 'INPUT') {
      value = node.value;
    }

    let nodeAlias = nodeName.split('.');
    nodeAlias = nodeAlias[nodeAlias.length - 1];
    objValidate[nodeAlias] = value;
  }
  schema.isValid(objValidate)
    .then((valid) => {
      isValid && isValid(valid, objValidate);
    });
};
