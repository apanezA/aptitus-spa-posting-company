import * as React from 'react';
import './index.scss';
import { Button } from 'aptitus-components/Components/Button';
import { ThemeProvider } from '@app/config/theme/config';
import { theme } from '@app/config/theme/themes';

interface IModalProps {
  closeModal(): void;
  redirect(): void;
}

export const ModalBack: React.SFC<IModalProps> = (props) => {
  return (
    <section className="b-modal-back">
      <div className="b-modal_header-back">
        <p>¿Está seguro que desea salir de esta página?</p>
      </div>
      <div className="b-modal_body-back">
        <Button
          id="btnModalLogin"
          text="Cancelar"
          type="submit"
          background="#807f7f"
          onClick={props.closeModal} />
        <Button
          id="btnModalLogin"
          text="Aceptar"
          type="submit"
          primary
          onClick={props.redirect} />
      </div>
    </section>
  );
};
