import * as React from 'react';
import { mount } from 'enzyme';
import { IconTooltip } from './index';

describe('Testing IconTooltip', () => {
  const props = {
    text: 'Texto de muestra',
    colorIcon: '#fff',
    nameIcon: 'question_circle',
  };

  const wrapper = mount(<IconTooltip {...props} />);
  test('Testing Props IconTooltip', () => {
    expect(wrapper.props()).toEqual({
      text: 'Texto de muestra',
      colorIcon: '#fff',
      nameIcon: 'question_circle',
    });
  });

  test('Render children', () => {
    props['children'] = <div id="messageChildren">Mensaje</div>;
    const wrapper = mount(<IconTooltip {...props} />);
    const element = wrapper.find('#messageChildren');
    expect(element.length).toBe(1);
  });
});
