import * as React from 'react';
import { WrapperIconTooltip, TooltipText } from './styled';
import { Tooltip } from 'aptitus-components/Components/Tooltip';
import { Icon } from 'aptitus-components/Components/Icon';

interface Props {
  text: string | JSX.Element;
  colorIcon: string;
  nameIcon: string;
  children?: JSX.Element;
}

export const IconTooltip: React.SFC<Props> = ({ text, colorIcon, nameIcon, children }) => (
  <WrapperIconTooltip>
    <Tooltip
      html={(
        <TooltipText>{text}</TooltipText>
      )} >
      <Icon
        data-name={nameIcon}
        width="13"
        height="13"
        fill={colorIcon}/>
    </Tooltip>
    { children }
  </WrapperIconTooltip>
);
