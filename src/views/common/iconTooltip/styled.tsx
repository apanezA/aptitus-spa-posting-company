import styled from 'styled-components';

export const WrapperIconTooltip = styled.span`
  align-items: center;
  display: flex;
`;

export const TooltipText = styled.span`
  font-size: 11px;
`;
