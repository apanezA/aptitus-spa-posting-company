import * as React from 'react';
import './index.scss';

interface ICounterWordsProps {
  maxCharacters: number;
  counterAlign : string;
}

interface ICounterWordsState {
  currentCharacters: number;
}

export class CountWords extends React.Component<ICounterWordsProps, ICounterWordsState> {
  public state : ICounterWordsState;
  maxCharacters: number;
  counterAlign: string;

  constructor(props:ICounterWordsProps) {
    super(props);
    this.maxCharacters = this.props.maxCharacters;
    this.counterAlign = this.props.counterAlign;
    this.state = {
      currentCharacters: this.maxCharacters,
    };
  }

  counter(countCharactersField: number) {
    let maxCharacters, leftCharacters;
    maxCharacters = this.maxCharacters;
    leftCharacters = maxCharacters - countCharactersField;
    this.setState({ currentCharacters: leftCharacters });
    return this.state.currentCharacters;
  }

  template() {
    return <small className="g-counting">Quedan {this.state.currentCharacters} caracteres</small>;
  }

  render() {
    const tmp = this.template();
    return tmp;
  }
}
