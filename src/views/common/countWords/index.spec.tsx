import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { CountWords } from './index';

describe('The CountWords component', () => {
  const props = {
    maxCharacters: 5,
    counterAlign : '',
  };

  describe('Render snapshot', () => {
    it('Render correctly', () => {
      const wrapper = shallow(<CountWords {...props}/>);
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  describe('Shallow test', () => {
    it('Render a <CountWords /> component with a prop maxCharacters', () => {
      const wrapper = mount(<CountWords {...props} maxCharacters={40}/>);
      expect(wrapper.props().maxCharacters).toBe(40);
    });

    it('Call to function value', () => {
      const wrapper = shallow(<CountWords {...props}/>);
    });
  });
});
