import * as React from 'react';
import { Formik } from 'formik';
import { Loader } from 'aptitus-components/Components/Loader';
import { Button } from 'aptitus-components/Components/Button';
import { Modal } from 'aptitus-components/Components/Modal';
import { FormPosting } from '@app/src/views/postingJob/formPosting/components/showPosting';
import { Notify } from 'aptitus-components/Components/Notify';
import { PostingData } from '@app/src/views/postingJob/formPosting/models/postingData';
import { refCropit } from '@app/src/views/postingJob/basicInformation/containers/uploadImage';
import './index.scss';

interface IPostingFormikContainerProps {
  changeSchema(event: string): void;
  typeService: string;
  saveForm(typeService: string, values: object): void;
  typeSchema: object;
  initialDataEdit?: object;
  textModal: string;
  textButton: string;
  updateTextModal(): void;
  showForm: boolean;
  userData: object;
  formData: object;
  showProductSelect: boolean;
  displayButtonDraft: boolean;
  setNotifyRef(notify: any): void;
  redirectCloseDraft(): void;
}

export class PostingFormikContainer extends React.Component<IPostingFormikContainerProps, any> {
  draftModal: boolean;
  modalLoading: any;

  constructor(props: IPostingFormikContainerProps) {
    super(props);
    this.draftModal = false;
    this.showModal = this.showModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  showModal() {
    this.modalLoading.open();
  }

  closeModal() {
    this.modalLoading.close();
  }

  cleanValues(formValues) {
    const values = { ...formValues };
    values.areaId = parseInt(values.areaId, 10);
    values.levelId = parseInt(values.levelId, 10);
    values.contractId = parseInt(values.contractId, 10);
    values.isDisabled = values.isDisabled.toString() === 'true';
    values.salaryIsNegotiable = values.salaryIsNegotiable.toString() === 'true';
    if (!values.salaryMin && !values.salaryMax) {
      values.salaryIsNegotiable = true;
    }
    values.notifyIsApply = values.notifyIsApply.toString() === 'true';
    values.notifySaveEmail = values.notifySaveEmail.toString() === 'true';
    values.programs = values.programs;
    values.programsExcluding = values.programsExcluding;
    return values;
  }

  onSubmit = (formValues) => {
    this.showModal();
    this.props.updateTextModal();
    const values = this.cleanValues(formValues);
    const formData = new PostingData(
      values['productId'],
      values['title'],
      values['description'],
      values['typeCompany'],
      values['isDisabled'],
      values['levelId'],
      values['areaId'],
      values['contractId'],
      values['salaryMin'],
      values['salaryMax'],
      values['salaryIsNegotiable'],
      values['companyId'],
      values['notifyEmail'],
      values['notifyIsApply'],
      values['notifySaveEmail'],
      values['questions'],
      values['locationId'],
      values['locationDisplay'],
      values['companyDisplayType'],
      values['companyLogo'],
      values['alterLogoCoords'],
      values['alterLogoZoom'],
      values['alterName'],
      values['alterLogo'],
      values['creatorUserId'],
      values['isFinalized'],
      values['genderValue'],
      values['genderIsExcluding'],
      values['ageMin'],
      values['ageMax'],
      values['ageIsExcluding'],
      values['residenceId'],
      values['residenceIsExcluding'],
      values['experienceId'],
      values['experienceMin'],
      values['experienceArea'],
      values['experienceIsExcluding'],
      values['studies'],
      values['languages'],
      values['programs'],
      values['programsExcluding'],
    );

    if (formData['companyDisplayType'] === 'other') {
      if (refCropit && refCropit.current.state.mustBeUploaded) {
        refCropit.current.getImage(
          (response: object, dataCropit: { coordinates: number[], scale: number }) => {
            formData['alterLogoCoords'] = dataCropit.coordinates.join(',');
            formData['alterLogoZoom'] = dataCropit.scale;
            formData['alterLogo'] = response['path'];
            this.props.saveForm(this.props.typeService, formData.exportToBack());
          },
        );
      } else {
        this.props.saveForm(this.props.typeService, formData.exportToBack());
      }
    } else {
      this.props.saveForm(this.props.typeService, formData.exportToBack());
    }
  }

  render() {
    return (
      <div className="u-center-box">
        {this.props.showForm ? <Formik
          initialValues={this.props.userData}
          onSubmit={this.onSubmit}
          validationSchema={this.props.typeSchema}
          render={props => <FormPosting
            {...props}
            isFinalized={this.props.userData['isFinalized']}
            formData={this.props.formData}
            textButton={this.props.textButton}
            changeSchema={this.props.changeSchema}
            displayButtonDraft={this.props.displayButtonDraft} />}
          /> : <Loader height={200} />
        }
        <Modal ref={(input) => { this.modalLoading = input; }}
          onCloseModal={this.props.redirectCloseDraft}>
          <div className="b-modal-publish">
            <div className="b-modal-publish_text">{this.props.textModal}</div>
            <Button
              background="#807f7f"
              text="Cerrar"
              onClick={this.closeModal} />
          </div>
        </Modal>
        <Notify ref={(notify) => { this.props.setNotifyRef(notify); }} />
      </div>
    );
  }
}
