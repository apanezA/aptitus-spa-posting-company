import * as React from 'react';
import { schemaValidation }
  from '@app/src/views/postingJob/formPosting/state/formPostingValidator';
import { jobOfferService } from '@app/src/views/postingJob/formFooter/state/services';
import { formPostingService } from '@app/src/views/postingJob/formPosting/state/services';
import { PostingData } from '@app/src/views/postingJob/formPosting/models/postingData';

interface IFormPostingState {
  typeSchema: object;
  textModal: string;
  showForm: boolean;
  formData: object;
  userData: object;
  jobDraf: boolean;
  textButton: string;
  jobId: number;
}

export const EditJobHOC = (WrappedComponent) => {
  return class PP extends React.Component {
    typeService: string;
    textLoading: string;
    TEXT_ERROR: string;
    notifyRef: any;
    public state: IFormPostingState;

    constructor(props) {
      super(props);
      this.state = {
        typeSchema: schemaValidation,
        textModal: '',
        showForm: false,
        formData: {},
        userData: {},
        jobDraf: false,
        textButton: '',
        jobId: null,
      };
      this.changeSchema = this.changeSchema.bind(this);
      this.saveForm = this.saveForm.bind(this);
      this.updateTextModal = this.updateTextModal.bind(this);
      this.setNotifyRef = this.setNotifyRef.bind(this);
      this.TEXT_ERROR = 'Ocurrió un error en el envio';
    }

    emailArrayToObject(emails: string[]): object[] {
      return emails.map((email, index) => {
        return {
          id: index,
          value: email,
        };
      });
    }

    setLanguageDefault(languageData, levels) {
      let languages = [...languageData];
      languages = languages.map((language) => {
        const newLanguage = { ...language };
        newLanguage['languageId'] = language['language_id'];
        return newLanguage;
      });
      const languageDefault = {
        id: null,
        languageId: '',
        level: levels[0]['value'],
        required: false,
      };
      languages.push(languageDefault);
      return languages;
    }

    setProgramsIsExcluding(data) {
      return !!data.length ? data[0].required : false;
    }

    setProductId(userData: object, productTypes) {
      let productId = userData['product_id'];
      const searchResult = productTypes.filter((productType) => {
        return productType['value'] === productId;
      });
      if (userData['is_finalized'] && !searchResult.length) {
        productId = '';
      }
      return productId;
    }

    getImage(company) {
      let image = '';
      if (company['display_type'] === 'other') {
        image = company['alternative_original_logo'];
      }
      return image;
    }

    componentDidMount() {
      Promise.all([formPostingService.getJobs(), formPostingService.getAds()]).then((values) => {

        const masterData = values[0].data.data;
        const editData = values[1].data;
        editData.company.isStateCompany = masterData.company.isStateCompany;
        // product_id (Ver NewJobHOC)
        const postingData = new PostingData(
          this.setProductId(editData, masterData['product_type']),
          editData.title,
          editData.description,
          editData.type_company,
          editData.is_disabled,
          editData.level_id,
          editData.area_id,
          editData.contract_id,
          editData.salary.min,
          editData.salary.max,
          editData.salary.is_negotiable,
          editData.company.id,
          this.emailArrayToObject(editData.notify_application.email),
          editData.notify_application.is_apply,
          editData.notify_application.save_email,
          editData.questions,
          editData.location_id,
          editData.location_display,
          editData.company.display_type,
          editData.company.logo,
          editData.company.alternative_logo_coordinates,
          editData.company.alternative_logo_zoom,
          editData.company.alternative_name,
          this.getImage(editData.company),
          editData.creator_user_id,
          editData.is_finalized,
          editData.requirements.gender.value,
          editData.requirements.gender.required,
          editData.requirements.age.minimum,
          editData.requirements.age.maximum,
          editData.requirements.age.required,
          editData.requirements.location.value,
          editData.requirements.location.required,
          editData.requirements.experiences.id,
          editData.requirements.experiences.experience_years,
          editData.requirements.experiences.area_id,
          editData.requirements.experiences.required,
          editData.requirements.studies,
          this.setLanguageDefault(
            editData.requirements.languages,
            masterData.requirements.level),
          editData.requirements.programs,
          this.setProgramsIsExcluding(editData.requirements.programs),
        );
        masterData['requirements'].location = editData['requirements'].location.list;
        this.setState({
          formData: masterData,
          userData: postingData,
          showForm: true,
          jobDraf: editData.is_draft,
          jobId: editData.id,
          textButton: (editData.is_finalized || editData.is_draft) ? 'Continuar' : 'Actualizar',
        });
      });
    }

    updateOffer = (formData) => {
      this.setAlternativeLogo(formData);
      jobOfferService.updateAds(formData).then((data) => {
        if (data.data.redirect) {
          window.location.href = data.data.redirect;
        }
      }).catch((error) => {
        this.setState({ typeSchema: schemaValidation, textModal: this.TEXT_ERROR });
      });
    }

    saveOffer(formData) {
      this.setAlternativeLogo(formData);
      jobOfferService.createAds(formData).then((data) => {
        if (data.data.status) {
          window.location.href = data.data.redirect;
        }
      }).catch((error) => {
        this.setState({ typeSchema: schemaValidation, textModal: this.TEXT_ERROR });
      });
    }

    setAlternativeLogo(formData: object) {
      if (formData['company'] && formData['company']['alternative_logo']) {
        const urlImage: string = formData['company']['alternative_logo'];
        formData['company']['alternative_logo'] = this.getImageFromUrl(urlImage);
      }
    }

    getImageFromUrl(urlImage: string): string {
      const sectionUrl: string[] = urlImage.split('/');
      return sectionUrl[sectionUrl.length - 1];
    }

    searchWordUrl(url, search) {
      const urlArray = url.split('/');
      for (const index in urlArray) {
        if (urlArray[index] === search) {
          return true;
        }
      }
      return false;
    }

    clearTemporalElements = (formData , typeRequerimients) => {
      const formDataClear = formData.requirements[typeRequerimients];
      const dataClear = formDataClear.filter((item) => {
        return item.type !== 'temp';
      });
      return dataClear;
    }

    convertDataToBack = (data, callback) => {
      const array = data.map((item) => {
        return callback(item);
      });
      return array;
    }

    saveForm = (typeService: string, formData: object) => {
      if (this.state.jobDraf) {
        const updateFormData = { ...formData };
        updateFormData['job_id'] = this.state.jobId;
        this.saveOffer(updateFormData);
      } else {
        this.updateOffer(formData);
      }
    }

    removeQuestionsTemp(questions: object[]) {
      return questions.filter((question) => {
        return question['mode'] !== 'temp';
      });
    }

    changeSchema(event: string) {
      this.textLoading = 'Publicando aviso...';
    }

    updateTextModal() {
      this.setState({ textModal: this.textLoading });
    }

    setNotifyRef(notify) {
      this.notifyRef = notify;
    }

    render() {
      const props = {
        changeSchema: this.changeSchema,
        typeService: this.typeService,
        saveForm: this.saveForm,
        typeSchema: this.state.typeSchema,
        textModal: this.state.textModal,
        updateTextModal: this.updateTextModal,
        showForm: this.state.showForm,
        textButton: this.state.textButton,
        setNotifyRef: this.setNotifyRef,
        userData: this.state.userData,
        formData: this.state.formData,
        displayButtonDraft: false,
      };
      return (
        <WrappedComponent {...props} />
      );
    }
  };
};
