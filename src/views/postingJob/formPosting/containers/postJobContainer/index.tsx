import * as React from 'react';
import { Utils } from 'aptitus-components/Utils';
import { schemaValidation }
  from '@app/src/views/postingJob/formPosting/state/formPostingValidator';
import { schemaValidDraft }
  from '@app/src/views/postingJob/formPosting/state/formDraftValidator';
import { jobOfferService } from '@app/src/views/postingJob/formFooter/state/services';
import { formPostingService } from '@app/src/views/postingJob/formPosting/state/services';
import { PostingData } from '@app/src/views/postingJob/formPosting/models/postingData';

interface IFormPostingState {
  typeSchema: object;
  textModal: string;
  showForm: boolean;
  formData: object;
  userData: object;
}

export const NewJobHOC = (WrappedComponent) => {
  return class PP extends React.Component {
    typeService: string;
    notifyRef: any;
    isSaveDraftOK: boolean;
    TEXT_ERROR: string;
    textLoading: string;
    public state: IFormPostingState;

    constructor(props) {
      super(props);
      this.changeSchema = this.changeSchema.bind(this);
      this.updateTextModal = this.updateTextModal.bind(this);
      this.saveForm = this.saveForm.bind(this);
      this.setNotifyRef = this.setNotifyRef.bind(this);
      this.redirectCloseDraft = this.redirectCloseDraft.bind(this);
      this.state = {
        typeSchema: schemaValidation,
        textModal: '',
        showForm: false,
        formData: {},
        userData: {},
      };
      this.isSaveDraftOK = false;
      this.TEXT_ERROR = 'Ocurrió un error en el envio';
    }

    getProductId() {
      const url = window.location.search;
      const id = Utils.getQueryParameterByName('product_type', url);
      return id ? id : '';
    }

    emailArrayToObject(emails: string[]): object[] {
      return emails.map((email, index) => {
        return {
          id: index,
          value: email,
        };
      });
    }

    getLanguageDefault(levels) {
      return [{
        languageId: '',
        level: levels[0]['value'],
        required: false,
      }];
    }

    componentDidMount() {
      formPostingService.getJobs().then((response) => {
        const data = response.data.data;
        const initialValues: any = {
          product_id: this.getProductId(),
          company_id: data.company.id,
          location_id: data.company.location_id,
          location_display: data.company.location_display,
          notify_email: this.emailArrayToObject(data.company.emailToNotifyApplication),
          creator_user_id: 132, // Siempre es 132?
          title: '',
          description: '',
          is_disabled: false,
          level_id: '',
          area_id: '',
          contract_id: '',
          salary_min: '',
          salary_max: '',
          salary_is_negotiable: false,
          company_display_type: 'real',
          company_logo: '',
          alter_name: '',
          alter_logo: '',
          alter_logo_coords: '',
          alter_logo_zoom: 1,
          notify_is_apply: false,
          notify_save_email: false,
          is_finalized: true,
          questions: [],
          genderValue: '',
          genderIsExcluding: false,
          ageMin: '',
          ageMax: '',
          ageIsExcluding: false,
          residence_id: null,
          residence_is_excluding: false,
          experienceId: null,
          experienceMin: '',
          experienceArea: '',
          experienceIsExcluding: false,
          studies: null,
          languages: this.getLanguageDefault(data.requirements.level),
          programs: null,
          programsIsExcluding: false,
          type_company: data.company.isStateCompany,
        };
        const initialFormValues = new PostingData(
          initialValues.product_id,
          initialValues.title,
          initialValues.description,
          initialValues.type_company,
          initialValues.is_disabled,
          initialValues.level_id,
          initialValues.area_id,
          initialValues.contract_id,
          initialValues.salary_min,
          initialValues.salary_max,
          initialValues.salary_is_negotiable,
          initialValues.company_id,
          initialValues.notify_email,
          initialValues.notify_is_apply,
          initialValues.notify_save_email,
          initialValues.questions,
          initialValues.location_id,
          initialValues.location_display,
          initialValues.company_display_type,
          initialValues.company_logo,
          initialValues.alter_logo_coords,
          initialValues.alter_logo_zoom,
          initialValues.alter_name,
          initialValues.alter_logo,
          initialValues.creator_user_id,
          initialValues.is_finalized,
          initialValues.genderValue,
          initialValues.genderIsExcluding,
          initialValues.ageMin,
          initialValues.ageMax,
          initialValues.ageIsExcluding,
          initialValues.residence_id,
          initialValues.residence_is_excluding,
          initialValues.experienceId,
          initialValues.experienceMin,
          initialValues.experienceArea,
          initialValues.experienceIsExcluding,
          initialValues.studies,
          initialValues.languages,
          initialValues.programs,
          initialValues.programsIsExcluding,
        );
        this.setState({
          formData: data,
          userData: initialFormValues,
          showForm: true,
        });
      });
    }

    saveOffer = (formData) => {
      jobOfferService.createAds(formData).then((data) => {
        if (data.data.status) {
          window.location.href = data.data.redirect;
        }
        this.setState({ typeSchema: schemaValidation, textModal: data.data.message });
      }).catch((error) => {
        this.setState({ typeSchema: schemaValidation, textModal: this.TEXT_ERROR });
      });
    }

    saveDraft = (formData) => {
      const questionsNoTemp = formData.questions.filter((item) => {
        return item.mode !== 'temp';
      });
      formData.questions = questionsNoTemp;
      jobOfferService.createDraft(formData).then((data) => {
        if (data.data.status) {
          this.isSaveDraftOK = true;
        }
        this.setState({ typeSchema: schemaValidation, textModal: data.data.message });
      }).catch((error) => {
        this.setState({ typeSchema: schemaValidation, textModal: this.TEXT_ERROR });
      });
    }

    clearTemporalElements = (formData , typeRequerimients) => {
      const formDataClear = formData.requirements[typeRequerimients];
      const dataClear = formDataClear.filter((item) => {
        return item.type !== 'temp';
      });
      return dataClear;
    }

    convertDataToBack = (data, callback) => {
      const array = data.map((item) => {
        return callback(item);
      });
      return array;
    }

    saveForm = (typeService: string, values: object) => {
      // formData.company.logo = '';
      // formData.product_id = parseInt(values['productId'], 10);
      if (typeService === 'schemaAdd') {
        this.saveOffer(values);
      } else {
        this.saveDraft(values);
      }
    }

    changeSchema(event: string) {
      this.typeService = event;
      switch (event) {
        case 'schemaDraft': {
          this.setState({ typeSchema: schemaValidDraft });
          this.textLoading = 'Guardando en Borrador...';
          break;
        }
        case 'schemaAdd': {
          this.setState({ typeSchema: schemaValidation });
          this.textLoading = 'Enviando datos ...';
          break;
        }
      }
    }

    updateTextModal() {
      this.setState({ textModal: this.textLoading });
    }

    setNotifyRef(notify) {
      this.notifyRef = notify;
    }

    redirectCloseDraft() {
      if (this.isSaveDraftOK) {
        window.location.href = '/empresa/mis-procesos';
      }
    }

    render() {
      const props = {
        changeSchema: this.changeSchema,
        typeService: this.typeService,
        saveForm: this.saveForm,
        typeSchema: this.state.typeSchema,
        textModal: this.state.textModal,
        updateTextModal: this.updateTextModal,
        showForm: this.state.showForm,
        textButton: 'Continuar',
        setNotifyRef: this.setNotifyRef,
        redirectCloseDraft: this.redirectCloseDraft,
        userData: this.state.userData,
        formData: this.state.formData,
        displayButtonDraft: true,
      };
      return (
        <WrappedComponent {...props} />
      );
    }
  };
};
