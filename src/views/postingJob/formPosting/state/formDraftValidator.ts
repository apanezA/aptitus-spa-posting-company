import { ERROR_MESSAGES } from '@app/src/views/postingJob/formPosting/state/constants';
import * as Yup from 'yup';

// TODO: agregar esta validacion para el WYSIWYG
// description: new EditorState.createEmpty(),

const schema = Yup.object().shape({
  productId: Yup.number()
    .required(ERROR_MESSAGES.required)
    .nullable(false),
  title: Yup.string()
    .min(2, ERROR_MESSAGES.min(2))
    .required(ERROR_MESSAGES.required)
    .test('title', ERROR_MESSAGES.title, (value) => {
      const finalAmount = 3;
      if (typeof value === 'undefined') {
        return false;
      }
      const countScript = value.split('-').length;
      const countSlash = value.split('/').length;
      if (countScript < finalAmount && countSlash < finalAmount) {
        return true;
      }
    }),
});

export const schemaValidDraft = schema;
