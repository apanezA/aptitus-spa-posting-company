import * as React from 'react';

export const PostingContext = React.createContext({});

interface IPostingProviderProps {
  data: {
    values: object,
    formData: object,
    displayButtonDraft: boolean,
    setFieldValue(schema: string, value: any, isShouldValidate: boolean): void;
    handleChange(e: any): void;
    handleBlur(e: any): void;
    touched: object;
    errors: object;
    isSubmitting: boolean;
  };
}

interface IPostingProviderState {
  userData: object;
  formData: object;
  listExperiencesArea: object[];
  displayButtonDraft: boolean;
  formik: {
    setFieldValue(schema: string, value: any, isShouldValidate: boolean): void;
    handleChange(e: any): void;
    handleBlur(e: any): void;
    touched: object;
    errors: object;
    isSubmitting: boolean;
  };
}

export class PostingProvider extends
  React.Component<IPostingProviderProps, IPostingProviderState > {
  constructor(props) {
    super(props);
    this.state = {
      userData: this.props.data.values,
      formData: this.props.data.formData,
      listExperiencesArea: this.props.data.formData['area'],
      displayButtonDraft: this.props.data.displayButtonDraft,
      formik: {
        setFieldValue: this.props.data.setFieldValue,
        handleChange: this.props.data.handleChange,
        handleBlur: this.props.data.handleBlur,
        touched: this.props.data.touched,
        errors: this.props.data.errors,
        isSubmitting: this.props.data.isSubmitting,
      },
    };
  }

  setUserData = (userData: object) => {
    this.setState({ userData });
  }

  setAreaExperiences = (area) => {
    const newAreas = [];
    const areas = [...this.state.formData['area']];
    if (!isNaN(area.value)) newAreas.push(area);
    areas.map((item) => {
      if (item['value'] !== area.value) newAreas.push(item);
    });
    this.setState({ listExperiencesArea: newAreas });
  }

  render() {
    const store = {
      store: this.state,
      reducers: {
        setAreaExperiences: this.setAreaExperiences,
        setUserData: this.setUserData,
      },
    };
    return (
      <PostingContext.Provider value={store}>
        {this.props.children}
      </PostingContext.Provider>
    );
  }
}

export function withPostingContext(Component) {
  return function PostingContextComponent(props) {
    return(
      <PostingContext.Consumer>
        {store => <Component {...props} store={store['store']} reducers={store['reducers']}/>}
      </PostingContext.Consumer>
    );
  };
}
