import { Utils } from 'aptitus-components/Utils';
import axios from 'axios';

const idPost = Utils.getUrlByAlias(window.location.href, 'id');

export const formPostingService = {
  async getJobs() {
    try {
      const request = await axios
        .get(`${process.env.APP_URL}/empresa/publica-aviso-web/master-data-ajax`);
      return request;
    } catch (e) {
      throw new Error();
    }
  },

  async getAds() {
    try {
      const { data } = await axios
        .get(`${process.env.APP_URL}/empresa/publica-aviso-web/jobs/${idPost}/`);
      return data;
    } catch (e) {
      throw new Error();
    }
  },

  cancelRequest() {
    return axios.CancelToken.source();
  },
};
