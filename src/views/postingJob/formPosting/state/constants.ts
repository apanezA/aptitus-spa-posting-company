export const ERROR_MESSAGES = {
  required: 'Este campo es obligatorio',
  email: 'Ingrese un correo válido',
  alpha: 'Solo letras',
  description: 'No se permiten enlaces en la descripción',
  min: (num: number) => {
    const characters = num < 2 ? 'carácter' : 'caracteres';
    return `Ingrese como mínimo ${num} ${characters}`;
  },
  max: (num: number) => {
    const characters = num < 2 ? 'carácter' : 'caracteres';
    return `Ingrese como máximo ${num} ${characters}`;
  },
  minAge: (num: number) => {
    const ages = num < 2 ? 'año' : 'años';
    return `Mínimo ${num} ${ages}`;
  },
  maxAge: (num: number) => {
    const ages = num < 2 ? 'año' : 'años';
    return `Máximo ${num} ${ages}`;
  },
  minFields: (num: number) => {
    const fields = num < 2 ? 'campo' : 'campos';
    return `Ingrese como mínimo ${num} ${fields}`;
  },
  multiple: 'Seleccione una de las opciones',
  salary: 'Salario inválido',
  location: 'Ingrese una ubicación válida',
  title: 'Este campo solo admite un "/" y un "-"',
  alphanumeric: 'Este campo solo acepta alfanumérico',
  letterCase: 'Este campo no admite que el texto sea solo en mayúsculas',
  common: 'Ingrese un campo válido',
  url: 'Esta no es una url válida',
  alter_name:'No se permite ingresar solo espacios en blanco',
  age_min: 'Mínima 14',
  age_max: 'Máximo 75',
  age: 'Edad Incorrecta',
};

export const REGEX_VALIDATION = {
  string: '^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$',
  number: '^[0-9]+$',
  alphanumeric: '^[0-9a-zA-ZñÑáéíóúÁÉÍÓÚ ]+$',
  title: '^[0-9a-zA-ZñÑáéíóúÁÉÍÓÚ/\\-, ]+$',
  description: '(?:https?:\/\/)?(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]' +
  '{2,256}\\.[a-z]{2,4}\\b(?:[-a-zA-Z0-9@:%_\\+.~#?&//=]*)',
};

export const SELECT_TYPE_GRADE = {
  ID_SECONDARY: '3',
  ID_PRIMARY: '2',
  ID_WITHOUT_STUDY: '',
};

export const SELECT_TYPE_CAREER = {
  ID_OTHER: 15,
};
