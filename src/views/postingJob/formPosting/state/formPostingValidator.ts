import { ERROR_MESSAGES, REGEX_VALIDATION, SELECT_TYPE_GRADE }
  from '@app/src/views/postingJob/formPosting/state/constants';
import * as Yup from 'yup';

// TODO: agregar esta validacion para el WYSIWYG
// description: new EditorState.createEmpty(),
export const validateDescription = (value) => {
  const MIN = 0;
  if (typeof value === 'undefined') {
    return false;
  }
  if (value.length > MIN) {
    const regex = new RegExp(REGEX_VALIDATION.description, 'g');
    const afterRegex = value.match(regex);
    if (afterRegex !== null) {
      if (afterRegex.length > MIN) {
        return false;
      }
    }
    return true;
  }
};

export const schemaLanguages = Yup.object({
  languageId: Yup.string(),
  level: Yup.string()
    .when(['languageId'], (value, schema) => {
      return value !== '' ? schema.required(ERROR_MESSAGES.required) : schema.notRequired();
    }),
  required: Yup.boolean(),
});

export const schemaValidation = Yup.object().shape({
  productId: Yup.number()
    .required(ERROR_MESSAGES.required)
    .nullable(false),
  title: Yup.string()
    .required(ERROR_MESSAGES.required)
    .matches(new RegExp(REGEX_VALIDATION.title), ERROR_MESSAGES.alphanumeric)
    .test('title', ERROR_MESSAGES.title, (value) => {
      const finalAmount = 3;
      if (typeof value === 'undefined') {
        return false;
      }
      const countScript = value.split('-').length;
      const countSlash = value.split('/').length;
      if (countScript < finalAmount && countSlash < finalAmount) {
        return true;
      }
    })
    .test('letterCase', ERROR_MESSAGES.letterCase, (value) => {
      if (typeof value === 'undefined' || value === value.toUpperCase()) {
        return false;
      }
      if (value !== value.toUpperCase()) {
        return true;
      }
    }),
  isDisabled: Yup.boolean(),
  typeCompany: Yup.boolean(),
  description: Yup.string()
    .min(2, ERROR_MESSAGES.min(2))
    .required(ERROR_MESSAGES.required)
    .when('typeCompany', (value, schema) => {
      if (!value) {
        return schema.test('description', ERROR_MESSAGES.description, (value) => {
          return validateDescription(value);
        });
      }
    }),
  levelId: Yup.number()
    .required(ERROR_MESSAGES.required)
    .nullable(false),
  areaId: Yup.number()
    .required(ERROR_MESSAGES.required)
    .nullable(false),
  contractId: Yup.number()
    .required(ERROR_MESSAGES.required)
    .nullable(false),
  salaryMax: Yup.number().nullable(true)
    .when(['salaryMin'], (min, schema) => {
      if (min) {
        return schema.min(min, ERROR_MESSAGES.salary);
      }
    })
    .when(['salaryIsNegotiable'], (value, schema) => {
      if (value) {
        return schema
          .notRequired();
      }
    }),
  salaryMin: Yup.number().nullable(true)
    .when(['salaryIsNegotiable'], (value, schema) => {
      if (value) {
        return schema
          .notRequired();
      }
    }),
  salaryIsNegotiable: Yup.boolean(),
  companyDisplayType: Yup.string().
    required(ERROR_MESSAGES.required),
  alterName: Yup.string()
    .when(['companyDisplayType'], (displayType, schema) => {
      if (displayType === 'other') {
        return schema
          .matches(new RegExp(REGEX_VALIDATION.alphanumeric), ERROR_MESSAGES.alphanumeric)
          .required(ERROR_MESSAGES.required)
          .min(2, ERROR_MESSAGES.min(2))
          .max(30, ERROR_MESSAGES.max(30))
          .test('alterName', ERROR_MESSAGES.alter_name, (value) => {
            if (/^\s+$/.test(value)) {
              return false;
            }
            return true;
          });
      }
    }),
  alterLogo: Yup.string()
    .when(['companyDisplayType'], (displayType, schema) => {
      if (displayType === 'other') {
        return schema.required(ERROR_MESSAGES.required);
      }
    }),
  alterLogoCoords: Yup.string(),
  notifyIsApply: Yup.boolean(),
  notifyEmail: Yup.array()
    .when(['notifyIsApply'], (displayType, schema) => {
      if (displayType === true || displayType === 'true') {
        return schema
          .of(
            Yup.object().shape({
              id: Yup.number(),
              value: Yup.string()
                .email(ERROR_MESSAGES.email)
                .required(ERROR_MESSAGES.required),
            }),
          )
          .required(ERROR_MESSAGES.required);
      }
    }),
  notifySaveEmail: Yup.boolean(),
  questions: Yup.array()
    .of(
      Yup.object().shape({
        type: Yup.string(),
        name: Yup.string()
          .required(ERROR_MESSAGES.required)
          .test('alter_options', 'Debe haber mínimo 2 respuestas', function (value) {
            return (this.parent.choices && this.parent.choices.length < 2) ? false : true;
          }),
        mode: Yup.string()
        .test('no-temp', 'Debe guardar la pregunta', value =>
          (value !== 'edit' && value !== 'temp'),
        ),
        choices: Yup.lazy((value) => {
          if (value !== undefined) {
            return Yup.array()
            .of(
              Yup.object().shape({
                value: Yup.string().required(ERROR_MESSAGES.required),
                checked: Yup.boolean(),
              }),
            );
          }
          return Yup.mixed().notRequired();
        }),
      }),
  ),
  locationId: Yup.number()
    .required(ERROR_MESSAGES.required)
    .moreThan(1000, ERROR_MESSAGES.location)
    .nullable(false),
  experienceMin: Yup.string().nullable(true)
    .test('fieldMin', ERROR_MESSAGES.minAge(1), (value) => {
      return (Number(value) <= 0) ? false : true;
    })
    .test('fieldMin', ERROR_MESSAGES.maxAge(60), (value) => {
      return (Number(value) > 60) ? false : true;
    }),
  experienceArea: Yup.string().nullable(true)
    .when(['experienceMin'], (experienceMin, schema) => {
      return (experienceMin === null || typeof experienceMin === 'undefined') ?
      schema.notRequired() : schema.required(ERROR_MESSAGES.required);
    }),
  languages: Yup.array()
    .of(schemaLanguages),
  programs: Yup.array(),
  creatorUserId: Yup.number(),
  ageMin: Yup.string()
    .test('ageMin Minimun ', ERROR_MESSAGES.age_min, (value) => {
      const ageMin = Number(value);
      return (ageMin === 0 || ageMin < 14) ? false : true;
    })
    .test('ageMin Maximun', ERROR_MESSAGES.age_max, (value) => {
      const ageMin = Number(value);
      return (ageMin > 75) ? false : true;
    }),
  ageMax: Yup.string()
    .test('ageMax Maximun', ERROR_MESSAGES.age_max, (value) => {
      const ageMax = Number(value);
      return (ageMax === 0 || (ageMax > 75)) ? false : true;
    })
    .test('ageMax Minimun', ERROR_MESSAGES.age_min, (value) => {
      const ageMax = Number(value);
      return (ageMax < 14) ? false : true;
    })
    .test('ageMax compare ageMin', ERROR_MESSAGES.age, function (value) {
      const ageMin = Number(this.parent.ageMin);
      const ageMax = Number(value);
      return (ageMin >= ageMax) ? false : true;
    }),
  studies: Yup.array()
    .of(
      Yup.object().shape({
        gradeId: Yup.string(),
        careerTypes: Yup.array()
          .test('careerTypes', 'Campo requerido', function (value) {
            return (this.parent.gradeId !== String(SELECT_TYPE_GRADE.ID_SECONDARY) &&
                    this.parent.gradeId !== String(SELECT_TYPE_GRADE.ID_PRIMARY) &&
                    this.parent.gradeId &&
                    !value.length) ? false : true;
          }),
      }),
    ),
});
