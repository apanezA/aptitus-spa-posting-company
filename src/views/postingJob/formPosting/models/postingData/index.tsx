import { SELECT_TYPE_GRADE } from '@app/src/views/postingJob/formPosting/state/constants';
import { savedLanguages } from
  '@app/src/views/postingJob/requeriments/containers/RequerimentLanguages';

export class PostingData {

  constructor(
    private productId: string,
    private title: string,
    private description: string,
    private typeCompany: boolean,
    private isDisabled: boolean,
    private levelId: number,
    private areaId: number,
    private contractId: number,
    private salaryMin: number,
    private salaryMax: number,
    private salaryIsNegotiable: boolean,
    private companyId: number,
    private notifyEmail: object[],
    private notifyIsApply: boolean,
    private notifySaveEmail: boolean,
    private questions: object[],
    private locationId: number,
    private locationDisplay: string,
    private companyDisplayType: string,
    private companyLogo: any,
    private alterLogoCoords: string,
    private alterLogoZoom: any,
    private alterName: any,
    private alterLogo: any,
    private creatorUserId: number,
    private isFinalized: boolean,
    private genderValue: string,
    private genderIsExcluding:  boolean,
    private ageMin: string,
    private ageMax: string,
    private ageIsExcluding: boolean,
    private residenceId: number,
    private residenceIsExcluding: boolean,
    private experienceId: number,
    private experienceMin: string,
    private experienceArea: number,
    private experienceIsExcluding: boolean,
    private studies: object[],
    private languages: object[],
    private programs: object[],
    private programsIsExcluding: boolean,
  ) {
    this.productId = productId;
    this.title = title;
    this.description = description;
    this.typeCompany = typeCompany;
    this.isDisabled = isDisabled;
    this.levelId = levelId;
    this.areaId = areaId;
    this.contractId = contractId;
    this.salaryMin = salaryMin;
    this.salaryMax = salaryMax;
    this.salaryIsNegotiable = salaryIsNegotiable;
    this.companyId = companyId;
    this.notifyEmail = notifyEmail;
    this.notifyIsApply = notifyIsApply;
    this.notifySaveEmail = notifySaveEmail;
    this.questions = questions;
    this.locationId = locationId;
    this.locationDisplay = locationDisplay;
    this.companyDisplayType = companyDisplayType;
    this.companyLogo = companyLogo;
    this.alterLogoCoords = alterLogoCoords;
    this.alterLogoZoom = parseFloat(alterLogoZoom);
    this.alterName = alterName;
    this.alterLogo = alterLogo;
    this.creatorUserId = creatorUserId;
    this.isFinalized = isFinalized;
    this.genderValue = genderValue;
    this.genderIsExcluding = genderIsExcluding;
    this.ageMin = ageMin;
    this.ageMax = ageMax;
    this.ageIsExcluding = ageIsExcluding;
    this.residenceId = Number(residenceId);
    this.residenceIsExcluding = residenceIsExcluding;
    this.experienceId = experienceId;
    this.experienceMin = experienceMin;
    this.experienceArea = experienceArea;
    this.experienceIsExcluding = experienceIsExcluding;
    this.studies = this.convertToFrontStudies(studies);
    this.languages = languages;
    this.programs = this.convertToFrontPrograms(programs);
  }

  exportToBack() {
    return {
      product_id: Number(this.productId),
      title: this.title,
      description: this.description,
      is_disabled: this.isDisabled,
      level_id: this.levelId,
      area_id: this.areaId,
      contract_id: this.contractId,
      salary: {
        min: this.converToSalary(this.salaryMin),
        max: this.converToSalary(this.salaryMax),
        is_negotiable: this.salaryIsNegotiable,
      },
      company: {
        id: this.companyId,
        display_type: this.companyDisplayType,
        logo: this.companyLogo,
        alternative_name: this.alterName,
        alternative_logo: this.alterLogo,
        alternative_logo_coordinates: this.alterLogoCoords,
        alternative_logo_zoom: this.alterLogoZoom,
      },
      notify_application: {
        is_apply: this.notifyIsApply,
        email: this.notifyEmail,
        save_email: this.notifySaveEmail,
      },
      is_finalized: this.isFinalized,
      questions: this.questions,
      location_id: this.locationId,
      creator_user_id: this.creatorUserId,
      requirements: {
        gender: this.hasGender(this.genderValue, this.genderIsExcluding),
        age: this.hasAge(this.ageMin, this.ageMax, this.ageIsExcluding),
        location: this.hasResidence(this.residenceId, this.residenceIsExcluding),
        experiences: this.hasExperiences(
          this.experienceId,
          Number(this.experienceMin),
          Number(this.experienceArea),
          this.experienceIsExcluding,
        ),
        studies: this.hasStudies(this.studies),
        languages: this.hasLanguagues(savedLanguages, this.languages),
        programs: this.hasPrograms(this.programs),
      },
    };
  }

  converToSalary(data) {
    const dataNumber = (data === '' || data === null) ? null : Number(data);
    return dataNumber;
  }

  convertToFrontExperiences(data) {
    const newData = data.map((item) => {
      return {
        experienceId: item.id,
        experienceMin: String(item.experience_years),
        experienceArea: String(item.area_id),
        experienceIsExcluding: item.required,
      };
    });
    const dataDefault = [{
      experienceId: null,
      experienceMin: '',
      experienceArea: 0,
      experienceIsExcluding: false,
    }];
    return data.length ? newData : dataDefault;
  }

  convertToFrontStudies(data) {
    let newData;
    if (data !== null) {
      newData = data.map((item) => {
        return {
          gradeId: item.grade_id || item.gradeId,
          careerTypes: (!!item.career_types || !!item.careerTypes)
            ? this.careerTypes(item.career_types, item.careerTypes) : [],
          required: item.required,
        };
      });
    } else {
      newData = [{
        gradeId: '',
        careerTypes: [],
        required: false,
      }];
    }
    return newData;
  }

  careerTypes(dataBack, dataFront) {
    let newData;
    newData = dataBack ? dataBack : dataFront;
    return newData.map((item) => {
      return {
        id: item.id,
        value: item.career_type_id || item.value,
      };
    });
  }

  convertToFrontPrograms(data) {
    const newdata = (data !== null) ? data : [];
    return newdata.map((item) => {
      return {
        id: item.id,
        value: item.program_id || item.value,
        label: item.label,
        required: item.required,
      };
    });
  }

  hasGender(gender: string, required: boolean) {
    const genderExport = { gender, required };
    return gender !== '' ? genderExport : null;
  }

  hasAge(min: string , max: string, ageIsExcluding: boolean) {
    const EMPTY = 0;
    const ageMin = Number(min) !== 0 ? Number(min) : null;
    const ageMax = Number(max) !== 0 ? Number(max) : null;
    const ageExport = {
      minimum: ageMin,
      maximum: ageMax,
      required: ageIsExcluding,
    };
    return (ageMin > EMPTY || ageMax > EMPTY) ? ageExport : null;
  }

  hasResidence(id, isExcluding) {
    const EMPTY = 0;
    const residenceExport = {
      location_id: id,
      required: isExcluding,
    };
    return id !== EMPTY ? residenceExport : null;
  }

  hasExperiences(id: number, min: number, area: number, required: boolean) {
    const yearMin = min === 0 ? null : min;
    const experienceExport = {
      id,
      required,
      area_id: area,
      experience_years: yearMin,
    };
    return (yearMin !== null || (!isNaN(area) && area > 1)) ? experienceExport : null;
  }

  hasStudies(data) {
    const studiesExport = data.map((item) => {
      const object = {
        grade_id: Number(item['gradeId']),
        career_types: this.setCareerTypes(item['careerTypes'], item['gradeId']),
        required: item['required'],
      };
      return item['gradeId'] ? object : null;
    });
    return studiesExport.includes(null) ? null : studiesExport;
  }

  setCareerTypes(data: object[], grade: string) {
    const buildCareers = data.map((item) => {
      return {
        id: item['id'],
        career_type_id: Number(item['value']),
      };
    });
    return (grade === SELECT_TYPE_GRADE.ID_PRIMARY ||
      grade === SELECT_TYPE_GRADE.ID_SECONDARY || !data.length) ? null : buildCareers;
  }

  hasLanguagues(languages, formikLanguage) {
    let newLanguages = languages.map((language) => {
      return {
        id: language['id'],
        language_id: language['languageId'],
        level: language['level'],
        required: language['required'],
      };
    });
    if (newLanguages.length === 1 && newLanguages[0].language_id === '') {
      newLanguages = null;
    }
    const lastIndex = formikLanguage.length - 1;
    const lastLanguage = formikLanguage[lastIndex];
    if (lastLanguage['languageId'].length && lastLanguage['level']) {
      newLanguages.push({
        id: lastLanguage['id'],
        language_id: lastLanguage['languageId'],
        level: lastLanguage['level'],
        required: lastLanguage['required'],
      });
    }
    return newLanguages;
  }

  hasPrograms(programs: object[]) {
    const newPrograms = programs.map((item) => {
      return {
        id: item['id'],
        label: item['label'],
        required: item['required'],
        program_id: item['value'],
      };
    });
    return newPrograms.length ? newPrograms : null;
  }
}
