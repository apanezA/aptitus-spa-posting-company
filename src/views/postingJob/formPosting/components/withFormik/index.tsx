import * as React from 'react';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';

interface IFormik {
  errors: any;
  touched: any;
  schema: string;
  fieldSchema: string;
  setFieldValue(field: string, value: any, shouldValidate?: boolean): void;
  handleChange(event): void;
  handleBlur(e: any): void;
  [propName: string]: any;
}

export interface IPropsHOC {
  hasErrorClass?(index?: number);
  hasErrorElement?(index?: number);
  updateSchemaValue?(value: any, shouldValidate?: boolean): void;
  updateErrorsChange?(event): void;
  updateErrorsBlur?(e: any): void;
}

export function withFormik(WrappedComponent) {
  return class extends React.Component<IFormik, any> {
    refField: any;
    constructor(props) {
      super(props);
      this.hasErrorClass = this.hasErrorClass.bind(this);
      this.hasErrorElement = this.hasErrorElement.bind(this);
      this.updateSchemaValue = this.updateSchemaValue.bind(this);
      this.updateErrorsChange = this.updateErrorsChange.bind(this);
      this.updateErrorsBlur = this.updateErrorsBlur.bind(this);
    }

    getInnerRef() {
      return this.refField;
    }

    hasError(index) {
      const errorField = this.props.errors;
      let errorValue = false;
      if (errorField) {
        if (errorField[index]) {
          errorValue = errorField[index][this.props.fieldSchema];
        } else {
          errorValue = errorField[this.props.fieldSchema];
        }
      }
      return errorValue;
    }

    hasTouched(index) {
      const touchedField = this.props.touched;
      let touchedValue = false;
      if (touchedField) {
        if (touchedField[index]) {
          touchedValue = touchedField[index][this.props.fieldSchema];
        } else {
          touchedValue = touchedField[this.props.fieldSchema];
        }
      }
      return touchedField;
    }

    hasErrorClass(index?): string {
      if (this.hasError(index) && this.hasTouched(index)) {
        return 'is-error';
      }
      return '';
    }

    hasErrorElement(index?): JSX.Element {
      return (
        <TooltipErrorForm error={this.hasError(index)} touched={this.hasTouched(index)}
                          isSubmitting={this.props.isSubmitting} />
      );
    }

    updateSchemaValue(value: any, shouldValidate?: boolean) {
      this.props.setFieldValue(this.props.schema, value, shouldValidate || false);
    }

    updateErrorsChange(event) {
      this.props.handleChange(event);
    }

    updateErrorsBlur(event) {
      this.props.handleBlur(event);
    }

    render() {
      return <WrappedComponent
        {...this.props}
        ref={ref => this.refField = ref}
        hasErrorClass={this.hasErrorClass}
        hasErrorElement={this.hasErrorElement}
        updateSchemaValue={this.updateSchemaValue}
        updateErrorsChange={this.updateErrorsChange}
        updateErrorsBlur={this.updateErrorsBlur}
      />;
    }
  };
}
