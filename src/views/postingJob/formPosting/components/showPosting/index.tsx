import * as React from 'react';
import { Field } from 'formik';
import { FormFooter } from '@app/src/views/postingJob/formFooter';
import { FormHeader } from '@app/src/views/postingJob/formHeader';
import '@app/src/views/postingJob/formPosting/index.scss';
import { SectionLocation } from '@app/src/views/postingJob/locationJob';
import { SectionQuestionsContainer }
  from '@app/src/views/postingJob/questionsJob';
import { SectionBasicInformationContainer }
  from '@app/src/views/postingJob/basicInformation';
import { SectionRequirements } from '@app/src/views/postingJob/requeriments';
import { PostingProvider } from '@app/src/views/postingJob/formPosting/state/store';
import { Steps } from 'aptitus-components/Components/Steps';
import { Utils } from 'aptitus-components/Utils';
import { TypeProductJob }
  from '@app/src/views/postingJob/formPosting/components/typeProductJob';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';

export const FormPosting: React.SFC<any> = (props) => {
  onsubmit = (event) => {
    props.handleSubmit(event);
    Utils.validateItemFocus();
  };
  const titles = ['Tipo de aviso', 'Datos del aviso', 'Confirma y destaca'];
  return (
    <PostingProvider data={props}>
      <FormHeader title="Datos de tu aviso" >
        <Steps titles={titles} active={2}/>
      </FormHeader>
      <form id="formPublicOffer" method="post" onSubmit={this.onsubmit} noValidate>
        { props.isFinalized &&
          <Field
            name="productId"
            render={({ field, form }) => (
              <TypeProductJob
                name={field.name}
                onBlur={field.onBlur}
                onChange={field.onChange}
                value={field.value}
                isError={form.errors[field.name] && form.touched[field.name]}
                data={props.formData.product_type}
                errorMessage={
                  <TooltipErrorForm error={form.errors[field.name]}
                    touched={form.touched[field.name]}
                    value={field.value}
                    isSubmitting={form.isSubmitting} />
                }
              />
            )}
          />
        }
        <div className="b-process-info">
          <SectionBasicInformationContainer
            formData={props.formData}
            values={props.values}
          />
        </div>
        <div className="b-process-info">
          <SectionLocation values={props.values} />
        </div>
        <div className="b-process-info"><SectionRequirements {...props} /></div>
        <div className="b-process-info"><SectionQuestionsContainer {...props} /></div>
        <FormFooter
          changeSchema={props.changeSchema}
          textButton={props.textButton}
          publishBtnProps={props.publishBtnProps}
          displayButtonDraft={props.displayButtonDraft}
          isSubmitting={props.isSubmitting}
          isValid={props.isValid}
        />
      </form>
    </PostingProvider>
  );
};
