import * as React from 'react';
import { Form } from 'aptitus-components/Components/Form';
import { SelectForm } from 'aptitus-components/Components/SelectForm';

interface Props {
  name: string;
  value: any;
  onBlur?(e: any): void;
  onChange?(e: any): void;
  isError?: boolean;
  data: object[];
  errorMessage: JSX.Element;
}

export class TypeProductJob extends React.Component<Props, any> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <Form.Wrapper type="normal">
        <Form.Row className="b-process-product-type">
          <Form.Column
            className="b-process-product-type_column"
            isError={this.props.isError}>
            <h2 className="b-process-product-type_title b-process-product-type_title--label">
              Tipo del aviso
            </h2>
            <div className="b-process-product-type_input">
              <SelectForm
                id={this.props.name}
                name={this.props.name}
                defaultText="Seleccionar"
                value={this.props.value}
                onBlur={this.props.onBlur}
                onChange={this.props.onChange}
                className={this.props.isError && 'is-error'}
                data={this.props.data} />
              {this.props.errorMessage}
            </div>
          </Form.Column>
        </Form.Row>
      </Form.Wrapper>
    );
  }
}
