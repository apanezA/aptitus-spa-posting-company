import * as React from 'react';
import { mount } from 'enzyme';
import { withFormik, FieldArray } from 'formik';
import { FieldItem } from './fieldItem';
import { FieldContainer } from './index';

describe('FieldContainer component test', () => {

  const defaultProps = {
    itemsList: [{
      id: 123,
      value: '123',
    }],
    maxLength: 200,
    maxFields: 5,
    titleField: 'Email',
    fieldSchema: null,
  };

  const InnerForm = props => (
    <form>
      <FieldArray
        name="notifyEmail"
        render={arrayHelpers => (
          <FieldContainer
            {...defaultProps}
            arrayHelpers={arrayHelpers}
          />
        )}
      />
      <button type="submit">Submit</button>
    </form>
  );

  const MyForm = withFormik({
    mapPropsToValues: props => ({
      notifyEmail: [],
    }),
    handleSubmit: (values) => {},
  })(InnerForm);

  describe('Functional tests', () => {

    let wrapper = mount(<MyForm/>);

    beforeEach(() => wrapper = mount(<MyForm/>));

    it('Correct rendering', () => {
      const actual = wrapper.find(FieldItem).length;
      expect(actual).toBe(1);
    });

  });

  describe('Unit tests', () => {

    let wrapper = mount(<MyForm/>);

    beforeEach(() => wrapper = mount(<MyForm/>));

    describe('Method onAddItem test', () => {

      it('Should add a new element with the empty value property ', () => {
        const wrapperFieldContainer = wrapper.find(FieldContainer).instance();
        wrapperFieldContainer.onAddItem();
        const newNotifyEmail = wrapperFieldContainer.props.arrayHelpers.form.values.notifyEmail;
        expect(Array.isArray(newNotifyEmail)).toBeTruthy();
        expect(newNotifyEmail.length).toBeGreaterThan(0);
        newNotifyEmail.map((item) => {
          expect(typeof item.id).toBe('number');
          expect(item.value).toBe('');
        });
      });

    });

    describe('Method onRemoveItem test', () => {

      it('Should remove an element from the array', () => {
        const wrapperFieldContainer = wrapper.find(FieldContainer).instance();
        wrapperFieldContainer.onAddItem();
        const newNotifyEmail = wrapperFieldContainer.props.arrayHelpers.form.values.notifyEmail;
        const sizeArrayNotify = newNotifyEmail.length;
        wrapperFieldContainer.onRemoveItem();
        const updatedNotifyEmail = wrapperFieldContainer.props.arrayHelpers.form.values.notifyEmail;
        expect(Array.isArray(newNotifyEmail)).toBeTruthy();
        expect(updatedNotifyEmail.length).toBe(sizeArrayNotify - 1);
      });

    });

  });

});
