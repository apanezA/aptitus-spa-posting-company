import * as React from 'react';
import { FieldItem }
  from '@app/src/views/postingJob/basicInformation/containers/fieldContainer/fieldItem';

interface Props {
  itemsList: {
    id: number,
    value: string,
    checked?: boolean,
  }[];
  maxFields: number;
  titleField: string;
  fieldSchema: string;
  maxLength?: number;
  arrayHelpers: object;
}

export class FieldContainer extends React.Component<Props, {}> {

  private fieldSchema: any = this.props.fieldSchema;

  onAddItem = () => {
    this.props.arrayHelpers['push']({ id: Date.now(), value: '' });
  }

  onRemoveItem = (id: number) => {
    let indexToDelete: number = 0;
    this.props.itemsList.map((element, index) => {
      element.id === id && (indexToDelete = index);
    });
    this.props.arrayHelpers['remove'](indexToDelete);
  }

  render(): JSX.Element[] {
    const { titleField, maxFields, itemsList } = this.props;

    return itemsList.map((item, index) => {
      const notLastChild = (index + 1 === itemsList.length && index + 1 < maxFields);
      const onlyChild = itemsList.length === 1;

      return (
        <FieldItem
          key={index}
          index={index}
          item={item}
          isNotLastChild={notLastChild}
          isOnlyChild={onlyChild}
          fieldSchema={this.fieldSchema}
          titleField={titleField}
          maxLength={this.props.maxLength}
          onAddItem={this.onAddItem}
          onRemoveItem={this.onRemoveItem}
        />
      );
    });
  }
}
