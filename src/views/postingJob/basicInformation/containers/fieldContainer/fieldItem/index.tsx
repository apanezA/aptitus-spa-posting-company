import * as React from 'react';
import { Button } from 'aptitus-components/Components/Button';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { Field, getIn } from 'formik';
import './index.scss';

interface Props {
  item: {
    id: number,
    value: string,
    checked?: boolean,
  };
  isNotLastChild: boolean;
  isOnlyChild: boolean;
  index: number;
  fieldSchema: string;
  titleField: string;
  extraActions?: object | null;
  maxLength?: number;
  onAddItem(): void;
  onRemoveItem(id: number): void;
}

export class FieldItem extends React.Component<Props, {}> {

  onAdd = () => {
    this.props.onAddItem();
  }

  onRemove = () => {
    this.props.onRemoveItem(this.props.item.id);
  }

  printClassError(form, field) {
    const isError = getIn(form.errors, field.name);
    const isTouched = getIn(form.touched, field.name);
    return isError && isTouched && 'is-error';
  }

  render() {
    const { item, fieldSchema, index, titleField, maxLength } = this.props;

    return (
      <div className="b-controls-fields">
        <label className="g-form_label" htmlFor={item.id && item.id.toString()}>
          <span className={'b-controls-fields_text'}>
            {titleField} {index + 1}:
          </span>
        </label>
        <div className="g-form_control">
          <Field
            name={`${fieldSchema}.${index}.value`}
            render={({ field, form }) => (
              <div className="b-controls-fields_input">
                <input type="text" className={this.printClassError(form, field)}
                  maxLength={maxLength} {...field}/>
                <TooltipErrorForm error={getIn(form.errors, field.name)}
                  touched={getIn(form.touched, field.name)}
                  isSubmitting={form.isSubmitting} />
              </div>
            )}
          />
          <div className="b-controls-bar">
            {this.props.isNotLastChild && <Button
              icon={{ name: 'plus-2', direction: 'left' , height:'12' }}
              onClick={this.onAdd}
              radial={true} secondary width="25px" height="25px"/>}&nbsp;
            {!this.props.isOnlyChild && <Button
              icon={{ name: 'delete2', direction: 'left', height:'15' }}
              onClick={this.onRemove}
              radial={true} background="#807f7f" width="25px" height="25px"/>}
          </div>
        </div>
      </div>
    );
  }
}
