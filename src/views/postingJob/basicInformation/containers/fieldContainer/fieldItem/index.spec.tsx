import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { withFormik } from 'formik';
import { FieldItem } from './index';

describe('FieldItem component test', () => {

  const defaultProps = {
    item: {
      id: 123,
      value: 'test@orbis.com.pe',
    },
    isNotLastChild: true,
    isOnlyChild: false,
    index: 0,
    fieldSchema: 'notifyEmail',
    titleField: 'Email',
    maxLength: 200,
    onAddItem: jest.fn(() => {}),
    onRemoveItem: jest.fn((id: number) => {}),
  };

  const InnerForm = props => (
    <form>
      <FieldItem {...defaultProps}/>
    </form>
  );

  const MyForm = withFormik({
    mapPropsToValues: props => ({
      notifyEmail: [{
        id: 123,
        value: 'test@orbis.com.pe',
      }],
    }),
    handleSubmit: (values) => {},
    validate: (values) => {
      const errors = {};
      if (values['notifyEmail'].length) {
        const notifyEmail = values['notifyEmail'].filter((item) => {
          // tslint:disable-next-line:max-line-length
          const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          const withMatch = regexEmail.test(String(item.value).toLowerCase());
          return !withMatch && {
            value: 'Ingrese un campo válido',
          };
        });
        notifyEmail.length && (errors['notifyEmail'] = notifyEmail);
      }
      return errors;
    },
  })(InnerForm);

  afterEach(() => {
    defaultProps.onAddItem.mockClear();
    defaultProps.onRemoveItem.mockClear();
  });

  describe('Functional tests', () => {

    let wrapper = mount(<MyForm/>);

    beforeEach(() => wrapper = mount(<MyForm/>));

    it('Correct rendering', () => {
      const actual = wrapper.find(FieldItem).length;
      expect(actual).toBe(1);
    });

    it('Should call the property onAddItem when you click on the add button', () => {
      const addButton = wrapper.find('button').first();
      addButton.simulate('click');
      expect(defaultProps.onAddItem).toBeCalled();
    });

    it('Should call the property onRemoveItem when you click on the delete button', () => {
      const deleteButton = wrapper.find('button').last();
      deleteButton.simulate('click');
      expect(defaultProps.onRemoveItem).toBeCalled();
    });

    it('Should allow the entry of a valid email ', () => {
      const invalidEmails = [
        'test*gmail.com', 'test', 'test.com', '*gmail.com', '*gmail.c', '#$%%&(/)=.com',
      ];
      invalidEmails.map((invalidEmail) => {
        const event = { target: { name: 'notifyEmail.0.value', value: invalidEmail } };
        const inputEmail = wrapper.find('input[name="notifyEmail.0.value"]');
        inputEmail.simulate('change', event);
        inputEmail.simulate('blur', event);
        const updatedInputEmail = wrapper.find('input[name="notifyEmail.0.value"]');
        const isError = updatedInputEmail.hasClass('is-error');
        expect(isError).toBeTruthy();
      });
    });

  });

  describe('Unit tests', () => {

    let wrapper = null;
    let instance = null;

    beforeEach(() => {
      wrapper = shallow(<FieldItem {...defaultProps}/>);
      instance = wrapper.instance();
    });

    describe('Method onAdd test', () => {

      it('Should call the onAddItem property when the function is executed', () => {
        instance.onAdd();
        expect(defaultProps.onAddItem).toBeCalled();
      });

    });

    describe('Method onRemove test', () => {

      it('Should call the onRemoveItem property when the function is executed', () => {
        instance.onRemove();
        expect(defaultProps.onRemoveItem).toBeCalled();
      });

    });

    describe('Method printClassError test', () => {

      it('Should be no error if the email is valid', () => {
        const form = {
          errors: {},
          touched: {},
        };
        const field = { name: 'notifyEmail' };
        const isError = instance.printClassError(form, field);
        expect(isError).toBe(undefined);
      });

      it('Should be an error if the email is invalid', () => {
        const form = {
          errors: {
            notifyEmail: 'Email incorrect',
          },
          touched: {
            notifyEmail: true,
          },
        };
        const field = { name: 'notifyEmail' };
        const isError = instance.printClassError(form, field);
        expect(isError).toBe('is-error');
      });

    });

  });

});
