import * as React from 'react';
import { mount } from 'enzyme';
import { withFormik } from 'formik';
import { JobNamePostingContainer } from './index';
import { REGEX_VALIDATION } from '@app/src/views/postingJob/formPosting/state/constants';
import { JobNamePosting }
  from '@app/src/views/postingJob/basicInformation/components/jobNamePosting';

describe('Job name component test', () => {

  const InnerForm = props => (
    <form>
      <JobNamePostingContainer/>
      <button type="submit">Submit</button>
    </form>
  );

  const MyForm = withFormik({
    mapPropsToValues: props => ({
      title: '',
    }),
    handleSubmit: (values) => {},
    validate: (values) => {
      const errors = {};
      const validate = new RegExp(REGEX_VALIDATION.title, 'g');
      if (!values['title']) {
        errors['title'] = 'Requerid';
      } else if (!validate['test'](values['title'])) {
        errors['title'] = 'Tiene caracteres especiales';
      }
      return errors;
    },
  })(InnerForm);

  describe('Functional tests', () => {

    let wrapper = null;

    beforeEach(() => wrapper = mount(<MyForm/>));

    it('Should update value field title', () => {
      const event = { target: { name: 'title', value: 'Programador' } };
      const input = wrapper.find('input[name="title"]');
      const errors = {};
      input.simulate('input', event);
      expect(wrapper.find(InnerForm).props().values.title).toBe('Programador');
      expect(wrapper.find(InnerForm).props().errors).toMatchObject(errors);
    });

    it('Should update value field title with errors on the write', () => {
      const event = { target: { name: 'title', value: 'Analista $oftware' } };
      const input = wrapper.find('input[name="title"]');
      const errors = { title: 'Tiene caracteres especiales' };
      input.simulate('input', event);
      expect(wrapper.find(InnerForm).props().values.title).toBe('Analista $oftware');
      expect(wrapper.find(InnerForm).props().errors).toMatchObject(errors);
    });

    it('Should update value field title when errors is void', () => {
      const event = { target: { name: 'title', value: '' } };
      const input = wrapper.find('input[name="title"]');
      const errors = {};
      input.simulate('input', event);
      expect(wrapper.find(InnerForm).props().values.title).toBe('');
      expect(wrapper.find(InnerForm).props().errors).toMatchObject(errors);
    });

    it('Should there is a component jobNamePosting into parent component test', () => {
      const jobNameComponent = wrapper.find(JobNamePosting).exists();
      expect(jobNameComponent).toBeTruthy();
    });

  });

});
