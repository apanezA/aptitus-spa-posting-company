import * as React from 'react';
import { Field } from 'formik';
import { Form } from 'aptitus-components/Components/Form';
import { serviceJobName } from '@app/src/views/postingJob/basicInformation/state/services';
import { JobNamePosting }
  from '@app/src/views/postingJob/basicInformation/components/jobNamePosting';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';

export class JobNamePostingContainer extends React.Component<{}, {}> {

  constructor(props) {
    super(props);
    this.searchJobName = this.searchJobName.bind(this);
  }

  searchJobName(value): Promise<any> {
    const jobName = {
      name: value,
    };
    return serviceJobName.getJobs(jobName);
  }

  render() {
    return (
      <Field
        name="title"
        render={({ field, form }) => (
          <Form.Column isError={form.errors[field.name] && form.touched[field.name]}>
            <Form.Control title="Nombre del Puesto:" required>
              <JobNamePosting
                name={field.name}
                maxCharacters={70}
                value={field.value}
                isError={form.touched[field.name] && form.errors[field.name]}
                getSuggestionValue={(suggestion) => {
                  form.setFieldValue(field.name, suggestion['value']);
                }}
                customSuggestion="value"
                onBlur={field.onBlur}
                onInput={field.onChange}
                cancelRequest={serviceJobName.cancelRequest}
                getSuggestions={this.searchJobName}
              />
              <TooltipErrorForm error={form.errors[field.name]}
                  touched={form.touched[field.name]}
                  value={field.value}
                  isSubmitting={form.isSubmitting} />
            </Form.Control>
          </Form.Column>
        )}
      />
    );
  }
}
