import * as React from 'react';
import { Field } from 'formik';
import MaskedInput from 'react-text-mask';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import { Checkbox } from 'aptitus-components/Components/Checkbox';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import './index.scss';

interface Props {
  integerLimit: number;
  decimalLimit: number;
}

export class SalaryJobsContainer extends React.Component<Props, {}> {

  private numberMask = this.createNumberMask();

  filterSalaryValue = (event) => {
    const valueParse = event.target.value;
    let valueState = null;
    if (valueParse) {
      valueState = Number(valueParse.replace(',', '', 'gi'));
    }
    return valueState;
  }

  setNewSalaryValues = (form) => {
    const newValues = form.values;
    newValues.salaryMin = '';
    newValues.salaryMax = '';
    form.setValues(newValues);
  }

  createNumberMask() {
    return createNumberMask({
      prefix: '',
      integerLimit: this.props.integerLimit,
      decimalLimit: this.props.decimalLimit,
    });
  }

  render(): JSX.Element {
    return (
      <div className="b-controls-salary">
        <span className="b-controls-salary_label"> entre</span>
        <div className="b-controls-salary_field">
          <Field
            name="salaryMin"
            render={({ field, form }) => (
              <React.Fragment>
                <MaskedInput
                  id={field.name}
                  name={field.name}
                  mask={this.numberMask}
                  guide={false}
                  type="text"
                  disabled={(form.values['salaryIsNegotiable']) ? 'disabled' : ''}
                  value={field.value}
                  onChange={(e) => {
                    e.target.value = this.filterSalaryValue(e);
                    field.onChange(e);
                  }}
                  onBlur={field.onBlur}
                  className={form.errors[field.name] && form.touched[field.name] && 'is-error'}
                />
                <TooltipErrorForm error={form.errors[field.name]}
                  touched={form.touched[field.name]}
                  isSubmitting={form.isSubmitting} />
              </React.Fragment>
            )}
          />
        </div>
        <span className="b-controls-salary_label"> y </span>
        <div className="b-controls-salary_field">
          <Field
            name="salaryMax"
            render={({ field, form }) => (
              <React.Fragment>
                <MaskedInput
                  id={field.name}
                  name={field.name}
                  mask={this.numberMask}
                  guide={false}
                  type="text"
                  disabled={(form.values['salaryIsNegotiable']) ? 'disabled' : ''}
                  value={field.value}
                  onChange={(e) => {
                    e.target.value = this.filterSalaryValue(e);
                    field.onChange(e);
                  }}
                  onBlur={field.onBlur}
                  className={form.errors[field.name] && form.touched[field.name] && 'is-error'}
                />
                <TooltipErrorForm error={form.errors[field.name]}
                  touched={form.touched[field.name]}
                  isSubmitting={form.isSubmitting} />
              </React.Fragment>
            )}
          />
        </div>
        <div className="b-controls-salary_check">
          <Field
            name="salaryIsNegotiable"
            render={({ field, form }) => (
              <Checkbox theme="white"
                id={field.name}
                name={field.name}
                text="A convenir"
                checked={field.value}
                onChange={(e) => {
                  this.setNewSalaryValues(form);
                  field.onChange(e);
                }}
              />
            )}
          />
        </div>
      </div>
    );
  }
}
