import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { withFormik } from 'formik';
import { SalaryJobsContainer } from './index';

describe('Salary component test', () => {

  const InnerForm = props => (
    <form>
      <SalaryJobsContainer integerLimit={6} decimalLimit={3} />
      <button type="submit">Submit</button>
    </form>
  );

  const MyForm = withFormik({
    mapPropsToValues: props => ({
      salaryIsNegotiable: false,
      salaryMin: '1000',
      salaryMax: '3000',
    }),
    handleSubmit: (values) => {},
    validate: (values) => {
      const errors = {};
      values['salaryMax'] < values['salaryMin']
        && (errors['salaryMax'] = 'Salario inválido');
      return errors;
    },
  })(InnerForm);

  describe('Functional tests', () => {

    let wrapper = null;

    beforeEach(() => wrapper = mount(<MyForm/>));

    it('Should update value field salaryMin', () => {
      const event = { target: { name: 'salaryMin', value: '800' } };
      const inputSalaryMin = wrapper.find('input[name="salaryMin"]');
      inputSalaryMin.simulate('change', event);
      const valueSalaryMin = wrapper.find(InnerForm).props().values.salaryMin;
      expect(typeof valueSalaryMin).toBe('number');
      expect(valueSalaryMin).toBe(800);
    });

    it('Should update value field salaryMax', () => {
      const event = { target: { name: 'salaryMax', value: '1500' } };
      const inputSalaryMax = wrapper.find('input[name="salaryMax"]');
      inputSalaryMax.simulate('change', event);
      const valueSalaryMax = wrapper.find(InnerForm).props().values.salaryMax;
      expect(typeof valueSalaryMax).toBe('number');
      expect(valueSalaryMax).toBe(1500);
    });

    it('Should clean salary when I change the <Checkbox />', () => {
      const checkbox = wrapper.find('input[name="salaryIsNegotiable"]');
      checkbox.simulate('change', { target: { name: 'salaryIsNegotiable', value: true } });
      expect(wrapper.find(InnerForm).props().values).toEqual({
        salaryIsNegotiable: true,
        salaryMax: '',
        salaryMin: '',
      });
    });

    it('Should clean the error when it is checked in the checkbox', () => {
      const event = { target: { name: 'salaryMax', value: '80' } };
      const inputSalaryMax = wrapper.find('input[name="salaryMax"]');
      inputSalaryMax.simulate('change', event);
      const checkbox = wrapper.find('input[name="salaryIsNegotiable"]');
      checkbox.simulate('change', { target: { name: 'salaryIsNegotiable', value: true } });
      const errors = wrapper.find(InnerForm).props().errors;
      expect(typeof errors).toBe('object');
      expect(errors['salaryMax']).toBe(undefined);
    });

    it('Should mark error if the minimum wage is greater than the maximum salary', () => {
      const inputSalaryMax = wrapper.find('input[name="salaryMax"]');
      inputSalaryMax.simulate('blur');
      const event = { target: { name: 'salaryMin', value: '4000' } };
      const inputSalaryMin = wrapper.find('input[name="salaryMin"]');
      inputSalaryMin.simulate('change', event);
      inputSalaryMin.simulate('blur');
      const updatedInputSalaryMax = wrapper.find('input[name="salaryMax"]');
      const isError = updatedInputSalaryMax.hasClass('is-error');
      expect(isError).toBeTruthy();
    });

    it('Should mark error if the maximum salary is less than the minimum wage', () => {
      const event = { target: { name: 'salaryMax', value: '80' } };
      const inputSalaryMax = wrapper.find('input[name="salaryMax"]');
      inputSalaryMax.simulate('change', event);
      inputSalaryMax.simulate('blur');
      const updatedInputSalaryMax = wrapper.find('input[name="salaryMax"]');
      const isError = updatedInputSalaryMax.hasClass('is-error');
      expect(isError).toBeTruthy();
    });

  });

  describe('Unit tests', () => {

    let wrapper = null;
    let instance = null;

    beforeEach(() => {
      wrapper = shallow(<SalaryJobsContainer integerLimit={6} decimalLimit={3} />);
      instance = wrapper.instance();
    });

    describe('Method filterSalaryValue test', () => {

      it('Should return null if the value of the field is empty', () => {
        const event = { target: { value: '' } };
        const inputValue = instance.filterSalaryValue(event);
        expect(inputValue).toBe(null);
      });

      it('Should return the same value of the field being of type number', () => {
        const event = { target: { value: '1200' } };
        const inputValue = instance.filterSalaryValue(event);
        expect(typeof inputValue).toBe('number');
        expect(inputValue).toBe(1200);
      });

    });

    describe('Method setNewSalaryValues test', () => {

      it('Should call the setValues ​​function and set the values ​​to empty', () => {
        const form = {
          values: {
            salaryMin: '1200',
            salaryMax: '3000',
          },
          setValues: jest.fn(() => {}),
        };
        instance.setNewSalaryValues(form);
        expect(form.setValues).toBeCalled();
        expect(form.setValues).toBeCalledWith({
          salaryMin: '',
          salaryMax: '',
        });
      });

    });

    describe('Method createNumberMask test', () => {

      it('Should return a function', () => {
        const numberMask = instance.createNumberMask();
        expect(typeof numberMask).toBe('function');
      });

    });

  });

});
