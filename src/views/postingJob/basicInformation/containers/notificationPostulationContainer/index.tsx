import * as React from 'react';
import { Field, FieldArray } from 'formik';
import { Checkbox } from 'aptitus-components/Components/Checkbox';
import { Icon } from 'aptitus-components/Components/Icon';
import { Radio } from 'aptitus-components/Components/Radio';
import { Form } from 'aptitus-components/Components/Form';
import { FieldContainer }
  from '@app/src/views/postingJob/basicInformation/containers/fieldContainer';
import './index.scss';

interface Props {
  itemsList: {
    id: number,
    value: string,
  }[];
  notifyIsApply: boolean;
}

export class NotificationPostulationContainer extends React.Component<Props, {}> {
  emailsDefault: {
    id: number,
    value: string,
  }[];
  SCHEMA: string;

  constructor(props: Props) {
    super(props);
    this.emailsDefault = [...this.props.itemsList];
    this.SCHEMA = 'notifyEmail';
    this.onChangeCheckNotify = this.onChangeCheckNotify.bind(this);
  }

  onChangeCheckNotify(event, form) {
    if (event.target.value === 'true') {
      if (!this.props.itemsList.length) {
        const item = {
          id: 0,
          value: '',
        };
        form.setFieldValue(this.SCHEMA, [item]);
      }
    } else {
      form.setFieldValue(this.SCHEMA, this.emailsDefault);
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="b-controls-notifications">
          <Form.Control title="Notificar postulaciones por email: ">
            <div className="b-controls-notifications_group">
              <div className="b-controls-notifications_options">
                <div className="b-controls-notifications_radio">
                  <Field
                    name="notifyIsApply"
                    render={({ field, form }) => {
                      return (
                        <Radio
                          id="notificationYes"
                          value="true"
                          name={field.name}
                          onChange={(e) => {
                            this.onChangeCheckNotify(e, form);
                            field.onChange(e);
                          }}
                          text="Sí"
                          checked={(true === field.value)} />
                      );
                    }}
                  />
                </div>
                <div className="b-controls-notifications_radio">
                  <Field
                    name="notifyIsApply"
                    render={({ field, form }) => {
                      return (
                        <Radio
                          id="notificationNot"
                          value="false"
                          name={field.name}
                          onChange={(e) => {
                            this.onChangeCheckNotify(e, form);
                            field.onChange(e);
                          }}
                          text="No"
                          checked={(false === field.value)} />
                      );
                    }}
                  />
                </div>
              </div>
              <div className="b-controls-notifications_informations">
                <Icon data-name="information" fill="#f3d044" />
                <span className="b-controls-notifications_informations__text">
                  Se enviará notificaciones por cada postulación
                </span>
              </div>
            </div>
          </Form.Control>
        </div>
        {this.props.notifyIsApply && (
          <React.Fragment>
            <FieldArray
              name={this.SCHEMA}
              render={arrayHelpers => (
                <FieldContainer
                  fieldSchema={this.SCHEMA}
                  itemsList={this.props.itemsList}
                  maxFields={5}
                  maxLength={200}
                  titleField="Email"
                  arrayHelpers={arrayHelpers}
                />
              )}
            />
            <div className="b-controls-saved-emails">
              <Field
                name="notifySaveEmail"
                render={({ field }) => (
                  <Checkbox
                    id="chkEmail"
                    theme="white"
                    name={field.name}
                    text="Guardar los emails ingresados para futuras publicaciones"
                    checked={field.value}
                    onChange={field.onChange}
                  />
                )}
              />
            </div>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}
