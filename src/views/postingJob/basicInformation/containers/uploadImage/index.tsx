import * as React from 'react';
import { serviceJobName } from '@app/src/views/postingJob/basicInformation/state/services';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { Cropit } from 'aptitus-components/Components/Cropit';
import './index.scss';

interface IUploadImageState {
  textButtonImage: string;
}

interface IUploadImageProps {
  name: string;
  touched: boolean;
  errors: string;
  values : { [key: string]: any};
  isSubmitting: boolean;
  handleChange(event): void;
  handleBlur(event): void;
  setFieldError(field: string, message: string): void;
  setFieldTouched(field: string, touched: boolean): void;
}

export let refCropit = null;

export class UploadImage extends React.Component<IUploadImageProps, IUploadImageState> {

  public state: IUploadImageState;
  fileEditor: any;
  fileInput: HTMLInputElement;
  instance: Cropit;
  IMAGE_EXTENSIONS: RegExp;

  constructor(props: IUploadImageProps) {
    super(props);
    this.IMAGE_EXTENSIONS = /(jpeg|jpg|png)/;
    refCropit = React.createRef();
    this.state = {
      textButtonImage: 'Cargar Logo',
    };
  }

  isValidImage = (event) => {
    this.setState({ textButtonImage: 'Cambiar Logo' });
    this.props.handleChange(event);
  }

  isInvalidImage = () => {
    this.props.setFieldError(this.props.name, 'Formato inválido');
    this.props.setFieldTouched(this.props.name, true);
  }

  onUploadService = (file, dataCropit, callback) => {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('coordinate', String(dataCropit.coordinates));
    serviceJobName.uploadFile(formData)
      .then((response) => {
        callback(response['data'], dataCropit);
      });
  }

  render() {
    const { touched, errors, values, name, handleBlur, isSubmitting } = this.props;

    return (
      <div className="b-controls-upload-image">
        <div className="b-controls-upload-image_preview" >
          <Cropit
            ref={refCropit}
            buttonText={this.state.textButtonImage}
            classWrapperImage={touched && errors && 'is-error' || ''}
            color={[186, 186, 186, 1]}
            coords={values['alterLogoCoords']}
            customRange="skyblue"
            defaultImage={`${process.env.PATH_CDS}images/photoEmpDefault.jpg`}
            hasError={(!!!errors && !!!touched)}
            image={values['alterLogo']}
            message="Selecciona un archivo de no más de 5Mb, formatos jpg, jpeg o png"
            nameFile={name}
            onBlur={handleBlur}
            onIsValidImage={this.isValidImage}
            onIsInvalidImage={this.isInvalidImage}
            scale={values['alterLogoZoom']}
            service={this.onUploadService}
            validExtensions={this.IMAGE_EXTENSIONS}
            tooltip={
              <TooltipErrorForm
                error={errors}
                touched={touched}
                isSubmitting={isSubmitting}/>
            }
          />
        </div>
      </div>
    );
  }
}
