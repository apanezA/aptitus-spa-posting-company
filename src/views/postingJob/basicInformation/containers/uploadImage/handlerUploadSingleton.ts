export class HandlerUploadSingleton {
  private static instance: HandlerUploadSingleton = null;
  private uploader;
  private constructor() { }

  public static getInstance() {
    if (!this.instance) {
      this.instance = new HandlerUploadSingleton();
    }
    return this.instance;
  }

  getUploaderFunction() {
    return this.uploader;
  }

  setUploaderFunction(uploader) {
    this.uploader = uploader;
  }
}
