import * as React from 'react';
import { CountWords } from '@app/src/views/common/countWords';
import { Autocomplete } from 'aptitus-components/Components/Autocomplete';
import './index.scss';

interface Props {
  name: string;
  value: string;
  maxCharacters: number;
  isError: boolean;
  getSuggestions(value: object): Promise<any>;
  customSuggestion?: string;
  getSuggestionValue?(suggestion: object): any;
  cancelRequest?(): void;
  onBlur?(e?: any): void;
  onInput?(e?: any): void;
}

export class JobNamePosting extends React.Component<Props, {}> {
  refCountWords: any;

  constructor(props: Props) {
    super(props);
    this.onAfterChange = this.onAfterChange.bind(this);
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
    this.refCountWords = React.createRef();
  }

  componentDidMount() {
    this.updateCounter(this.props.value);
  }

  onAfterChange(value) {
    this.updateCounter(value);
    if (value === '') {
      this.props.cancelRequest && this.props.cancelRequest();
    }
  }

  onSuggestionSelected(suggestion) {
    if (suggestion.value !== 'No se encontraron resultados') {
      this.props.getSuggestionValue(suggestion);
      this.updateCounter(suggestion[this.props.customSuggestion]);
    }
  }

  updateCounter(value) {
    this.refCountWords && this.refCountWords.current.counter(value.length);
  }

  render() {
    const inputProps = {
      maxLength: this.props.maxCharacters,
      id: this.props.name,
      name: this.props.name,
      onBlur: this.props.onBlur,
      onInput: this.props.onInput,
      value: this.props.value,
      className: this.props.isError ? 'is-error' : '',
    };
    return (
      <div className="b-autocomplete">
        <Autocomplete
          properties={inputProps}
          customSuggestion={this.props.customSuggestion}
          onAfterChange={this.onAfterChange}
          getSuggestions={this.props.getSuggestions}
          onSuggestionSelected={this.onSuggestionSelected}
        />
        <div className="b-autocomplete_counting">
          <CountWords ref={this.refCountWords}
            maxCharacters={this.props.maxCharacters}
            counterAlign="left" />
        </div>
      </div>
    );
  }
}
