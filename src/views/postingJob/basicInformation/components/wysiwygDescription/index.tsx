import * as React from 'react';
import { CountWords } from '@app/src/views/common/countWords/';
import { WysiwygCustom } from 'aptitus-components/Components/Wysiwyg';
import './index.scss';

interface Props {
  name: string;
  maxCharactersWysiwyg: number;
  handleBlurWysiwyg?(fieldValue: string, contentField: string);
  isErrorWysiwyg?: boolean;
  contentWysiwyg?: string;
}

export class WysiwygDescription extends React.Component<Props, {}> {
  refCountWords: any;
  constructor(props: Props) {
    super(props);
    this.refCountWords = React.createRef();
    this.onAfterChangeEditor = this.onAfterChangeEditor.bind(this);
  }

  onAfterChangeEditor(length) {
    this.refCountWords.current.counter(length);
  }

  render() {
    return (
      <React.Fragment>
        <WysiwygCustom
          contentWysiwyg={this.props.contentWysiwyg}
          afterEditorChange={this.onAfterChangeEditor}
          maxCharacters={this.props.maxCharactersWysiwyg}
          name={this.props.name}
          onBlur={this.props.handleBlurWysiwyg}
          isError={this.props.isErrorWysiwyg}
        />
        <div className="b-description_counting">
          <CountWords
            ref={this.refCountWords}
            maxCharacters={this.props.maxCharactersWysiwyg}
            counterAlign="left" />
        </div>
      </React.Fragment>
    );
  }
}
