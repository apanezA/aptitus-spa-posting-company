import axios from 'axios';

let cancelToken = null;
let source = null;

export const serviceJobName = {
  async getJobs(values): Promise<any> {
    const url = `${process.env.APP_URL}/empresa/publica-aviso-web/search-job-ajax`;
    try {
      if (source) {
        source.cancel();
      }
      cancelToken = axios.CancelToken;
      source = cancelToken.source();
      const data = {};
      const { id, name } = values;
      const params = {
        company_id: id,
        q: name,
      };

      const response = await axios.get(url, {
        params,
        cancelToken: source.token,
      });
      data['data'] = response.data;
      return data;
    } catch (e) {
      throw new Error();
    }
  },

  async uploadFile(formData) {
    const url = `${process.env.APT_SERVICES_URL}/jobs/upload-file`;
    try {
      const { data } = await axios.post(url, formData);
      return data;
    } catch (e) {
      throw new Error();
    }
  },

  cancelRequest() {
    return source.cancel();
  },
};
