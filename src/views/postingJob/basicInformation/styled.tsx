import styled from 'styled-components';
import { GLOBAL } from 'aptitus-components/Utils/theme/constants';

export const Title = styled.div`
  align-items: center;
  color: ${(props) => {
    return props.theme ? props.theme.first_color : 'black';
  }};
  display: flex;
  font-family: ${GLOBAL.FAMILY_MUSEO};
`;
