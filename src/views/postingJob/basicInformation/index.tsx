import * as React from 'react';
import { Field } from 'formik';
import { Accordion } from 'aptitus-components/Components/Accordion';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { Checkbox } from 'aptitus-components/Components/Checkbox';
import { Form } from 'aptitus-components/Components/Form';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { JobNamePostingContainer }
  from '@app/src/views/postingJob/basicInformation/containers/jobNamePostingContainer';
import { WysiwygDescription }
  from '@app/src/views/postingJob/basicInformation/components/wysiwygDescription';
import { SalaryJobsContainer }
  from '@app/src/views/postingJob/basicInformation/containers/salaryJobsContainer';
import { UploadImage } from '@app/src/views/postingJob/basicInformation/containers/uploadImage';
import { Title } from '@app/src/views/common/styledCommon';
import { withPostingContext } from '@app/src/views/postingJob/formPosting/state/store';
import { NotificationPostulationContainer } from
  '@app/src/views/postingJob/basicInformation/containers/notificationPostulationContainer';
import './index.scss';

export const SectionBasicInformation: React.SFC<any> = (
  { formData, values, reducers }) => {
  const OTHER_LOGO = 'other';
  return (
    <Accordion statePanel={true}
      arrowPositionLeft = {true}
      header={
        <Title className="b-process-info_title b-process-info_title--label">
          Datos del Puesto
        </Title>
      }
      body={
        <div className="g-form g-form--posting">
        <Form.Wrapper type="normal">
          <Form.Row><JobNamePostingContainer/></Form.Row>
          <Form.Row className="b-form-row-description">
            <Field
              name="description"
              render={({ field, form }) => (
                <Form.Column isError={form.errors[field.name] && form.touched[field.name]}>
                  <Form.Control title="Descripción del Puesto:" required>
                    <WysiwygDescription
                      name={field.name}
                      maxCharactersWysiwyg={5000}
                      isErrorWysiwyg={form.errors[field.name] && form.touched[field.name]}
                      contentWysiwyg={field.value}
                      handleBlurWysiwyg={(fieldValue, contentField) => {
                        form.setFieldValue(fieldValue, contentField);
                        if (contentField) {
                          form.setFieldTouched(fieldValue, true);
                        }
                      }}
                    />
                    <TooltipErrorForm error={form.errors[field.name]}
                        touched={form.touched[field.name]}
                        value={field.value}
                        isSubmitting={form.isSubmitting} />
                  </Form.Control>
                </Form.Column>
              )}
            />
          </Form.Row>
          <Form.Row>
            <Field
              name="isDisabled"
              render={({ field }) => (
                <Form.Column>
                  <Form.Control title=" ">
                    <Checkbox id={field.name}
                      theme="white"
                      name={field.name}
                      checked={field.value}
                      onChange={field.onChange}
                      text="Este trabajo podrá ser desempeñado por personas con discapacidad." />
                  </Form.Control>
                </Form.Column>
              )}
            />
          </Form.Row>
          <Form.Row>
            <Field
              name="levelId"
              render={({ field, form }) => (
                <Form.Column isError={form.errors[field.name] && form.touched[field.name]}>
                  <Form.Control title="Nivel del Puesto:" required>
                    <SelectForm
                      id={field.name}
                      name={field.name}
                      defaultText="Seleccionar"
                      value={field.value}
                      onBlur={field.onBlur}
                      onChange={field.onChange}
                      className={form.errors[field.name] && form.touched[field.name] && 'is-error'}
                      data={formData.level} />
                    <TooltipErrorForm error={form.errors[field.name]}
                      touched={form.touched[field.name]}
                      value={field.value}
                      isSubmitting={form.isSubmitting} />
                  </Form.Control>
                </Form.Column>
              )}
            />
            <Field
              name="areaId"
              render={({ field, form }) => (
                <Form.Column isError={form.errors[field.name] && form.touched[field.name]}>
                  <Form.Control title="Área del Puesto:" required>
                    <SelectForm
                      id={field.name}
                      name={field.name}
                      defaultText="Seleccionar"
                      value={field.value}
                      onBlur={field.onBlur}
                      onChange={(e) => {
                        const areaSelected = e.target.selectedIndex;
                        const newItemArea = {};
                        newItemArea['label'] = e.target.options[areaSelected].text;
                        newItemArea['value'] = parseInt(e.target.value, 10);
                        reducers['setAreaExperiences'](newItemArea);
                        field.onChange(e);
                      }}
                      className={form.errors[field.name] && form.touched[field.name] && 'is-error'}
                      data={formData.area} />
                    <TooltipErrorForm error={form.errors[field.name]}
                      touched={form.touched[field.name]}
                      value={field.value}
                      isSubmitting={form.isSubmitting} />
                  </Form.Control>
                </Form.Column>
              )}
            />
          </Form.Row>
          <Form.Row>
            <Field
              name="contractId"
              render={({ field, form }) => (
                <Form.Column isError={form.errors[field.name] && form.touched[field.name]}>
                  <Form.Control title="Modalidad de Contrato:" required>
                    <SelectForm
                      id={field.name}
                      name={field.name}
                      defaultText="Seleccionar"
                      value={field.value}
                      onBlur={field.onBlur}
                      onChange={field.onChange}
                      className={form.errors[field.name] && form.touched[field.name] && 'is-error'}
                      data={formData.contract} />
                    <TooltipErrorForm error={form.errors[field.name]}
                      touched={form.touched[field.name]}
                      value={field.value}
                      isSubmitting={form.isSubmitting} />
                  </Form.Control>
                </Form.Column>
              )}
            />
            <Form.Column>
              <Form.Control title="Salario S/ :" >
                <SalaryJobsContainer integerLimit={6} decimalLimit={3}/>
              </Form.Control>
            </Form.Column>
          </Form.Row>
          <Form.Row>
            <Form.Column>
              <div className="b-controls-company">
                <Form.Control title="Nombre de la Empresa en el Aviso: ">
                  <Field
                    name="companyDisplayType"
                    render={({ field, form }) => (
                      <SelectForm
                        {...field}
                        id={field.name}
                        className={form.errors[field.name] && form.touched[field.name]
                          && 'is-companyDisplayType'}
                        data={formData.company_display_type}/>
                    )}
                  />
                </Form.Control>
                { values.companyDisplayType === OTHER_LOGO && (
                  <React.Fragment>
                    <div className="b-controls-other-company">
                      <Form.Control title="Otra Empresa: ">
                        <Field
                          name="alterName"
                          render={({ field, form }) => (
                            <React.Fragment>
                              <input type="text"
                                {...field}
                                id={field.name}
                                placeholder="Ingresa otro nombre"
                                maxLength={30}
                                className={form.errors[field.name] && form.touched[field.name]
                                    && 'is-error'}
                              />
                              <TooltipErrorForm
                                error={form.errors[field.name]}
                                touched={form.touched[field.name]}
                                isSubmitting={form.isSubmitting}
                              />
                            </React.Fragment>
                          )}
                        />
                      </Form.Control>
                    </div>
                    <div className="b-controls-logo-other">
                      <Form.Control title="Logo de Otra Empresa: ">
                        <Form.Wrapper type="normal">
                          <Field
                            name="alterLogo"
                            render={({ field, form }) => (
                              <UploadImage
                                name={field.name}
                                touched={form.touched[field.name]}
                                errors={form.errors[field.name]}
                                values={form.values}
                                isSubmitting={form.isSubmitting}
                                handleChange={field.onChange}
                                handleBlur={field.onBlur}
                                setFieldError={form.setFieldError}
                                setFieldTouched={form.setFieldTouched}
                              />
                            )}
                          />
                        </Form.Wrapper>
                      </Form.Control>
                    </div>
                  </React.Fragment>
                )}
              </div>
            </Form.Column>
            <Form.Column>
              <NotificationPostulationContainer
                itemsList={values.notifyEmail}
                notifyIsApply={String(values.notifyIsApply) === 'true'}
              />
            </Form.Column>
          </Form.Row>
          </Form.Wrapper>
        </div>
      }
    />
  );
};

export const SectionBasicInformationContainer = withPostingContext(SectionBasicInformation);
