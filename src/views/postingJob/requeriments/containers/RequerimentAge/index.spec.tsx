import * as React from 'react';
import { shallow, render } from 'enzyme';
import { RequerimentAge } from './index';
import { Form } from 'aptitus-components/Components/Form';

describe('Test for RequerimentAge Component when there is a value for Age', () => {
  const wrapper = shallow(<RequerimentAge/>);

  test('Correct rendering', () => {
    const actual = wrapper.find(Form.Row).length;
    expect(actual).toBe(1);
  });

  describe('Method onCheckedDisabled', () => {
    test('When the fields are fill', () => {
      const values = {
        ageMax: '34',
        ageMin: '15',
      };
      const instance = wrapper.instance();
      const disabled = instance.onCheckedDisabled(values);
      expect(disabled).toBeFalsy();

    });

    test('When the fields are empty', () => {
      const values = {
        ageMax: '',
        ageMin: '',
      };
      const instance = wrapper.instance();
      const disabled = instance.onCheckedDisabled(values);
      expect(disabled).toBeTruthy();

    });

  });

});
