import styled from 'styled-components';

export const AgeWrapper = styled.div`
  align-items: center;
  width: 85%;
`;

export const AgeWrapperItemLabel = styled.div`
  display: inline-block;
  margin-right: 10px;
  text-align: center;
  width: 38px;
`;

export const AgeWrapperItemField = styled.div`
  display: inline-block;
  margin-right: ${props => props['data-primary'] ? '50px' : '0px' };
  position: relative;
  width: 40px;
`;

export const RequerimentTitle = styled.label`
  margin-left: 5px;
`;
