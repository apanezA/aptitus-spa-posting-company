import * as React from 'react';
import { Form } from 'aptitus-components/Components/Form';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import MaskedInput from 'react-text-mask';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { AgeWrapper, AgeWrapperItemLabel, AgeWrapperItemField, RequerimentTitle }
  from './index.style';
import { IconTooltip } from '@app/src/views/common/iconTooltip';
import { Field } from 'formik';

export class RequerimentAge extends React.Component<{}, {}> {

  onChange = (form, name) => (e) => {
    if (form.values[name] === '' && e.target.value === '') {
      form.setFieldValue('ageIsExcluding', false);
    }
    form.handleChange(e);
  }

  onCheckedDisabled = (values) => {
    const disabled = (values['ageMax'] !== '' || values['ageMin'] !== '') ? false : true;
    return disabled;
  }

  render() {
    const numberMask = createNumberMask({
      prefix: '',
      integerLimit: 2,
    });

    return (
      <Form.Row>
        <Form.Column>
          <Form.Control title={
            <IconTooltip
              text={<span>Este requisito no se<br /> visualizará en el aviso</span>}
              colorIcon="#989898"
              nameIcon="question_circle">
              <RequerimentTitle>Edad: </RequerimentTitle>
            </IconTooltip>
          }>
            <RequerimentItem
              handleCheckboxDisabled={this.onCheckedDisabled}
              checkboxName="ageIsExcluding"
              checkboxText="Excluyente"
              form={
                <AgeWrapper>
                  <AgeWrapperItemLabel>Desde</AgeWrapperItemLabel>
                  <Field
                    name="ageMin"
                    render={ ({ field, form }) => (
                      <AgeWrapperItemField data-primary>
                        <MaskedInput
                          id="ageMin"
                          name="ageMin"
                          mask={numberMask}
                          value={field['value']}
                          onChange={this.onChange(form, 'ageMax')}
                          onBlur={form.handleBlur}
                          type="text"
                          className={(form.touched['ageMin'] &&
                            form.errors['ageMin']) ? 'is-error' : ''}
                        />
                        <TooltipErrorForm error={form.errors['ageMin']}
                          touched={form.touched['ageMin']}
                          isSubmitting={form.isSubmitting} />
                      </AgeWrapperItemField>
                    )}
                  />
                  <AgeWrapperItemLabel>Hasta</AgeWrapperItemLabel>
                  <Field
                    name="ageMax"
                    render={ ({ field, form }) => (
                      <AgeWrapperItemField data-primary>
                        <MaskedInput
                          id="ageMax"
                          name="ageMax"
                          mask={numberMask}
                          value={field['value']}
                          onChange={this.onChange(form, 'ageMin')}
                          onBlur={form.handleBlur}
                          type="text"
                          className={(form.touched['ageMax'] &&
                            form.errors['ageMax']) ? 'is-error' : ''}
                        />
                        <TooltipErrorForm error={form.errors['ageMax']}
                          touched={form.touched['ageMax']}
                          isSubmitting={form.isSubmitting} />
                      </AgeWrapperItemField>
                    )}
                  />
                </AgeWrapper>
              } />
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}
