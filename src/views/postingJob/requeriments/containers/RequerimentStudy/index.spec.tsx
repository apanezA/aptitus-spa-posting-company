import * as React from 'react';
import { shallow } from 'enzyme';
import { Form } from 'aptitus-components/Components/Form';
import { RequerimentStudy } from './index';
import { areas, areasOrder } from './mock';

describe('RequerimentStudy test', () => {

  const props = {
    setFieldValue() {},
    areas: [],
    degrees: [],
    values: [],
    studyIsExcluding: true,
  };

  test('Correct rendering', () => {
    const wrapper = shallow(<RequerimentStudy {...props}/>);
    const actual = wrapper.find(Form.Row).length;
    expect(actual).toBe(1);
  });

  describe('Method getCountChecked && getCountDischecked test', () => {

    test('Get array of objects checked', () => {
      const wrapper = shallow(<RequerimentStudy {...props}/>);
      const instance = wrapper.instance();
      const newArea = instance.getCountChecked(areas);
      expect(typeof newArea).toBe('object');
    });

    test('Get array of objects disChecked', () => {
      const wrapper = shallow(<RequerimentStudy {...props}/>);
      const instance = wrapper.instance();
      const newAreas = instance.getCountDischecked(areas);
      expect(typeof newAreas).toBe('object');
      expect(newAreas).not.toEqual(expect.arrayContaining(areas));
    });

  });

  describe('Method getOrder test', () => {

    test('When it is updated and there are no added areas', () => {
      const wrapper = shallow(<RequerimentStudy {...props}/>);
      const instance = wrapper.instance();
      const newOrder = instance.getOrder(areas);
      wrapper.update();
      expect(wrapper.state('areas')).toMatchObject(areas);
    });

  });

  describe('Method getSort test', () => {

    test('When it is updated and there are no added areas', () => {
      const wrapper = shallow(<RequerimentStudy {...props}/>);
      const instance = wrapper.instance();
      const newOrder = instance.getSort(areas);
      expect(newOrder).toMatchObject(areasOrder);
    });

  });

  describe('Method isDisabledCareer test', () => {
    const wrapper = shallow(<RequerimentStudy {...props}/>);
    const instance = wrapper.instance();
    const PRIMARY = '2';
    const OTHER_VALUE = '4';

    test('Select option Primary, Secondary or default in Study Minium should return true', () => {
      const result = instance.isDisabledCareer(PRIMARY);
      expect(result).toBeTruthy();
    });

    test('Select option different in Study Minium should return false', () => {
      const result = instance.isDisabledCareer(OTHER_VALUE);
      expect(result).toBeFalsy();
    });

  });

  describe('Method onCheckboxDisabled test', () => {
    const wrapper = shallow(<RequerimentStudy {...props}/>);
    const instance = wrapper.instance();
    const INDEX = 0;

    test('Return false when the field are fill', () => {
      const obj = {
        studies: [{
          gradeId: '4',
          careerTypes: {
            id: 1852944,
            career_type_id: 12,
          },
        }],
      };

      const result = instance.onCheckboxDisabled(INDEX)(obj);
      expect(result).toBeFalsy();
    });

    test('Return true when the field are empty', () => {
      const obj = {
        studies: [{
          gradeId: '',
          careerTypes: [],
        }],
      };

      const result = instance.onCheckboxDisabled(INDEX)(obj);
      expect(result).toBeTruthy();
    });

  });

});
