const areas = [
  {
    value: 1,
    label: 'Administración',
    isChecked: true,
    isDisabled: true,
  },
  {
    value: 3,
    label: 'Tecnología',
    isChecked: true,
    isDisabled: true,
  },
  {
    value: 2,
    label: 'Contabilidad',
    isChecked: false,
    isDisabled: true,
  },
];

const areasOrder = [
  {
    value: 1,
    label: 'Administración',
    isChecked: true,
    isDisabled: true,
  },
  {
    value: 2,
    label: 'Contabilidad',
    isChecked: false,
    isDisabled: true,
  },
  {
    value: 3,
    label: 'Tecnología',
    isChecked: true,
    isDisabled: true,
  },
];

export { areas, areasOrder };
