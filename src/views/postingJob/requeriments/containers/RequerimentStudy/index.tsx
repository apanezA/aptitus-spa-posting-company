import * as React from 'react';
import { Form } from 'aptitus-components/Components/Form';
import { Field } from 'formik';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import { MultiSelect } from 'aptitus-components/Components/MultiSelect';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { SELECT_TYPE_GRADE } from '@app/src/views/postingJob/formPosting/state/constants';
import {
  FieldStudy,
  FieldStudyMinium,
  FieldStudyArea,
  MultiSelectBox,
  WrapperSelectForm,
  WrapperTextStudyArea,
  NameTitle,
  TextItalic,
} from './style';

interface IData {
  value: number;
  label: string;
  isChecked: boolean;
  isDisabled: boolean;
}

interface State {
  areas: IData[];
}

interface Props {
  degrees: object[];
  areas: IData[];
  values: object[];
}

export class RequerimentStudy extends React.Component<Props, State> {
  private refCareerTypes = React.createRef<MultiSelect>();
  state = {
    areas: [...this.props.areas],
  };

  getOrder = (areas) => {
    const areasChecked = this.getCountChecked(areas);
    const areasDischecked = this.getCountDischecked(areas);
    const orderIsChecked = this.getSort(areasChecked);
    const orderDisChecked = this.getSort(areasDischecked);
    const newAreas = orderIsChecked.concat(orderDisChecked);
    this.setState({ areas: newAreas });
  }

  getCountChecked = (areas) => {
    return areas.filter((element) => {
      return element.isChecked  === true;
    });
  }

  getCountDischecked = (areas) => {
    return areas.filter((element) => {
      return element.isChecked  === false;
    });
  }

  getSort = (areas) => {
    let compare;
    return areas.sort((previousItem, nextItem) => {
      if (previousItem.label < nextItem.label) {
        compare = -1;
      } else if (previousItem.label > nextItem.label) {
        compare = 1;
      } else {
        compare = 0;
      }
      return compare;
    });
  }

  onChangeCareer = (form, index) => (areas) => {
    let buildObject;
    const checkedAreas = this.getCountChecked(areas);
    const valueInitial = form.initialValues.studies[0].careerTypes;
    const newAreas = checkedAreas.map(item => (this.buildObject(item)));

    if (valueInitial.length) {
      buildObject = newAreas.map((data) => {
        let obj;
        valueInitial.map(item =>
          (obj = (data.value === item.value) ? this.buildObject(item) : this.buildObject(data)));
        return obj;
      });
    } else {
      buildObject = newAreas;
    }
    form.setFieldValue(`studies.${index}.careerTypes`, buildObject);
  }

  buildObject(data) {
    return {
      id: data.id ? data.id : null,
      value: data.value,
    };
  }

  onChangeGrade = (field, form, index) => (event) => {
    const gradeId = event.target.value;
    const flagCheckbox = gradeId === '' ? true : false ;
    if (flagCheckbox) {
      form.setFieldValue(`studies.${index}.required`, false);
    }
    if (this.isDisabledCareer(gradeId)) {
      const areas = this.refCareerTypes.current.resetDataOptions();
      this.getOrder(areas);
      form.setFieldValue(`studies.${index}.careerTypes`, []);
    }
    field.onChange(event);
  }

  isDisabledCareer = (gradeId) => {
    return [
      SELECT_TYPE_GRADE.ID_PRIMARY,
      SELECT_TYPE_GRADE.ID_SECONDARY,
      SELECT_TYPE_GRADE.ID_WITHOUT_STUDY,
    ].includes(String(gradeId));
  }

  onCheckboxDisabled = index => (values) => {
    return (values.studies[index]['gradeId'] === SELECT_TYPE_GRADE.ID_WITHOUT_STUDY &&
      !values.studies[index]['careerTypes'].length) ? true : false;
  }

  validateStudyCareer(form, index) {
    return (form.errors.studies && form.touched.studies &&
      form.errors.studies[index]['careerTypes'] &&
      form.touched.studies[index]['careerTypes'])
      ? true : false;
  }

  render(): JSX.Element {
    return(
      <Form.Row>
        <Form.Column>
          <Form.Control title="Estudios mínimos:">
          { this.props.values.map((item, index) => {
            return(
              <RequerimentItem
                key={index}
                handleCheckboxDisabled={this.onCheckboxDisabled(index)}
                checkboxName={`studies.${index}.required`}
                checkboxText="Excluyente"
                form= {
                  <FieldStudy>
                    <FieldStudyMinium>
                      <WrapperSelectForm>
                        <Field
                          name={`studies.${index}.gradeId`}
                          render={({ field, form }) => (
                            <React.Fragment>
                              <SelectForm
                                id={field.name}
                                name={field.name}
                                defaultText="Seleccionar"
                                value={field.value}
                                data={this.props.degrees}
                                onChange={this.onChangeGrade(field, form, index)}
                              />
                            </React.Fragment>
                          )}
                        />
                      </WrapperSelectForm>
                    </FieldStudyMinium>
                    <FieldStudyArea>
                      <WrapperTextStudyArea>
                        <NameTitle>Área de estudios:</NameTitle>
                        <TextItalic>(Máximo 5 áreas)</TextItalic>
                      </WrapperTextStudyArea>
                      <MultiSelectBox>
                        <Field
                          name={`studies.${index}.careerTypes`}
                          render={({ field, form }) => {
                            const hasError = this.validateStudyCareer(form, index);
                            return (
                              <React.Fragment>
                                <MultiSelect
                                  ref={this.refCareerTypes}
                                  limit={5}
                                  value={field.value}
                                  dataOptions={this.state.areas}
                                  handleChange={this.onChangeCareer(form, index)}
                                  handleBlur={this.getOrder}
                                  hasLimit={true}
                                  disabled=
                                  {this.isDisabledCareer(form.values['studies'][index]['gradeId'])}
                                  className={hasError && 'g-form_col is-error'}
                                  hasError={hasError}/>
                                <TooltipErrorForm
                                  error={form.errors.studies &&
                                    form.errors.studies[index]['careerTypes']}
                                  touched={form.touched.studies &&
                                    form.touched.studies[index]['careerTypes']}
                                    isSubmitting={form['isSubmitting']}
                                />
                              </React.Fragment>
                            );
                          }}
                        />
                      </MultiSelectBox>
                    </FieldStudyArea>
                  </FieldStudy>
                }
              />
            );
          })}
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}
