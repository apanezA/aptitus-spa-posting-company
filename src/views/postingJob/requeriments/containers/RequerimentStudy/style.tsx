import styled from 'styled-components';

export const FieldStudy = styled.div`
  align-items: flex-start;
  display: flex;
  justify-content: space-between;
  width: 85%;
`;

export const FieldStudyMinium = styled.div`
  width: 48%;
`;

export const WrapperSelectForm = styled.div`
  width: 290px;
`;

export const FieldStudyArea = styled.div`
  align-items: flex-start;
  display: flex;
  width: 52%;
`;

export const WrapperTextStudyArea = styled.div`
  margin-right: 10px;
  width: 110px;
`;

export const TextItalic = styled.div`
  font-size: 12px;
  font-style: italic;
  text-align: right;
`;

export const NameTitle = styled.div`
  text-align: right;
`;

export const MultiSelectBox = styled.div`
  position: relative;
  width: 290px;
`;
