import * as React from 'react';
import { shallow } from 'enzyme';
import { RequerimentPrograms } from './index';
import { Form } from 'aptitus-components/Components/Form';

describe('RequerimentPrograms test', () => {

  const props = {
    setFieldValue: jest.fn(),
    programs: [],
    programsExcluding: false,
  };

  afterEach(() => {
    props.setFieldValue.mockClear();
  });

  test('Correct rendering', () => {
    const wrapper = shallow(<RequerimentPrograms {...props} />);
    const actual = wrapper.find(Form.Row).length;
    expect(actual).toBe(1);
  });

  describe('Method addProgram test', () => {

    test('When a program is added', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const currentPrograms = [{
        id: '1235678',
        value: 123,
        label: 'Word',
        required: false,
      }];
      const isAdded = true;
      instance.addProgram(currentPrograms, isAdded);
      const programs = instance.programs;
      expect(Array.isArray(programs)).toBeTruthy();
      expect(typeof programs[0]).toBe('object');
      expect(programs[0]).toHaveProperty('id', currentPrograms[0].id);
      expect(programs[0]).toHaveProperty('value', currentPrograms[0].value);
      expect(programs[0]).toHaveProperty('label', currentPrograms[0].label);
      expect(programs[0]).toHaveProperty('required', currentPrograms[0].required);
      expect(props.setFieldValue).toBeCalled();
    });

    test('When you add a program and you do not have an id', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const currentPrograms = [{
        value: 123,
        label: 'Word',
        required: false,
      }];
      const isAdded = true;
      instance.addProgram(currentPrograms, isAdded);
      const programs = instance.programs;
      expect(Array.isArray(programs)).toBeTruthy();
      expect(typeof programs[0]).toBe('object');
      expect(programs[0].id).toBe(null);
      expect(programs[0]).toHaveProperty('value', currentPrograms[0].value);
      expect(programs[0]).toHaveProperty('label', currentPrograms[0].label);
      expect(programs[0]).toHaveProperty('required', currentPrograms[0].required);
      expect(props.setFieldValue).toBeCalled();
    });

    test('When the program has not been added', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const currentPrograms = [{ id: '123', label: 'word' }];
      const isAdded = false;
      instance.addProgram(currentPrograms, isAdded);
      const programs = instance.programs;
      expect(Array.isArray(programs)).toBeTruthy();
      expect(programs.length).toBe(0);
      expect(props.setFieldValue).not.toBeCalled();
    });

  });

  describe('Method builderProgram test', () => {

    test('When a new program element is built', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const program = {
        id: '12345678',
        value: '123',
        label: 'Word',
      };
      const programExcluding = true;
      const newProgram = instance.builderProgram(program, programExcluding);
      expect(typeof newProgram).toBe('object');
      expect(newProgram).toHaveProperty('id', program.id);
      expect(typeof newProgram.value).toBe('number');
      expect(newProgram).toHaveProperty('value', Number(program.value));
      expect(newProgram).toHaveProperty('label', program.label);
      expect(newProgram).toHaveProperty('required', programExcluding);
    });

    test('When a new program element is built and it does not have a defined id', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const program = {
        value: '123',
        label: 'Word',
      };
      const programExcluding = true;
      const newProgram = instance.builderProgram(program, programExcluding);
      expect(typeof newProgram).toBe('object');
      expect(newProgram).toHaveProperty('id', null);
      expect(typeof newProgram.value).toBe('number');
      expect(newProgram).toHaveProperty('value', Number(program.value));
      expect(newProgram).toHaveProperty('label', program.label);
      expect(newProgram).toHaveProperty('required', programExcluding);
    });

  });

  describe('Method afterRemoveProgram test', () => {

    test('When an element is removed', () => {
      const wrapper = shallow(<RequerimentPrograms {...props} />);
      const instance = wrapper.instance();
      const currentPrograms = [{
        id: '1235678',
        value: 123,
        label: 'Word',
        required: false,
      }];
      instance.afterRemoveProgram(currentPrograms);
      const programs = instance.programs;
      expect(Array.isArray(programs)).toBeTruthy();
      expect(programs.length).toBe(1);
      expect(props.setFieldValue).toBeCalled();
    });

  });

  describe('Method clearExcluding test', () => {

    test('When there are programs', () => {
      const wrapper = shallow(<RequerimentPrograms {...props} />);
      const instance = wrapper.instance();
      const currentPrograms = [{
        id: '1235678',
        value: 123,
        label: 'Word',
        required: false,
      }];
      instance.clearExcluding(currentPrograms);
      expect(props.setFieldValue).not.toBeCalled();
    });

    test('When there are no programs', () => {
      const wrapper = shallow(<RequerimentPrograms {...props} />);
      const instance = wrapper.instance();
      const currentPrograms = [];
      instance.clearExcluding(currentPrograms);
      expect(props.setFieldValue).toBeCalled();
    });

  });

  describe('Method suggestionsFilter test', () => {

    test('When there are ids to filter', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const programsId = [12, 13];
      const suggestions = [
        { id: 12, label: 'word' },
        { id: 13, label: 'excel' },
      ];
      const suggestionsFilter = instance.suggestionsFilter(suggestions, programsId);
      expect(Array.isArray(suggestionsFilter)).toBeTruthy();
      expect(suggestionsFilter.length).toBe(0);
    });

    test('When there are no ids to filter', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const programsId = [];
      const suggestions = [
        { id: 12, label: 'word' },
        { id: 13, label: 'excel' },
      ];
      const suggestionsFilter = instance.suggestionsFilter(suggestions, programsId);
      expect(Array.isArray(suggestionsFilter)).toBeTruthy();
      expect(typeof suggestionsFilter[0]).toBe('object');
      expect(typeof suggestionsFilter[1]).toBe('object');
      expect(suggestionsFilter.length).toBe(2);
    });

  });

  describe('Method updateExcludingInPrograms test', () => {

    const programs = [{
      id: '1235678',
      value: 123,
      label: 'Word',
      required: false,
    }];

    test('When the value of the exclusive field has changed', () => {
      const wrapper = shallow(<RequerimentPrograms {...props} programs={programs}/>);
      const instance = wrapper.instance();
      const currentPrograms = [{
        id: '1235678',
        value: 123,
        label: 'Word',
        required: false,
      }];
      const isAdded = true;
      instance.addProgram(currentPrograms, isAdded);
      const programExcluding = true;
      instance.updateExcludingInPrograms(programExcluding);
      const isExcluding = instance.programs[0].required;
      expect(isExcluding).toBeTruthy();
      expect(props.setFieldValue).toBeCalled();
    });

  });

  describe('Method suggestionsFilter test', () => {

    test('When there are ids to filter', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const programsId = [12, 13];
      const suggestions = [
        { id: 12, label: 'word' },
        { id: 13, label: 'excel' },
      ];
      const suggestionsFilter = instance.suggestionsFilter(suggestions, programsId);
      expect(Array.isArray(suggestionsFilter)).toBeTruthy();
      expect(suggestionsFilter.length).toBe(0);
    });

    test('When there are no ids to filter', () => {
      const wrapper = shallow(<RequerimentPrograms {...props}/>);
      const instance = wrapper.instance();
      const programsId = [];
      const suggestions = [
        { id: 12, label: 'word' },
        { id: 13, label: 'excel' },
      ];
      const suggestionsFilter = instance.suggestionsFilter(suggestions, programsId);
      expect(Array.isArray(suggestionsFilter)).toBeTruthy();
      expect(typeof suggestionsFilter[0]).toBe('object');
      expect(typeof suggestionsFilter[1]).toBe('object');
      expect(suggestionsFilter.length).toBe(2);
    });

  });

  describe('Method onCheckboxDisabled test', () => {

    test('When there are programs', () => {
      const wrapper = shallow(<RequerimentPrograms {...props} />);
      const instance = wrapper.instance();
      const formikValues = {
        programs: [{
          id: '1235678',
          value: 123,
          label: 'Word',
          required: false,
        }],
      };
      const isCheckboxDisabled = instance.onCheckboxDisabled(formikValues);
      expect(isCheckboxDisabled).toBeFalsy();
    });

    test('When there are no programs', () => {
      const wrapper = shallow(<RequerimentPrograms {...props} />);
      const instance = wrapper.instance();
      const formikValues = { programs: [] };
      const isCheckboxDisabled = instance.onCheckboxDisabled(formikValues);
      expect(isCheckboxDisabled).toBeTruthy();
    });

  });

});
