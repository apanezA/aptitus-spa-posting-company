import * as React from 'react';
import { Form } from 'aptitus-components/Components/Form';
import { Field } from 'formik';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import { InputTag } from 'aptitus-components/Components/InputTag';
import { searchPrograms } from '@app/src/views/postingJob/requeriments/state/services';
import { FormInformation } from './styled';

interface Props {
  setFieldValue(key: string, value: any): void;
  programs: object[];
  programsExcluding: boolean;
}

class RequerimentPrograms extends React.Component<Props, {}> {

  private programs: object[] = [];

  addProgram = (values: object[], isAdded: boolean) => {
    if (isAdded) {
      const { programsExcluding } = this.props;
      this.programs = values.map((item) => {
        return this.builderProgram(item, programsExcluding);
      });
      this.props.setFieldValue('programs', this.programs);
    }
  }

  builderProgram(item: object, excluding: boolean): object {
    return {
      id: item['id'] ? item['id'] : null,
      value: Number(item['value']),
      label: item['label'],
      required: excluding,
    };
  }

  afterRemoveProgram = (values: object[]): void => {
    const { programsExcluding } = this.props;
    this.programs = values.map((item) => {
      return this.builderProgram(item, programsExcluding);
    });
    this.props.setFieldValue('programs', this.programs);
    this.clearExcluding(values);
  }

  clearExcluding(values) {
    const WITHOUT_ELEMENTS = 0;
    if (values.length === WITHOUT_ELEMENTS) {
      this.props.setFieldValue('programsIsExcluding', false);
    }
  }

  getSuggestions = async (value: string) => {
    const programsId: number[] = this.programs.map(item => item['value']);
    const response = {
      data: await searchPrograms.getPrograms(value),
    };
    const suggestions: object[] = response.data.data;
    const filterSuggestions: object[] = this.suggestionsFilter(suggestions, programsId);
    programsId.length && (response.data.data = filterSuggestions);
    return value.length && response;
  }

  suggestionsFilter(suggestions: object[], programsId: number[]): object[] {
    return suggestions.filter((suggestion: object) => {
      const searchResult: number[] = programsId.filter(id => id === suggestion['id']);
      const withoutResults: boolean = !searchResult.length;
      return withoutResults && suggestion;
    });
  }

  onAfterChangeSuggestion = (value: string): void => {
    const isEmpty: boolean = value.length ? false : true;
    isEmpty && searchPrograms.cancelRequest();
  }

  updateExcludingInPrograms(valueExcluding: boolean) {
    this.programs = this.programs.map((program) => {
      return this.builderProgram(program, valueExcluding);
    });
    this.props.setFieldValue('programs', this.programs);
  }

  onCheckboxDisabled = (values: object): boolean => {
    return values['programs'].length ? false : true;
  }

  shouldComponentUpdate(nextProps: object): boolean {
    let isNeedToRender: boolean = false;
    if (this.props.programsExcluding !== nextProps['programsExcluding']) {
      this.updateExcludingInPrograms(nextProps['programsExcluding']);
      isNeedToRender = true;
    } else if (this.props.programs !== nextProps['programs']) {
      isNeedToRender = true;
    }
    return isNeedToRender;
  }

  render(): JSX.Element {
    return(
      <Form.Row>
        <Form.Column>
          <Form.Control title={
            <span>
              <span>Conocimientos Informáticos:</span>
              <FormInformation>(Máximo 20)</FormInformation>
            </span>
          }>
            <RequerimentItem
              handleCheckboxDisabled={this.onCheckboxDisabled}
              checkboxName="programsIsExcluding"
              checkboxText="Excluyente"
              form={
                <Field
                  name="programs"
                  render={({ field }) => {
                    this.programs = field.value;
                    return (
                      <InputTag
                        name="programs"
                        handleAdd={this.addProgram}
                        handleRemove={this.afterRemoveProgram}
                        autocomplete
                        initialTags={this.props.programs}
                        keySuggestion="name"
                        suggestions={this.getSuggestions}
                        handleAfterChangeSuggestion={this.onAfterChangeSuggestion}
                      />
                    );
                  }}/>
              } />
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}

export { RequerimentPrograms };
