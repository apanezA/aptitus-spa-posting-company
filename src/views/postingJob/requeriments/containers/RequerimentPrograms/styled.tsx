import styled from 'styled-components';

export const FormInformation = styled.p`
  color: #575757;
  font-size: 12px;
  font-style: italic;
  font-weight: 400;
  margin: 3px 0;
  text-align: right;
`;
