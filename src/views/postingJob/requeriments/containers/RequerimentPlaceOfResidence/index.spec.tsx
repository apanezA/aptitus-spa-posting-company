import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { withFormik, Field } from 'formik';
import { RequerimentPlaceOfResidence } from './index';

describe('RequerimentPlaceOfResidence component test', () => {

  const props = {
    formData: [
      { value: 2533, label: 'Perú' },
      { value: 3926, label: 'Lima' },
      { value: 3927, label: 'Lima' },
      { value: 3958, label: 'San Isidro' },
    ],
  };

  const InnerForm = props => (
    <form id="formRequerimentPlaceOfResidence">
      <RequerimentPlaceOfResidence {...props} />
      <button type="submit"></button>
    </form>
  );

  const MyForm = withFormik({
    mapPropsToValues: props => ({
      residenceId: 1234,
      residenceIsExcluding: true,
    }),
    handleSubmit: (values) => { },
  })(InnerForm);

  describe('Functional tests', () => {

    let wrapper = null;

    beforeEach(() => wrapper = mount(<MyForm/>));

    test('Correct rendering', () => {
      const actual = wrapper.find('#formRequerimentPlaceOfResidence').length;
      expect(actual).toBe(1);
    });

    test('When the value of the element was changed and it is not empty', () => {
      const name = 'residenceId';
      const select = wrapper.find(Field).first();
      const newResidentId = 7896;
      select.simulate('change', { target: { name, value: newResidentId } });
      const formikValues = wrapper.find(InnerForm).props().values;
      expect(formikValues).toEqual({
        residenceId: newResidentId,
        residenceIsExcluding: true,
      });
    });

    test('When the value of the item was changed to empty', () => {
      const name = 'residenceId';
      const select = wrapper.find(Field).first();
      const newResidentId = 0;
      select.simulate('change', { target: { name, value: newResidentId } });
      const formikValues = wrapper.find(InnerForm).props().values;
      expect(formikValues).toEqual({
        residenceId: 0,
        residenceIsExcluding: false,
      });
    });

  });

  describe('Unit tests', () => {

    let wrapper = null;
    let instance = null;

    beforeEach(() => {
      wrapper = shallow(<RequerimentPlaceOfResidence {...props} />);
      instance = wrapper.instance();
    });

    describe('Method getValueOption test', () => {

      test('When a value change is made in the element', () => {
        const event = {
          target: { value: 2544 },
        };
        const form = {
          handleChange: jest.fn(() => {}),
        };
        instance.getValueOption(form)(event);
        expect(form.handleChange).toBeCalled();
      });

    });

    describe('Method getCheckboxValue test', () => {

      test('When the residence id exists', () => {
        const formikValues = {
          residenceId: 1234,
        };
        const checkboxValue = instance.getCheckboxValue(formikValues);
        expect(checkboxValue).toBeFalsy();
      });

      test('When the residence id does not exist', () => {
        const formikValues = {
          residenceId: null,
        };
        const checkboxValue = instance.getCheckboxValue(formikValues);
        expect(checkboxValue).toBeTruthy();
      });

    });

  });

});
