import * as React from 'react';
import { Field } from 'formik';
import { Form } from 'aptitus-components/Components/Form';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import { IconTooltip } from '@app/src/views/common/iconTooltip';
import { PlaceWrapper, PlaceWrapperItem, RequerimentTitle } from './index.style';

interface Props {
  formData: {
    value: number;
    label: string;
  }[];
}

export class RequerimentPlaceOfResidence extends React.Component<Props, {}> {

  getValueOption = (formikMethods: any) => (e: any) => {
    const emptyValue = e.target.value ? false : true;
    emptyValue && formikMethods.setFieldValue('residenceIsExcluding', false);
    formikMethods.handleChange(e);
  }

  getCheckboxValue = (formikValues: object): boolean => {
    return formikValues['residenceId'] ? false : true;
  }

  render(): JSX.Element {
    return (
      <Form.Row>
        <Form.Column>
          <Form.Control title={
            <IconTooltip
              text={<span>Este requisito no se<br /> visualizará en el aviso</span>}
              colorIcon="#989898"
              nameIcon="question_circle">
              <RequerimentTitle>Lugar de Residencia: </RequerimentTitle>
            </IconTooltip>
          }>
            <RequerimentItem
              handleCheckboxDisabled={this.getCheckboxValue}
              checkboxName="residenceIsExcluding"
              checkboxText="Excluyente"
              form={
                <PlaceWrapper>
                  <PlaceWrapperItem>
                    <Field
                      name="residenceId"
                      render={({ field, form }) => (
                        <SelectForm
                          id="residenceId"
                          name="residenceId"
                          defaultText="Seleccionar"
                          data={this.props.formData}
                          value={field.value}
                          onChange={this.getValueOption(form)}
                        />
                      )}/>
                  </PlaceWrapperItem>
                </PlaceWrapper>
              }
            />
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}
