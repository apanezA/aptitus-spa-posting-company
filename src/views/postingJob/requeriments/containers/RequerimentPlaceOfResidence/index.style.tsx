import styled from 'styled-components';

export const PlaceWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 85%;
`;

export const PlaceWrapperItem = styled.div`
  width: 290px;
`;

export const RequerimentTitle = styled.label`
  margin-left: 5px;
`;
