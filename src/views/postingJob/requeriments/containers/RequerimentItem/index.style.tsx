import styled from 'styled-components';

export const RequirementItemWrapper = styled.div`
  display: inline-flex;
  width: 100%;
`;

export const CheckboxWrapper = styled.div`
  align-items: flex-start;
  display: flex;
  justify-content: flex-end;
  min-width: 15%;
  padding-top: 7px;
  width: 15%;
`;
