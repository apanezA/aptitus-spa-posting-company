import * as React from 'react';
import { Field } from 'formik';
import { Checkbox } from 'aptitus-components/Components/Checkbox';
import { RequirementItemWrapper, CheckboxWrapper } from './index.style';

interface Props {
  form?: JSX.Element;
  header?(changeView: any, isOpened: boolean): JSX.Element;
  checkboxName: string;
  checkboxText: string;
  handleCheckboxDisabled?(values: object): boolean;
  showForm?: boolean;
}

interface State {
  showForm: boolean;
}

class RequerimentItem extends React.Component<Props, State> {

  state = {
    showForm: this.props.showForm,
  };

  static defaultProps = {
    showForm: true,
  };

  changeView = () => {
    this.setState({ showForm: !this.state.showForm });
  }

  render() {
    const { form, header, checkboxName, checkboxText, handleCheckboxDisabled } = this.props;
    const { showForm } = this.state;

    return (
      <React.Fragment>
        { header && header(this.changeView, showForm) }
        { showForm &&
          <RequirementItemWrapper>
          { form }
            <CheckboxWrapper>
              <Field
                name={checkboxName}
                render={({ field, form }) => (
                  <Checkbox theme="white"
                    id={field.name}
                    name={field.name}
                    text={checkboxText}
                    onChange={field.onChange}
                    disabled={handleCheckboxDisabled && handleCheckboxDisabled(form.values)}
                    checked={field.value}
                  />
                )}
              />
            </CheckboxWrapper>
        </RequirementItemWrapper>
        }
      </React.Fragment>
    );
  }
}

export { RequerimentItem };
