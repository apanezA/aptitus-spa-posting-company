import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { RequerimentGender } from './index';

describe('Test for RequerimentGender Component when there is a value for gender', () => {
  const props = {
    formData: [
      {
        value: 'i',
        label: 'Indistinto',
      },
      {
        value: 'm',
        label: 'Masculino',
      },
      {
        value: 'f',
        label: 'Femenino',
      },
    ],
    genderValue: 'M',
    handleChange: () => {},
  };

  describe('Testing the result gotten for setChecked', () => {
    const wrapper = shallow(<RequerimentGender {...props} />);
    const instance = wrapper.instance();
    props.formData.map((item) => {
      const resultExpect = instance.setChecked(item);
      if (resultExpect) {
        test('When the result is true', () => {
          expect(resultExpect).toBe(true);
        });
      } else {
        test('When the result is false', () => {
          expect(resultExpect).toBe(false);
        });
      }
    });
  });

  describe('Testing OnCheckboxDisabled method returned', () => {
    test(
    'OnCheckboxDisabled method returned false' +
    'when there is a default gender',
    () => {
      const wrapper = shallow(<RequerimentGender {...props} />);
      const instance = wrapper.instance();
      const isChecked = instance.onCheckboxDisabled();
      expect(isChecked).toBe(false);
    });

    test(
    'OnCheckboxDisabled method returned true' +
    'when there is not a default gender',
    () => {
      props.genderValue = '';
      const wrapper = shallow(<RequerimentGender {...props} />);
      const instance = wrapper.instance();
      const isChecked = instance.onCheckboxDisabled();
      expect(isChecked).toBe(true);
    });
  });
});
