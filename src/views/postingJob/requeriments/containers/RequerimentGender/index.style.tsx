import styled from 'styled-components';

export const RequerimentGenderWrapper = styled.div`
  align-items: center;
  display: flex;
  height: 34px;
  width: 100%;
`;

export const RequerimentGenderItem = styled.span`
  align-items: center;
  display: flex;
  width: 140px;
`;

export const RequerimentTitle = styled.label`
  margin-left: 5px;
`;
