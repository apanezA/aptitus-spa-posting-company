import * as React from 'react';
import { Field } from 'formik';
import { Form } from 'aptitus-components/Components/Form';
import { Radio } from 'aptitus-components/Components/Radio';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import { RequerimentGenderWrapper, RequerimentGenderItem, RequerimentTitle }
  from './index.style';
import { IconTooltip } from '@app/src/views/common/iconTooltip';

interface Props {
  formData: {
    value: string,
    label: string,
  }[];
  genderValue: string;
}

export class RequerimentGender extends React.Component<Props, {}> {
  onCheckboxDisabled = () => {
    return this.props.genderValue ? false : true;
  }

  onChangeGender = (e: any, field) => {
    field.onChange(e);
  }

  setChecked = (item) => {
    return item.value === this.props.genderValue ? true : false;
  }

  render(): JSX.Element {
    return(
      <Form.Row>
        <Form.Column>
          <Form.Control title={
            <IconTooltip
              text={<span>Este requisito no se<br /> visualizará en el aviso</span>}
              colorIcon="#989898"
              nameIcon="question_circle">
              <RequerimentTitle>Género: </RequerimentTitle>
            </IconTooltip>
          }>
            <RequerimentItem
              handleCheckboxDisabled={this.onCheckboxDisabled}
              checkboxName="genderIsExcluding"
              checkboxText="Excluyente"
              form={
                <RequerimentGenderWrapper>
                  {
                    this.props.formData.map((item, index) => {
                      return (
                        <RequerimentGenderItem key={index}>
                          <Field
                            name="genderValue"
                            render={({ field }) => {
                              return (
                                <Radio
                                  id={item.value}
                                  value={item.value}
                                  name="genderValue"
                                  onClick={(e: Event) => this.onChangeGender(e, field)}
                                  text={item.label}
                                  checked={this.setChecked(item)} />
                              );
                            }}
                          />
                        </RequerimentGenderItem>
                      );
                    })
                  }
                </RequerimentGenderWrapper>
              }/>
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}
