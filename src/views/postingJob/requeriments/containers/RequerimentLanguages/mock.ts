const defaultProps = {
  languages: [
    {
      languageId: '',
      level: 'indistinto',
      required: false,
    },
  ],
  formData: {
    languages: [
      { value: 'de',
        label: 'Alemán',
      },
      { value: 'zh',
        label: 'Chino mandarin',
      },
    ],
    levels: [{
      value: 'indistinto',
      label: 'Indistinto',
    }],
  },
  languageLimit: 2,
};

const savedLanguages = [
  { id: null,
    languageId: 'zh',
    level: 'indistinto',
    required: undefined,
    languageLabel: 'Chino mandarin',
    levelLabel: 'Indistinto',
  },
  { id: null,
    languageId: 'es',
    level: 'indistinto',
    required: true,
    languageLabel: 'Español',
    levelLabel: 'Indistinto',
  },
];

export { defaultProps, savedLanguages };
