import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { Form } from 'aptitus-components/Components/Form';
import { RequerimentLanguages } from './index';
import { withFormik } from 'formik';
import { LanguagesForm }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentLanguages/languagesForm';
import { defaultProps, savedLanguages } from './mock';

describe('RequerimentLanguages test', () => {
  window.alert = jest.fn();
  describe('Functional tests with behaviour normal', () => {
    let wrapper = null;

    const InnerForm = props => (
      <form>
        <RequerimentLanguages  {...defaultProps}/>
        <button type="submit"></button>
      </form>
    );

    const MyForm = withFormik({
      mapPropsToValues: props => ({
        languages: [{
          languageId: '',
          level: 'indistinto',
          required: false,
        }],
      }),
      handleSubmit: (values) => { },
    })(InnerForm);

    beforeEach(() => {
      wrapper = mount(<MyForm/>);
    });

    test('Correct rendering', () => {
      const actual = wrapper.find(Form.Row).length;
      expect(actual).toBe(1);
    });

    test('Show LanguagesForm', () => {
      const form = wrapper.find(RequerimentLanguages);
      const actual = wrapper.find(LanguagesForm).length;
      expect(form.prop('languageLimit')).toBe(2);
      expect(actual).toBe(1);
    });

  });

  describe('Unit Tests', () => {
    let wrapper = null;
    let instance = null;

    beforeEach(() => {
      wrapper = shallow(<RequerimentLanguages {...defaultProps} />);
      instance = wrapper.instance();
      instance.formikArrayHelpers = {
        push: jest.fn(() => {}),
      };
    });

    describe('Validate props', () => {
      test('Validate formData with properties', () => {
        defaultProps.formData.languages.map((item) => {
          expect(item).toHaveProperty('value');
          expect(item).toHaveProperty('label');
        });
      });

      test('Validate levels with properties', () => {
        defaultProps.formData.levels.map((item) => {
          expect(item).toHaveProperty('value');
          expect(item).toHaveProperty('label');
        });
      });
    });

    describe('Method disabledCheckbox test', () => {
      test('Disabled checkbox when languages is empty', () => {
        const formValue = {
          languages: [
            { languageId: '' },
          ],
        };
        const INDEX = 0;
        const disabled = instance.disabledCheckbox(INDEX)(formValue);
        expect(disabled).toBeTruthy();
      });
    });

    describe('Method onAddLanguage test', () => {
      const data = {
        languageId: 'zh',
        languageLabel: 'Chino mandarin',
        level: 'indistinto',
        levelLabel: 'Indistinto',
      };
      const savedLanguages = [{
        id: null,
        languageId: 'zh',
        level: 'indistinto',
        required: false,
        languageLabel: 'Chino mandarin',
        levelLabel: 'Indistinto',
      }];

      test('Add Languages ', () => {
        const isAdded = instance.onAddLanguage(data);
        expect(isAdded).toBeTruthy();
        expect(instance.savedLanguages).toEqual(savedLanguages);
      });

      test('Not added when the data is repeated', () => {
        instance.savedLanguages = savedLanguages;
        const isAdded = instance.onAddLanguage(data);
        expect(isAdded).toBeFalsy();
      });

      test('Not added when pass the limit ', () => {
        instance.savedLanguages = savedLanguages;
        const isAdded = instance.onAddLanguage(data);
        expect(isAdded).toBeFalsy();
      });
    });

    describe('Method BuilderTitle', () => {

      test('show text data saved without excluding', () => {
        instance.savedLanguages = savedLanguages;
        const INDEX = 0;
        const title = instance.builderTitle(INDEX);
        expect(title).toBe('Chino mandarin - Indistinto');
      });

      test('show text data saved with excluding', () => {
        instance.savedLanguages = savedLanguages;
        const INDEX = 1;
        const title = instance.builderTitle(INDEX);
        expect(title).toBe('Español - Indistinto (Excluyente)');
      });

    });

  });

});
