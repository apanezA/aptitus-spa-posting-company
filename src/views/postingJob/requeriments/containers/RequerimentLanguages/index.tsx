import * as React from 'react';
import { FieldArray } from 'formik';
import { Form } from 'aptitus-components/Components/Form';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import { RequerimentsHeaderItem }
  from '@app/src/views/postingJob/requeriments/components/requerimentsHeaderItem';
import { LanguagesForm }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentLanguages/languagesForm';

interface Props {
  languages: object[];
  formData: {
    languages: object[],
    levels: object[],
  };
  languageLimit: number;
}

export let savedLanguages: object[] = [];

class RequerimentLanguages extends React.Component<Props, {}> {

  private savedLanguages: object[] = this.builderSavedLanguages();
  private isExcluding: boolean = false;
  private formikArrayHelpers: any = {};

  onAddLanguage = (data: object) => {
    let isAdded = false;
    const searchResult: object[] = this.searchRepeated(data);
    const isNotRepeated: boolean = searchResult.length ? false : true;
    const isNotInTheLimit: boolean = this.props.languages.length <= this.props.languageLimit;
    const isFullData: boolean = Object.keys(data).length ? true : false;
    const hasLanguageId: boolean = data['languageId'] && data['languageId'].length ? true : false;
    const hasLevel: boolean = data['level'] && data['level'].length ? true : false;
    if (isFullData && hasLanguageId && hasLevel && isNotRepeated && isNotInTheLimit) {
      this.addLanguage(data);
      isAdded = true;
    } else {
      !isNotRepeated && alert('El idioma ya existe');
    }
    return isAdded;
  }

  searchRepeated(data: object): object[] {
    return this.savedLanguages.filter((item: object) => {
      return item['languageId'] === data['languageId'];
    });
  }

  addLanguage(data: object) {
    const newLanguage: object =
      this.builderLanguage(null, data['languageId'], data['level'], this.isExcluding);
    this.setLanguageDefault();
    this.addItemArrayLanguages(newLanguage, data);
    savedLanguages = [...this.savedLanguages];
  }

  builderLanguage(id: number, languageId: string, level: string, required: boolean): object {
    return { id, languageId, level, required };
  }

  setLanguageDefault() {
    const { languageLimit, formData } = this.props;
    const languageDefault = this.builderLanguage(null, '', formData['levels'][0]['value'], false);
    this.savedLanguages.length < languageLimit && this.formikArrayHelpers.push(languageDefault);
  }

  addItemArrayLanguages(newLanguage: object, data: object) {
    newLanguage['languageLabel'] = data['languageLabel'];
    newLanguage['levelLabel'] = data['levelLabel'];
    this.savedLanguages.push(newLanguage);
  }

  disabledCheckbox = (index: number) => (values): boolean => {
    this.isExcluding = values.languages[index].required;
    return values.languages[index].languageId === '' ? true : false;
  }

  builderTitle = (index: number): string => {
    const languageLabel = this.savedLanguages[index]['languageLabel'];
    const levelLabel = this.savedLanguages[index]['levelLabel'];
    const required = this.savedLanguages[index]['required'] ? ' (Excluyente)' : '';
    return `${languageLabel} - ${levelLabel}${required}`;
  }

  isDefaultLanguage(index: number): boolean {
    return this.savedLanguages[index] ? false : true;
  }

  builderSavedLanguages(): object[] {
    let languages = [...this.props.languages];
    languages = languages.slice(0, languages.length - 1);
    if (languages.length) {
      languages = languages.map((language) => {
        const newLanguage = { ...language };
        newLanguage['languageLabel'] = this.getLabel('languages', newLanguage['languageId']);
        newLanguage['levelLabel'] = this.getLabel('levels', newLanguage['level']);
        return newLanguage;
      });
    }
    savedLanguages = [...languages];
    return languages;
  }

  getLabel(formDataKey: string, valueCompare: string): string {
    return this.props.formData[formDataKey].filter((data: object) => {
      return data['value'] === valueCompare;
    })[0]['label'];
  }

  updateSavedLanguages = (index: number) => (): boolean => {
    const { languages } = this.props;
    let isAdded = false;
    if (languages[index]['languageId'].length && languages[index]['level'].length) {
      this.savedLanguages = this.builderSavedLanguages();
      savedLanguages = [...this.savedLanguages];
      isAdded = true;
    }
    return isAdded;
  }

  onDeleteLanguage = (index: number) => () => {
    this.formikArrayHelpers.remove(index);
    this.savedLanguages.splice(index, 1);
    savedLanguages = [...this.savedLanguages];
  }

  render() {
    const { languages, languageLimit } = this.props;

    return (
      <Form.Row>
        <Form.Column>
          <Form.Control title="Idioma:">
            <FieldArray
              name="languages"
              render={(arrayHelpers) => {
                this.formikArrayHelpers = arrayHelpers;
                return languages.map((data: object, index: number) => (
                  index < languageLimit &&
                  <RequerimentItem
                    key={this.isDefaultLanguage(index) ? 'default' : index}
                    handleCheckboxDisabled={this.disabledCheckbox(index)}
                    checkboxName={`languages.${index}.required`}
                    checkboxText="Excluyente"
                    showForm={this.isDefaultLanguage(index) && languages.length <= languageLimit}
                    header={(changeView: any, showForm: boolean) => (
                      !this.isDefaultLanguage(index) &&
                      <RequerimentsHeaderItem
                        changeView={changeView}
                        dynamicButtonIcon={showForm ? 'check2' : 'edit'}
                        dynamicNameTooltip={showForm}
                        title={this.builderTitle(index)}
                        handleEdit={this.updateSavedLanguages(index)}
                        handleDelete={this.onDeleteLanguage(index)} />
                    )}
                    form={
                      <LanguagesForm
                        formData={this.props.formData}
                        indexItem={index}
                        handleAdd={this.onAddLanguage}
                        showAddButton={this.isDefaultLanguage(index)}
                        languageName={`languages.${index}.languageId`}
                        levelName={`languages.${index}.level`} />
                    }
                  />
                ));
              }}
            />
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}

export { RequerimentLanguages };
