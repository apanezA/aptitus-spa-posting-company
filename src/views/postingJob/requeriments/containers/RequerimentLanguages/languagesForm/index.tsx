import * as React from 'react';
import { Field, getIn } from 'formik';
import { Icon } from 'aptitus-components/Components/Icon';
import { Form } from 'aptitus-components/Components/Form';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import {
  WrapperFieldLanguages,
  WrapperFieldLevel,
  WrapperRow,
  LanguageForm,
  WrapperLabelLevels,
  WrapperSelectLanguages,
  WrapperSelectLevels,
  ButtonWrapper,
  TextAdd,
} from './index.style';

interface Props {
  formData: {
    languages: object[],
    levels: object[],
  };
  indexItem?: number;
  handleAdd?(languageData: object): boolean;
  showAddButton: boolean;
  languageName: string;
  levelName: string;
}

export class LanguagesForm extends React.Component<Props, {}> {

  private languageData: object = {};
  private level: object = {};

  onChangeLanguage = (onChange: any, setFieldValue: any) => (event: any) => {
    const input = event.target;
    input.value.length
      ? onChange(event)
      : this.cleanFields(setFieldValue);
    this.updateLanguage(input);
  }

  cleanFields(setFieldValue: any) {
    this.languageData = {};
    const cleanLanguage = {
      languageId: '',
      level: '',
      required: false,
    };
    setFieldValue(`languages.${this.props.indexItem}`, cleanLanguage);
  }

  updateLanguage(input) {
    const { formData } = this.props;
    this.languageData['languageId'] = input.value;
    this.languageData['languageLabel'] = input[input.selectedIndex].text;
    if (this.level['level'] === formData.levels[0]['value']) {
      this.languageData['level'] = 'indistinto';
      this.languageData['levelLabel'] = 'Indistinto';
    }
  }

  onChangeLevel = (onChange: any) => (event: any) => {
    onChange(event);
    this.updateLevel(event.target);
  }

  updateLevel(input) {
    this.languageData['level'] = input.value;
    this.languageData['levelLabel'] = input[input.selectedIndex].text;
  }

  onAddItem = () => {
    this.props.handleAdd && this.props.handleAdd(this.languageData) && (this.languageData = {});
  }

  printErrorClass(errors, touched, name) {
    const error = getIn(errors, name);
    const touch = getIn(touched, name);
    return touch && error ? 'g-form_col is-error' : '';
  }

  disabledField(values) {
    const index = this.props.languageName.split('.')[1];
    return values[index]['languageId'] === '' ? true : false;
  }

  render () {
    const { formData, languageName, levelName } = this.props;

    return (
      <LanguageForm>
        <WrapperRow>
          <WrapperFieldLanguages>
            <Form.Control>
              <Field
                name={languageName}
                render={({ field, form }) => (
                  <WrapperSelectLanguages>
                    <SelectForm
                      id={field.name}
                      name={field.name}
                      defaultText="Seleccionar"
                      data={formData.languages}
                      value={field.value}
                      onChange={this.onChangeLanguage(field.onChange, form.setFieldValue)}
                    />
                  </WrapperSelectLanguages>
                )}
              />
            </Form.Control>
          </WrapperFieldLanguages>
          <WrapperFieldLevel>
            <WrapperLabelLevels> Nivel: </WrapperLabelLevels>
              <Field
                name={levelName}
                render={({ field, form }) => {
                  this.level = {
                    level: field.value,
                    levelLabel: formData.levels[0]['label'],
                  };
                  return (
                    <WrapperSelectLevels>
                      <SelectForm
                        id={field.name}
                        name={field.name}
                        data={formData.levels}
                        value={field.value}
                        disabled={this.disabledField(form.values.languages)}
                        onChange={this.onChangeLevel(field.onChange)}
                        className={this.printErrorClass(form.errors, form.touched, field.name)}
                      />
                      <TooltipErrorForm
                        error={getIn(form.errors, field.name)}
                        touched={getIn(form.touched, field.name)}
                        isSubmitting={form.isSubmitting}
                      />
                    </WrapperSelectLevels>
                  );
                }}
              />
          </WrapperFieldLevel>
        </WrapperRow>
          { this.props.showAddButton &&
            <ButtonWrapper type="button" onClick={this.onAddItem}>
              <Icon data-name="add" fill="#3798cf"/>
              <TextAdd>Agregar otro idioma</TextAdd>
            </ButtonWrapper>
          }
      </LanguageForm>
    );
  }
}
