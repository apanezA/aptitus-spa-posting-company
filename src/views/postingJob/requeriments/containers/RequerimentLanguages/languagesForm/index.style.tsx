import styled from 'styled-components';
import { GLOBAL } from 'aptitus-components/Utils/theme/constants';

export const RequerimentsItem = styled.div`
  background-color: #fff;
  border: 1px solid #eeefee;
  display: flex;
  height: 43px;
  margin-bottom: 12px;
  min-height: 41px;
  width: 838px;
`;

export const WrapperRow = styled.div`
  display: flex;
  padding-bottom: 10px;
`;

export const WrapperFieldLanguages = styled.div`
  width: 48%;
`;

export const WrapperFieldLevel = styled.div`
  display: flex;
  width: 52%
`;

export const WrapperSelectLanguages = styled.div`
  width: 290px;
`;

export const WrapperSelectLevels = styled.div`
  position: relative;
  width: 290px;
`;

export const WrapperLabelLevels = styled.label`
  align-items: center;
  display: flex;
  justify-content: flex-end;
  padding-right: 10px;
  width: 120px;
`;

export const LanguageForm = styled.div`
  padding-bottom: 15px;
  width: 85%;
`;

export const TextAdd = styled.span`
  color: #3798cf;
  font-family: ${GLOBAL.FAMILY_MUSEO};
  height: 13px;
  margin-left: 8px;
  width: 123px;
`;

export const ButtonWrapper = styled.button`
  background: transparent;
  border: 0px;
  cursor: pointer;
  display: inline-block;
`;
