import * as React from 'react';
import { shallow } from 'enzyme';
import { LanguagesForm } from './index';
import { LanguageForm } from './index.style';

describe.skip('LanguagesForm test', () => {

  const props = {
    formData: {
      languages: [],
      levels: [],
    },
    showAddButton: false,
    languageName: '',
    levelName: '',
  };
  const onChangeMock = jest.fn();
  const setValuesMock = jest.fn();
  const formValues = {
    languagesDefault: {
      level: 'basico',
      excluding: false,
    },
  };
  const event = {
    target: {
      value: 'espanol',
      selectedIndex: 0,
      0: { text: 'Español' },
    },
  };

  afterEach(() => {
    onChangeMock.mockClear();
    setValuesMock.mockClear();
  });

  test('Correct rendering', () => {
    const wrapper = shallow(<LanguagesForm {...props} />);
    const actual = wrapper.find(LanguageForm).length;
    expect(actual).toBe(1);
  });

  describe('Method onChangeLanguage test', () => {

    test('When the value of language entry changes', () => {
      const wrapper = shallow(<LanguagesForm {...props} />);
      const instance = wrapper.instance();
      instance.onChangeLanguage(onChangeMock, formValues, setValuesMock)(event);
      const languageData = instance.languageData;
      expect(typeof languageData).toBe('object');
      expect(languageData).toHaveProperty('languageId', event.target.value);
      expect(languageData).toHaveProperty('languageLabel', event.target[0].text);
      expect(onChangeMock).toBeCalled();
    });

  });

  describe('Method onChangeLevel test', () => {

    test('When the value of level entry changes', () => {
      const wrapper = shallow(<LanguagesForm {...props} />);
      const instance = wrapper.instance();
      instance.onChangeLevel(onChangeMock)(event);
      const languageData = instance.languageData;
      expect(typeof languageData).toBe('object');
      expect(languageData).toHaveProperty('level', event.target.value);
      expect(languageData).toHaveProperty('levelLabel', event.target[0].text);
      expect(onChangeMock).toBeCalled();
    });

  });

  describe('Method validateFieldLanguage test', () => {

    test('When the language input field is complete', () => {
      const wrapper = shallow(<LanguagesForm {...props} />);
      const instance = wrapper.instance();
      const inputValue = 'espanol';
      instance.validateFieldLanguage(inputValue, formValues, setValuesMock);
      const levelDisabled = instance.levelDisabled;
      expect(levelDisabled).toBeFalsy();
      expect(setValuesMock).not.toBeCalled();
    });

    test('When the language input field is empty', () => {
      const wrapper = shallow(<LanguagesForm {...props} />);
      const instance = wrapper.instance();
      const inputValue = '';
      instance.validateFieldLanguage(inputValue, formValues, setValuesMock);
      const levelDisabled = instance.levelDisabled;
      expect(levelDisabled).toBeTruthy();
      expect(setValuesMock).toBeCalled();
    });

  });

  describe('Method onAddItem test', () => {

    test('When you try to add a new language', () => {
      let languageDataMock = {};
      const onAdd = jest.fn((data) => {
        languageDataMock = data;
      });
      const wrapper = shallow(<LanguagesForm {...props} handleAdd={onAdd}/>);
      const instance = wrapper.instance();
      const languageData = {
        languageId: 'es',
        languageLabel: 'Español',
        level: 'basico',
        levelLabel: 'Básico',
      };
      instance.languageData = languageData;
      instance.onAddItem();
      expect(typeof languageDataMock).toBe('object');
      expect(languageDataMock).toHaveProperty('languageId', languageData.languageId);
      expect(languageDataMock).toHaveProperty('languageLabel', languageData.languageLabel);
      expect(languageDataMock).toHaveProperty('level', languageData.level);
      expect(languageDataMock).toHaveProperty('levelLabel', languageData.levelLabel);
      expect(onAdd).toBeCalled();
    });

  });

});
