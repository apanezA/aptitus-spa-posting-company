import * as React from 'react';
import { shallow } from 'enzyme';
import { RequerimentExperience } from './index';

describe('Test for Studies Component', () => {
  const experienceMin = 'experienceMin';
  const experienceArea = 'experienceArea';
  const valueYears = '4';
  const valueArea = '34';
  const emptyValue = '';
  const field = {
    onChange: (e: any) => {},
  };
  const form = {
    setFieldValue: (schema: string, value: any) => {},
  };
  const areas = [
    {
      value: 34,
      label: 'Almacén / Logística / Transporte',
      slug: 'almacen-logistica-transporte',
    },
    {
      value: 45,
      label: 'Tecnología, Sistemas y Telecomunicaciones',
      slug: 'tecnologia-sistemas-y-telecomunicaciones',
    },
  ];
  const wrapper = shallow(<RequerimentExperience areas={...areas}/>);

  describe('Test for onCheckboxDisable method', () => {
    test(
      'If experienceMinValue and experienceAreValue have value ' +
      'then onCheckboxDisable is false',
      () => {
        const instance = wrapper.instance();
        instance.experienceMinValue = valueYears;
        instance.experienceAreaValue = valueArea;
        const valueCheckBox = instance.onCheckboxDisabled();
        expect(valueCheckBox).toBe(false);
      },
    );

    test(
      'If experienceMinValue and experienceAreValue have not value ' +
      'then onCheckboxDisable is false',
      () => {
        const instance = wrapper.instance();
        instance.experienceMinValue = emptyValue;
        instance.experienceAreaValue = emptyValue;
        const valueCheckBox = instance.onCheckboxDisabled();
        expect(valueCheckBox).toBe(true);
      },
    );
  });

  describe('Test for onChangeExperienceMin method', () => {
    test('experienceMinValue has the value that change the onChangeExperienceMin method', () => {
      const instance = wrapper.instance();
      const e = { target: { name: experienceMin, value: '12' } };
      instance.onChangeExperienceMin(field, form)(e);
      wrapper.update();
      expect(instance.experienceMinValue).toBe(e.target.value);
    });

    test('experienceMinValue has the value that change the onChangeExperienceArea method', () => {
      const instance = wrapper.instance();
      const e = { target: { name: experienceArea, value: '45' } };
      instance.onChangeExperienceArea(field, form)(e);
      wrapper.update();
      expect(instance.experienceAreaValue).toBe(e.target.value);
    });
  });
});
