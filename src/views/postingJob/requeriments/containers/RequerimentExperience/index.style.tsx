import styled from 'styled-components';

export const ExperienceWrapper = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  width: 85%;
`;

export const ExperienceWrapperItem = styled.div`
  align-items: center;
  display: flex;
  width: 48%;
`;

export const AreaWrapperItem = styled.div`
  align-items: center;
  display: flex;
  width: 52%;
`;

export const ExperienceWrapperInput = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  width: 40px;
`;
export const ExperienceInputLabel = styled.div`
  display: inline-block;
  margin-left: 10px;
  text-align: center;
  width: 38px;
`;
export const ExperienceWrapperSelect = styled.div`
  display: flex;
  justify-content: space-between;
  position: relative;
  width: 290px;
`;

export const ExperienceSelectLabel = styled.label`
  display: inline-block;
  margin-right: 10px;
  text-align: right;
  width: 110px;
`;
