import * as React from 'react';
import { Form } from 'aptitus-components/Components/Form';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { Field, getIn } from 'formik';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';
import MaskedInput from 'react-text-mask';
import { RequerimentItem } from '@app/src/views/postingJob/requeriments/containers/RequerimentItem';
import { ExperienceWrapper,
  ExperienceWrapperItem,
  AreaWrapperItem,
  ExperienceWrapperInput,
  ExperienceInputLabel,
  ExperienceWrapperSelect,
  ExperienceSelectLabel,
}  from './index.style';

interface Props {
  areas: object[];
}

export class RequerimentExperience extends React.Component<Props, {}> {
  experienceMinValue: string = '';
  experienceAreaValue: string = '';

  onCheckboxDisabled = () => {
    return (this.experienceMinValue === '' &&
      this.experienceAreaValue === '') ? true : false;
  }

  setExperienceIsExcludingChecked = (form) => {
    if (this.experienceMinValue === '' && this.experienceAreaValue === '') {
      form.setFieldValue('experienceIsExcluding', false);
    }
  }

  onChangeExperienceMin = (field, form) => (e) => {
    this.experienceMinValue = e.target.value;
    this.setExperienceIsExcludingChecked(form);
    field.onChange(e);
  }

  onChangeExperienceArea = (field, form) => (e) => {
    this.experienceAreaValue = e.target.value;
    this.setExperienceIsExcludingChecked(form);
    field.onChange(e);
  }

  fieldValidClass(errors, touched, name) {
    const hasError = getIn(errors, name);
    const hasTouched = getIn(touched, name);
    return (hasError && hasTouched) ? 'is-error' : '';
  }

  render(): JSX.Element {
    const numberMask = createNumberMask({
      prefix: '',
      integerLimit: 2,
      thousandsSeparatorSymbol: '',
    });
    return (
      <Form.Row>
        <Form.Column>
          <Form.Control title="Experiencia mínima:">
            <RequerimentItem
              handleCheckboxDisabled={this.onCheckboxDisabled}
              checkboxName="experienceIsExcluding"
              checkboxText="Excluyente"
              form={
                <ExperienceWrapper>
                  <ExperienceWrapperItem>
                    <Field
                      name="experienceMin"
                      render={({ field, form }) => {
                        this.experienceMinValue = field.value;
                        return (
                          <ExperienceWrapperInput>
                            <MaskedInput
                              id="experienceMin"
                              name="experienceMin"
                              mask={numberMask}
                              value={field.value}
                              onChange={this.onChangeExperienceMin(field, form)}
                              type="text"
                              className={
                                (form.errors.experienceMin && form.touched.experienceMin ?
                                'g-form_col is-error' : '')
                              }
                            />
                            <TooltipErrorForm
                              error={form.errors.experienceMin}
                              touched={form.touched.experienceMin}
                              value={field.value}
                              isSubmitting={form['isSubmitting']}
                            />
                          </ExperienceWrapperInput>
                        );
                      }}/>
                    <ExperienceInputLabel>Año(s)</ExperienceInputLabel>
                  </ExperienceWrapperItem>
                  <AreaWrapperItem>
                    <ExperienceSelectLabel>Área:</ExperienceSelectLabel>
                    <Field
                      name="experienceArea"
                      render={({ field, form }) => {
                        this.experienceAreaValue = field.value;
                        return (
                          <ExperienceWrapperSelect>
                            <SelectForm
                              id="experienceArea"
                              name="experienceArea"
                              defaultText="Seleccionar"
                              data={this.props.areas}
                              value={String(field.value)}
                              onChange={this.onChangeExperienceArea(field, form)}
                              className={
                                (form.errors.experienceArea && form.touched.experienceArea ?
                                  'g-form_col is-error' : '')
                              }
                            />
                            <TooltipErrorForm
                              error={form.errors.experienceArea}
                              touched={form.touched.experienceArea}
                              value={field.value}
                              isSubmitting={form['isSubmitting']}/>
                          </ExperienceWrapperSelect>
                        );
                      }}
                    />
                  </AreaWrapperItem>
                </ExperienceWrapper>
              } />
          </Form.Control>
        </Form.Column>
      </Form.Row>
    );
  }
}
