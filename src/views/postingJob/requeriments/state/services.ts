import axios from 'axios';

let cancelToken = null;
let source = null;

export const searchPrograms = {
  async getPrograms(values) {
    try {
      if (source) {
        source.cancel();
      }
      cancelToken = axios.CancelToken;
      source = cancelToken.source();
      const params = {
        q: values,
      };
      const { data } = await axios
        .get(`${process.env.APP_URL}/empresa/publica-aviso-web/search-program-ajax`, {
          params,
          cancelToken: source.token,
        });
      return data;
    } catch (e) {
      throw new Error();
    }
  },

  cancelRequest() {
    return source.cancel();
  },
};
