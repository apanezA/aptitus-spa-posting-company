import * as React from 'react';
import { Accordion } from 'aptitus-components/Components/Accordion';
import { Form } from 'aptitus-components/Components/Form';
import { Title } from '@app/src/views/common/styledCommon';
import { RequerimentGender }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentGender';
import { RequerimentAge } from '@app/src/views/postingJob/requeriments/containers/RequerimentAge';
import { RequerimentPlaceOfResidence } from
  '@app/src/views/postingJob/requeriments/containers/RequerimentPlaceOfResidence';
import { RequerimentPrograms }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentPrograms';
import { RequerimentExperience }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentExperience';
import { RequerimentStudy }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentStudy';
import { RequerimentLanguages }
  from '@app/src/views/postingJob/requeriments/containers/RequerimentLanguages';
import { withPostingContext } from '@app/src/views/postingJob/formPosting/state/store';
import { Icon } from 'aptitus-components/Components/Icon';
import './index.scss';

interface IData {
  value: number;
  label: string;
  isChecked: boolean;
  isDisabled: boolean;
}

interface ISectionRequirementsProps {
  values  : object;
  errors  : object;
  formData: object;
  handleChange(e: any): void;
  setFieldValue(key: string, value: number): void;
  store: {
    userData: object;
    formData: object;
    listExperiencesArea: object[];
    formik: object;
  };
}

interface ISectionRequirementsState {
  requeriments : object | object[];
}

export class SectionRequirementsComponent extends
  React.Component<ISectionRequirementsProps, ISectionRequirementsState> {
  data: IData[];
  constructor(props: ISectionRequirementsProps) {
    super(props);
    this.data = this.getArrAreaStudy(this.props.formData['requirements'].career_type);
  }

  getArrAreaStudy(data) {
    return data.map((element) => {
      return {
        value: element.value,
        label: element.label,
        isChecked: element.is_checked,
        isDisabled: element.is_disabled,
      };
    });
  }

  render() {
    const formDataLanguage = {
      languages: this.props.store.formData['requirements'].language,
      levels: this.props.store.formData['requirements'].level,
    };

    return (
      <Accordion statePanel={true}
        arrowPositionLeft={true}
        header={
          <div className="b-section-requirements_title">
            <Title className="b-process-info_title"> Requisitos</Title>
            <div className="b-section-requirements_title__information">
              <Icon data-name="information" fill="#f3d044" />
              <span className="b-section-requirements_title__text">
                Agrega requisitos para hacer más precisa tu publicación,
                los cuales una vez seleccionados aparecerán en el aviso.
              </span>
            </div>
          </div>
        }
        body={
          <div className="g-form g-form--posting">
            <Form.Wrapper type="normal">
              <RequerimentGender
                genderValue={this.props.values['genderValue']}
                formData={this.props.formData['requirements'].gender_type}/>
              <RequerimentAge/>
              <RequerimentPlaceOfResidence
                formData={this.props.formData['requirements'].location} />
              <RequerimentExperience
                areas={this.props.store.listExperiencesArea}/>
              <RequerimentStudy
                values={this.props.values['studies']}
                degrees={this.props.formData['requirements'].degree}
                areas={this.data} />
              <RequerimentLanguages
                languages={this.props.values['languages']}
                formData={formDataLanguage}
                languageLimit={5}/>
              <RequerimentPrograms
                setFieldValue={this.props.setFieldValue}
                programs={this.props.values['programs']}
                programsExcluding={this.props.values['programsIsExcluding']} />
            </Form.Wrapper>
          </div>
        }
      />
    );
  }
}

export const SectionRequirements = withPostingContext(SectionRequirementsComponent);
