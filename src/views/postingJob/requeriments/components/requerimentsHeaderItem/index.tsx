import * as React from 'react';
import { Button } from 'aptitus-components/Components/Button';
import { Modal } from 'aptitus-components/Components/Modal';
import { RequerimentsItem, Text, SectionButtons,
  RequerimentsModal, ModalText, ModalButtons, CustomButton } from './index.style';
import { Tooltip } from 'aptitus-components/Components/Tooltip';

interface Props {
  changeView(): void;
  title: string;
  dynamicButtonIcon: string;
  handleEdit?(): boolean;
  handleDelete?(): void;
  dynamicNameTooltip: boolean;
}

export class RequerimentsHeaderItem extends React.Component<Props, {}> {

  refModalRequeriments: any;

  state = {
    showTooltip: false,
  };

  onEdit = () => {
    this.props.handleEdit() && this.props.changeView();
  }

  onDelete = () => {
    this.props.handleDelete();
    this.closeModal();
  }

  openModal = () => {
    this.refModalRequeriments.open();
  }

  closeModal = () => {
    this.refModalRequeriments.close();
  }

  render() {
    const { dynamicButtonIcon, dynamicNameTooltip } = this.props;

    return (
      <RequerimentsItem>
        <Text>{this.props.title}</Text>
        <SectionButtons>
          <Tooltip title={dynamicNameTooltip ? 'Guardar' : 'Editar'}>
            <Button
              icon={{ name: dynamicButtonIcon, direction: 'left', height: '15' }}
              onClick={this.onEdit}
              radial={true} background="#3798cf" width="25px" height="25px"/>
          </Tooltip>
          <Tooltip title="Eliminar">
            <Button
              icon={{ name: 'delete2', direction: 'left' , height:'15' }}
              onClick={this.openModal}
              radial={true} background="#807f7f" width="25px" height="25px"/>
          </Tooltip>
        </SectionButtons>
        <Modal
          ref={input => this.refModalRequeriments = input}
          responsive>
          <RequerimentsModal>
            <ModalText>
              ¿Estás seguro que deseas eliminarlo?
            </ModalText>
            <ModalButtons>
              <CustomButton onClick={this.closeModal} text="Cancelar" background="#606060"/>
              <CustomButton onClick={this.onDelete} text="Aceptar" background="#045584"/>
            </ModalButtons>
          </RequerimentsModal>
        </Modal>
      </RequerimentsItem>
    );
  }
}
