import styled from 'styled-components';
import { Button } from 'aptitus-components/Components/Button';

export const RequerimentsItem = styled.div`
  background-color: #fff;
  border: 1px solid #eeefee;
  display: flex;
  height: 43px;
  margin-bottom: 12px;
  min-height: 41px;
  width: 818px;
`;

export const Text = styled.span`
  display: block;
  font-weight: bold;
  padding: 12px;
  width: calc(100% - 80px);
  word-wrap: break-word;
`;

export const SectionButtons = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-evenly;
  width: 80px;
`;

export const RequerimentsModal = styled.div`
  min-width: 500px;
  padding: 50px 50px 20px 50px;
`;

export const ModalText = styled.div`
  padding-bottom: 20px;
  text-align: center;
`;
export const ModalButtons = styled.div`
  display: flex;
  justify-content: space-evenly;
`;
export const CustomButton = styled(Button)`
  width: 40%;
`;
