import * as React from 'react';
import { Form } from 'aptitus-components/Components/Form';

const Header = ({ title }) => {
  return (
    <div className="b-controls-question_header">
      <h2 className="b-controls-question_header-title">{title}</h2>
      <div className="b-controls-question_header-line"></div>
    </div>
  );
};

const Body = ({ options, input, title }) => {
  return (
    <div className="b-controls-question_body">
      <Form.Control title={title}>
        <div className="b-controls-question_field">
          {input}
        </div>
        {options}
      </Form.Control>
    </div>
  );
};

export const QuestionLayout = {
  Header,
  Body,
};
