import { IQuestionValue } from './index.interfaces';

export const QuestionsBox = {
  open: { value: 'open', label: 'Pregunta Abierta' },
  suggested:{
    value: 'suggested',
    label: 'Pregunta Predefinida',
    questions: [
      {
        value: 1,
        label: '\u00bfA cu\u00e1nto ascienden tus expectativas salariales? Indicar en bruto.',
      },
      {
        value: 2,
        label: '\u00bfCu\u00e1nto tiempo tienes de experiencia en el rubro? Especificar.',
      },
      {
        value: 3,
        label: '\u00bfCuentas con disponibilidad inmediata para trabajar?',
      },
      {
        value: 4,
        label: '\u00bfCuentas con licencia de conducir? Indicar la categor\u00eda.',
      },
      {
        value: 5,
        label: '\u00bfCuentas con movilidad propia?',
      },
      {
        value: 6,
        label: '\u00bfCuentas con disponibilidad de trabajar en horarios rotativos?',
      },
      {
        value: 7,
        label: '\u00bfCuentas con disponibilidad para viajar?',
      },
    ],
  },
  alternative: {
    value: 'alternative',
    label: 'Pregunta de Elecci\u00f3n M\u00faltiple',
  },
};

export const questions:IQuestionValue[] = [
  {
    id: 280563,
    name: 'aaaaaa',
    type: 'open',
  },
  {
    id: 280564,
    name: '¿Cuánto tiempo tienes de experiencia en el rubro? Especificar.',
    type: 'suggested',
  },
  {
    id: 280565,
    name: 'pregunta3',
    type: 'alternative',
    choices: [
      {
        id: 1723,
        value: '11111',
        checked: true,
      },
      {
        id: 1724,
        value: '2222',
        checked: false,
      },
      {
        id: 1725,
        value: '33333',
        checked: false,
      },
    ],
  },
];
