import * as React from 'react';
import { shallow, mount, render } from 'enzyme';
import { QuestionsJobsContainer } from './index';
import { OptionMultipleQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionMultipleQuestion';
import { OptionOpenedQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionOpenedQuestion';
import { OptionPredefinedQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionPredefinedQuestion';
import { QuestionsBox }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/dataMockSpec';
import { Utils } from 'aptitus-components/Utils';

describe('The QuestionJobs component', () => {
  const props = {
    errors : {},
    touched: {},
    store  : {
      userData: {
        questions: [{}],
      },
      formData: {
        question_type: {
          suggested: {
            questions: QuestionsBox.suggested.questions,
          },
        },
      },
      formik: {
        setFieldValue: () => {},
        handleChange : () => {},
        handleBlur   : () => {},
      },
    },
    setTouched: () => {},
    isSubmitting : {},
    submitForm   : () => {},
    setFieldValue: () => {},
  };

  describe('Render snapshot', () => {
    test('Render Component correctly', () => {
      const wrapper = shallow(<QuestionsJobsContainer {...props}/>);
      const instance = wrapper.instance();
      wrapper.update();
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  describe('test render a Multiple Question', () => {
    test('render a new Multiple Question', () => {
      const wrapper = mount(<QuestionsJobsContainer {...props} />);
      const instance = wrapper.instance();
      instance.addTemplateList('alternative');
      wrapper.update();
      expect(wrapper.find(OptionMultipleQuestion)).toHaveLength(1);
    });

    test('Create a new item multiple', () => {
      const uuId = Utils.uuID();
      const expectToCompare = {
        identifier: uuId,
        question: {
          id: null,
          name: '',
          type: 'alternative',
          mode: 'temp',
          choices: [
            {
              id: null,
              value: '',
              checked: true,
            },
            {
              id: null,
              value: '',
              checked: false,
            },
          ],
        },
      };
      const wrapper = mount(<QuestionsJobsContainer {...props} />);
      const instance = wrapper.instance();
      const newItem = instance.getNewItem('alternative', uuId);
      expect(expectToCompare).toMatchObject(newItem);
    });
  });

  describe('test render a suggested Question', () => {
    test('Render a New Suggested Question', () => {
      const wrapper = mount(<QuestionsJobsContainer {...props} />);
      const instance = wrapper.instance();
      instance.addTemplateList('suggested');
      wrapper.update();
      expect(wrapper.find(OptionPredefinedQuestion)).toHaveLength(1);
    });

    test('test a new item suggested', () => {
      const uuId = Utils.uuID();
      const expectToCompare = {
        identifier: uuId,
        question: {
          id: null,
          name: '',
          type: 'suggested',
          mode: 'temp',
        },
      };

      const wrapper = mount(<QuestionsJobsContainer {...props} />);
      const instance = wrapper.instance();
      const newItem = instance.getNewItem('suggested', uuId);
      expect(expectToCompare).toMatchObject(newItem);
    });
  });

  describe('test render a Opened Question', () => {
    test('Render a New Opened Question', () => {
      const wrapper = mount(<QuestionsJobsContainer {...props} />);
      const instance = wrapper.instance();
      instance.addTemplateList('open');
      wrapper.update();
      expect(wrapper.find(OptionOpenedQuestion)).toHaveLength(1);
    });

    test('Create a new item opened', () => {
      const uuId = Utils.uuID();
      const expectToCompare = {
        identifier: uuId,
        question: {
          id: null,
          name: '',
          type: 'open',
          mode: 'temp',
        },
      };

      const wrapper = mount(<QuestionsJobsContainer {...props} />);
      const instance = wrapper.instance();
      const newItem = instance.getNewItem('open', uuId);
      expect(expectToCompare).toMatchObject(newItem);
    });
  });

  test('Test Limit of Questions', () => {
    const wrapper = shallow(<QuestionsJobsContainer {...props} />);
    const instance = wrapper.instance();
    wrapper.update();
    expect(wrapper.state().questionsSelected.length).toBeLessThan(6);
  });
});
