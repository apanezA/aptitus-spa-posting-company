export const QUESTIONS = {
  type: {
    opened    : 'open',
    predefined: 'suggested',
    multiple  : 'alternative',
  },
  actionsListAdded: {
    edit  : 'edit',
    delete: 'delete',
  },
  modeListAdded: {
    temp: 'temp',
    list: 'list',
    edit: 'edit',
  },
};
