import * as React from 'react';
import { QuestionHeader } from '@app/src/views/postingJob/questionsJob/containers/questionHeader';
import './index.scss';
import { QuestionBuilder } from '@app/src/views/postingJob/questionsJob/containers/questionBuilder';
import { IStructureQuestion, IQuestionValue }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.interfaces';
import { QUESTIONS }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.const';
import { Utils } from 'aptitus-components/Utils';
import { Form } from 'aptitus-components/Components/Form';
import { withPostingContext } from '@app/src/views/postingJob/formPosting/state/store';

interface IQuestionsJobsState {
  questionsSelected: IStructureQuestion[];
}

interface IQuestionsJobsProps {
  store: {
    userData: {
      questions: object[];
    };
    formData: object;
    formik: {
      setFieldValue(field: string, value: string, shouldValidate?: boolean): void;
      handleChange(): void;
      handleBlur()  : void;
    };
  };
  errors : object;
  touched: object;
  setTouched(fields: any): void;

  isSubmitting: object;
  submitForm()  : void;
  setFieldValue(field: string, value: string, shouldValidate?: boolean): void;
}

export class QuestionsJobsContainer
  extends React.Component<IQuestionsJobsProps, IQuestionsJobsState> {
  public state: IQuestionsJobsState;
  LIMIT_QUESTIONS: number;
  FORMIK_SCHEMA: string;

  constructor(props: any) {
    super(props);
    this.state = {
      questionsSelected: this.getInitialQuestions(this.props.store.userData['questions']),
    };
    this.LIMIT_QUESTIONS = 5;
    this.FORMIK_SCHEMA = 'questions';
    this.addTemplateList = this.addTemplateList.bind(this);
    this.handlerClickListAdded = this.handlerClickListAdded.bind(this);
  }

  getInitialQuestions(questions: object[]) {
    if (!questions) return [];
    return questions.map((element) => {
      return this.getNewItem(null, Utils.uuID(), element);
    });
  }

  getItems(items) {
    return items.map(element => element.question);
  }

  getNewItem(typeQuestion: string, uuId, existingQuestion?):IStructureQuestion {
    const newQuestion: IQuestionValue = {
      id: null,
      name: '',
      type: typeQuestion,
      mode: QUESTIONS.modeListAdded.temp,
    };

    // En caso de que sea pregunta multiple agrego dos opciones por defecto
    if (typeQuestion === QUESTIONS.type.multiple) {
      newQuestion.choices = [
        { id: null, value: '', checked: true },
        { id: null, value: '', checked: false },
      ];
    }

    // En caso de que venga una pregunta ya definida, le agrego que sea en modo lista
    if (existingQuestion) {
      existingQuestion.mode = QUESTIONS.modeListAdded.list;
    }

    return {
      identifier: uuId,
      question: existingQuestion || newQuestion,
    };
  }

  addTemplateList(typeQuestion) {
    const questionsSelected = [...this.state.questionsSelected];
    if (questionsSelected.length < this.LIMIT_QUESTIONS) {
      questionsSelected.push(this.getNewItem(typeQuestion, Utils.uuID()));
      this.props.setFieldValue(this.FORMIK_SCHEMA, this.getItems(questionsSelected), false);
      this.setState({ questionsSelected });
    }
  }

  handlerClickListAdded(element: IStructureQuestion, action: 'edit'|'delete') {
    const { identifier, question } = element;
    if (action === QUESTIONS.actionsListAdded.delete) {
      this.deleteQuestionFromList(identifier, question.mode);
    } else if (action === QUESTIONS.actionsListAdded.edit) {
      this.editQuestionFromList(identifier);
    }
  }

  editQuestionFromList(identifier) {
    let shouldValidate:boolean;
    let questionsSelected = [...this.state.questionsSelected];
    questionsSelected = questionsSelected.map((element) => {
      if (element.identifier === identifier) {
        switch (element.question.mode) {
          case QUESTIONS.modeListAdded.list:
            element.question.mode = QUESTIONS.modeListAdded.edit;
            shouldValidate = true;
            break;
          case QUESTIONS.modeListAdded.edit:
          case QUESTIONS.modeListAdded.temp:
            element.question.mode = QUESTIONS.modeListAdded.list;
            shouldValidate = true;
            break;
        }
      }
      return element;
    });
    this.props.setFieldValue(this.FORMIK_SCHEMA, this.getItems(questionsSelected), shouldValidate);
    this.setState({ questionsSelected });
  }

  deleteQuestionFromList(identifier, mode) {
    let questionsSelected = [...this.state.questionsSelected];

    switch (mode) {
      case QUESTIONS.modeListAdded.list:
      case QUESTIONS.modeListAdded.temp:
        questionsSelected = questionsSelected.filter((element) => {
          return element.identifier !== identifier;
        });
        break;
      case QUESTIONS.modeListAdded.edit:
        questionsSelected = questionsSelected.map((element) => {
          if (element.identifier === identifier) {
            element.question.mode = QUESTIONS.modeListAdded.list;
          }
          return element;
        });
    }

    this.props.setFieldValue(this.FORMIK_SCHEMA, this.getItems(questionsSelected), false);
    this.setState({ questionsSelected });
  }

  render () {
    return (
      <div className="b-controls-question">
      <Form.Wrapper type="normal">
        <Form.Row classOptional="b-controls-question_form">
          <QuestionHeader
            formData={this.props.store.formData}
            addTemplateList={this.addTemplateList}
            errors={this.props.errors['questions']}
            setTouched={this.props.setTouched}
            userData={this.props.store.userData}
            submitForm={this.props.submitForm}
          />
        </Form.Row>
        <div className="b-controls-question_row">
          {
            this.state.questionsSelected.map((element, index) => {
              return (
                <QuestionBuilder
                  key={index}
                  index={index}
                  schema={this.FORMIK_SCHEMA}
                  element={element}
                  handlerClick={this.handlerClickListAdded}
                  formData={this.props.store.formData}
                  errors={this.props.errors}
                  touched={this.props.touched}
                  setFieldValue={this.props.store.formik.setFieldValue}
                  handleChange={this.props.store.formik.handleChange}
                  handleBlur={this.props.store.formik.handleBlur}
                  isSubmitting={this.props.isSubmitting}
                  submitForm={this.props.submitForm}
                />
              );
            })
          }
        </div>
      </Form.Wrapper>
      </div>
    );
  }
}

export const QuestionsJobs = withPostingContext(QuestionsJobsContainer);
