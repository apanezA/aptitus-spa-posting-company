export interface IOptionQuestionProps {
  index: number;
  schema: string;
  handlerAdd(question: IQuestionValue): void;
  handlerDelete(identifier: null): void;
  defaultValues? : IQuestionValue;
  options?: JSX.Element;
  isSubmitting: boolean;
  submitForm?(): void;

  touched: object;
  errors: object;
  hasErrorClass?(fieldSchema: string, index?: number): void;
  hasErrorElement?(fieldSchema: string, index?: number): void;
  setFieldValue?(field: string, value: any, shouldValidate?: boolean): void;
  handleChange?(e: React.ChangeEvent<any>): void;
  handleBlur?(e: any): void;
  questionsList?: IQuestionListValue[];
}

export interface IQuestionListValue {
  value: number;
  label: string;
}

export interface IStructureQuestion {
  identifier: number | string;
  question: IQuestionValue;
}

export interface IQuestionValue {
  id: string | number;
  name: string;
  type: string;
  mode?: string;
  choices?: IOptionsQuestion[];
}

export interface IOptionsQuestion {
  id: string | number;
  value: string;
  checked?: boolean;
}

export interface IOptionQuestionState {
  errors?: string;
  touched?: boolean;
  setFieldValue?(field: string, value: any, shouldValidate?: boolean): void;
}
