import * as React from 'react';
import { mount } from 'enzyme';
import { OptionMultipleQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionMultipleQuestion';

describe('The OptionMultipleQuestion component', () => {
  describe('', () => {
    const props = {
      index: 0,
      schema: '',
      isSubmitting: false,
      touched: {},
      errors: {},
      defaultValues: {
        mode: 'temp',
        name: 'Pregunta',
        id: 232131,
        type: 'alternative',
        choices: [
          {
            id: 1723,
            value: 'opcion1',
            checked: true,
          },
          {
            id: 1724,
            value: 'opcion2',
            checked: false,
          },
        ],
      },

      submitForm: () => {},
      handlerAdd: () => {},
      handlerDelete: () => {},
    };

    test('Testing a new Multiple Question with 2 options', () => {
      const wrapper = mount(<OptionMultipleQuestion {...props}/>);
      expect(wrapper.find('.b-controls-fields')).toHaveLength(2);
    });

    test('Testing add New Question with 2 valid options', () => {
      const wrapper = mount(<OptionMultipleQuestion {...props}/>);
      // const WrapperDynamic = mount(<DynamicFieldFormik {...propsDynamicFieldFormik}/>);
      const instance = wrapper.instance();
      const validOptions = instance.areOptionsEmpty(props.defaultValues.choices);

      expect(validOptions).toBeFalsy();
    });
  });
});
