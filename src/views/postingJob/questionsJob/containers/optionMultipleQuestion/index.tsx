import * as React from 'react';
import { DynamicFieldFormik }
  from '@app/src/views/postingJob/questionsJob/containers/dynamicField';
import '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.scss';
import { Button } from 'aptitus-components/Components/Button';
import { IOptionQuestionProps, IOptionsQuestion, IQuestionValue }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.interfaces';
import { QuestionLayout }
  from '@app/src/views/postingJob/questionsJob/containers/questionLayout';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { Tooltip } from 'aptitus-components/Components/Tooltip';
import { CountWords } from 'aptitus-components/Components/CountWords';

export class OptionMultipleQuestion extends React.Component<IOptionQuestionProps, any> {
  refDynamicField: any;
  refInput: HTMLInputElement;
  refCountMultipleQuestion: any;
  FORMIK_FIELDSCHEMA: string;
  MINIMUM_AMOUNT_QUESTIONS: number;

  constructor(props: IOptionQuestionProps) {
    super(props);
    this.FORMIK_FIELDSCHEMA = 'questions';
    this.MINIMUM_AMOUNT_QUESTIONS = 2;
    this.refCountMultipleQuestion = React.createRef();
    this.onAddQuestion = this.onAddQuestion.bind(this);
    this.onDeleteQuestion = this.onDeleteQuestion.bind(this);
    this.onInputText = this.onInputText.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    this.refCountMultipleQuestion &&
    this.refCountMultipleQuestion.current.counter(this.refInput);
  }

  onAddQuestion(value: any) {
    const options: IOptionsQuestion[] = this.refDynamicField.getInnerRef().getItems();
    const areOptionsEmpty = this.areOptionsEmpty(options);

    if (value !== '' &&
      !areOptionsEmpty && options.length >= this.MINIMUM_AMOUNT_QUESTIONS) {
      const question: IQuestionValue = {
        id: this.props.defaultValues.id,
        name: this.refInput.value,
        type: this.props.defaultValues.type,
        mode: this.props.defaultValues.mode,
        choices: options,
      };
      this.props.handlerAdd(question);
    }
    this.props.submitForm();
  }

  areOptionsEmpty(options) {
    let areEmpty = false;
    for (const option of options) {
      if (option.value.trim() === '') {
        areEmpty =  true;
        break;
      }
    }
    return areEmpty;
  }

  onDeleteQuestion() {
    this.props.handlerDelete(null);
  }

  setErrorClass() {
    const errorField = this.props.errors[this.FORMIK_FIELDSCHEMA];
    const touchedField = this.props.touched[this.FORMIK_FIELDSCHEMA];

    const isError = errorField && errorField[this.props.index] &&
          (errorField[this.props.index].name || errorField[this.props.index].mode);
    const isTouched = touchedField && touchedField[this.props.index] &&
          (touchedField[this.props.index].name || touchedField[this.props.index].mode);

    return (isError && isTouched) ? 'is-error' : '';
  }

  setErrorMessage() {
    const errorField = this.props.errors[this.FORMIK_FIELDSCHEMA];
    const errorMessage = (errorField && errorField[this.props.index]) ?
        errorField[this.props.index].name ||
        errorField[this.props.index].mode : '';
    return errorMessage;
  }

  setActiveTouched() {
    const touchedField = this.props.touched[this.FORMIK_FIELDSCHEMA];
    const touchedIsActive = (touchedField && touchedField[this.props.index]) ?
    touchedField[this.props.index].name ||
    touchedField[this.props.index].mode : '';
    return touchedIsActive;
  }

  onInputText(event) {
    this.props.handleChange(event);
    if (event.keyCode === 13) {
      this.onAddQuestion(this.refInput.value);
    }
  }

  verifiedStringEmpty(value) {
    return (!value || /^\s*$/.test(value));
  }

  render() {
    return (
      <React.Fragment>
        <QuestionLayout.Header title={'Pregunta de elección múltiple'} />
        <QuestionLayout.Body
          title={'Pregunta:'}
          input={
            <React.Fragment>
              <input
                type="text"
                name={`${this.props.schema}.${this.props.index}.name`}
                maxLength={100}
                ref={ref => this.refInput = ref}
                defaultValue={this.props.defaultValues.name}
                onInput={this.onInputText}
                onBlur={this.props.handleBlur}
                onChange={this.onChange}
                placeholder="Ejemplo: Elige tu habilidad más importante"
                className={this.setErrorClass()}
                autoFocus />
              <CountWords
                ref={this.refCountMultipleQuestion}
                maxCharacters={100}
                counterAlign="left"
                alternativeText="Quedan "
                />
              <TooltipErrorForm error={this.setErrorMessage()}
                touched={this.setActiveTouched()}
                isSubmitting={this.props.isSubmitting} />
            </React.Fragment>
          }
          options={
            <div className="b-controls-bar">
              <Tooltip title="Guardar">
                <Button
                  icon={{ name: 'check3', direction: 'left' , height: '12' }}
                  onClick={() => this.onAddQuestion(this.refInput.value)}
                  radial={true} secondary width="25px" height="25px"/>
              </Tooltip>&nbsp;
              <Tooltip title="Cancelar">
                <Button
                  icon={{ name: 'cross', direction: 'left', height: '9' }}
                  onClick={this.onDeleteQuestion}
                  radial={true} background="#807f7f" width="25px" height="25px"/>
              </Tooltip>
            </div>
          }
        />
        <div className="b-controls-question_options">
        <DynamicFieldFormik
            schema={`${this.FORMIK_FIELDSCHEMA}.${this.props.index}.choices`}
            fieldSchema="value"
            itemsList={this.props.defaultValues.choices}
            maxFields={5}
            maxLength={50}
            titleField="Respuesta"
            extraInput={{
              type: 'radio',
              name: 'choices',
            }}
            textPlaceholder="Ejemplo: Programación en JAVA"
            isSubmitting={this.props.isSubmitting}
            errors={this.props.errors['questions'] &&
                    this.props.errors['questions'][this.props.index] &&
                    this.props.errors['questions'][this.props.index]['choices']}
            touched={this.props.touched['questions'] &&
                    this.props.touched['questions'][this.props.index] &&
                    this.props.touched['questions'][this.props.index]['choices']}
            setFieldValue={this.props.setFieldValue}
            handleBlur={this.props.handleBlur}
            handleChange={this.props.handleChange}
            hasErrorClass={this.props.hasErrorClass}
            hasErrorElement={this.props.hasErrorElement}
            onBlurText={this.props.handleBlur}
            ref={ref => this.refDynamicField = ref}
          />
        </div>
      </React.Fragment>
    );
  }
}
