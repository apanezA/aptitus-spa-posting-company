import * as React from 'react';
import { Button } from 'aptitus-components/Components/Button';
import { QUESTIONS }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.const';
import { QuestionLayout } from '@app/src/views/postingJob/questionsJob/containers/questionLayout';
import './index.scss';
import { Tooltip } from 'aptitus-components/Components/Tooltip';

export const OptionReadMode = ({ text, title, handlerClick }) => {
  return (
    <QuestionLayout.Body
      title={title}
      input={text}
      options={
        <div className="b-controls-bar">
          <Tooltip title="Editar">
            <Button
              icon={{ name: 'edit', direction: 'left' , height: '12' }}
              onClick={() => handlerClick(QUESTIONS.actionsListAdded.edit)}
              radial={true} secondary width="25px" height="25px"/>
          </Tooltip>&nbsp;
          <Tooltip title="Eliminar">
            <Button
              icon={{ name: 'cross', direction: 'left', height: '12' }}
              onClick={() => handlerClick(QUESTIONS.actionsListAdded.delete)}
              radial={true} background="#807f7f" width="25px" height="25px"/>
          </Tooltip>
        </div>
      }
    />
  );
};
