import * as React from 'react';
import { QUESTIONS }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.const';
import { IQuestionValue }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.interfaces';
import { OptionOpenedQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionOpenedQuestion';
import { OptionPredefinedQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionPredefinedQuestion';
import { OptionMultipleQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionMultipleQuestion';
import { OptionReadMode }
  from '@app/src/views/postingJob/questionsJob/containers/optionReadQuestion';

export const QuestionBuilder =
  ({ index, element, schema, handlerClick, isSubmitting, setFieldValue, formData,
    errors, touched, handleChange, handleBlur, submitForm }) => {
    let html: JSX.Element;

    const onAddItem = (question: IQuestionValue) => {
      // Aqui se setea al arreglo principal, la data que puse en modo Edición
      element.question = question;
      handlerClick(element, 'edit');
    };

    const onRemoveItem = () => {
      handlerClick(element, 'delete');
    };

    const onAddItemFromSelect = (action: string) => {
      handlerClick(element, action);
    };

    if (element.question.mode === 'edit' || element.question.mode === 'temp') {
      switch (element.question.type) {
        case QUESTIONS.type.opened:
          html = (
            <OptionOpenedQuestion
              index={index}
              schema={schema}
              handlerDelete={onRemoveItem}
              handlerAdd={onAddItem}
              defaultValues={element.question}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
              isSubmitting={isSubmitting}
            />
          );
          break;
        case QUESTIONS.type.predefined:
          html = (
            <OptionPredefinedQuestion
              index={index}
              schema={schema}
              handlerDelete={onRemoveItem}
              handlerAdd={onAddItem}
              defaultValues={element.question}
              questionsList={formData.question_type.suggested.questions}
              errors={errors}
              touched={touched}
              handleChange={handleChange}
              handleBlur={handleBlur}
              isSubmitting={isSubmitting}
            />
          );
          break;
        case QUESTIONS.type.multiple:
          html = (
            <OptionMultipleQuestion
              index={index}
              schema={schema}
              handlerDelete={onRemoveItem}
              handlerAdd={onAddItem}
              defaultValues={element.question}
              errors={errors}
              touched={touched}
              setFieldValue={setFieldValue}
              handleChange={handleChange}
              handleBlur={handleBlur}
              isSubmitting={isSubmitting}
              submitForm={submitForm}
            />
          );
          break;
      }
    } else {
      html = (
        <OptionReadMode
          text={element.question.name}
          title={`Pregunta ${index + 1}:`}
          handlerClick={onAddItemFromSelect}
        />
      );
    }
    return html;
  };
