import * as React from 'react';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { Icon } from 'aptitus-components/Components/Icon';
import { QUESTIONS }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.const';
import { Form } from 'aptitus-components/Components/Form';

export class QuestionHeader extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.onSelectChange = this.onSelectChange.bind(this);
  }

  setSelectQuestionData(): object[] {
    const data = this.props.formData.question_type || {};
    const keys = Object.keys(data);
    return keys.map(item =>  data[item]);
  }

  getTypeQuestion(value) {
    let type = '';
    switch (value) {
      case 'open':
        type = QUESTIONS.type.opened;
        break;
      case 'suggested':
        type = QUESTIONS.type.predefined;
        break;
      case 'alternative':
        type = QUESTIONS.type.multiple;
        break;
    }
    return type;
  }

  getHasQuestionTemp() {
    let hasQuestionTemp = false;
    const questions = this.props.userData['questions'];
    const questionsKeys = Object.keys(questions);
    questionsKeys.map((item) => {
      return (questions[item].mode === 'temp') && (hasQuestionTemp = true);
    });
    return hasQuestionTemp;
  }

  onSelectChange(event) {
    const errors = this.props.errors;
    const hasErrors = errors && Object.keys(errors).length ? true : false;
    const hasQuestionTemp = this.getHasQuestionTemp();
    if (hasErrors && hasQuestionTemp) {
      const keys = Object.keys(errors);
      keys.length || this.props.addTemplateList(this.getTypeQuestion(event.target.value));
    } else {
      this.props.addTemplateList(this.getTypeQuestion(event.target.value));
    }
    event.target.value = '';
  }

  render() {
    return(
      <React.Fragment>
        <Form.Wrapper type="normal">
          <Form.Row>
            <Form.Column>
              <Form.Control title="Agregar pregunta: ">
                <SelectForm
                  id={'selTypeQuestions'}
                  name={'selTypeQuestions'}
                  defaultText="Seleccionar"
                  value=""
                  onChange={this.onSelectChange}
                  data={this.setSelectQuestionData()} />
              </Form.Control>
            </Form.Column>
            <Form.Column>
              <div className="b-controls-question_information">
                <Icon data-name="information" fill="#f3d044" />
                <span className="b-controls-question_information-text">
                  Puedes agregar hasta 5 preguntas para obtener
                  información específica de los postulantes.
                </span>
              </div>
            </Form.Column>
          </Form.Row>
        </Form.Wrapper>
      </React.Fragment>
    );
  }
}
