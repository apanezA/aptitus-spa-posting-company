import * as React from 'react';
import '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.scss';
import { Button } from 'aptitus-components/Components/Button';
import { IOptionQuestionProps, IQuestionValue }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.interfaces';
import { QuestionLayout }
  from '@app/src/views/postingJob/questionsJob/containers/questionLayout';
import { SelectForm } from 'aptitus-components/Components/SelectForm';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { Tooltip } from 'aptitus-components/Components/Tooltip';

export class OptionPredefinedQuestion extends
  React.Component<IOptionQuestionProps, any> {
  ref: HTMLSelectElement;
  FORMIK_FIELDSCHEMA: string;

  constructor(props: IOptionQuestionProps) {
    super(props);
    this.onAddQuestion = this.onAddQuestion.bind(this);
    this.onDeleteQuestion = this.onDeleteQuestion.bind(this);
    this.onChangeSelect = this.onChangeSelect.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.FORMIK_FIELDSCHEMA = 'questions';
  }

  onAddQuestion() {
    if (this.ref.value !== '') {
      const question = this.getStructureQuestion();
      this.props.handlerAdd(question);
    }
  }

  getStructureQuestion() {
    return {
      id: this.props.defaultValues.id,
      name: (this.ref) ? this.ref.options[this.ref.selectedIndex].text :
        this.props.defaultValues.name,
      type: this.props.defaultValues.type,
      mode: this.props.defaultValues.mode,
    };
  }

  onDeleteQuestion() {
    this.props.handlerDelete(null);
  }

  onChangeSelect(event) {
    this.ref = event.target;
    this.props.handleChange(event);
  }

  getQuestionList() {
    return this.props.questionsList.map((questionItem) => {
      return {
        value: questionItem['label'],
        label: questionItem['label'],
      };
    });
  }

  setErrorClass() {
    const errorField = this.props.errors[this.FORMIK_FIELDSCHEMA];
    const touchedField = this.props.touched[this.FORMIK_FIELDSCHEMA];

    const isError = errorField && errorField[this.props.index] &&
          (errorField[this.props.index].name || errorField[this.props.index].mode);
    const isTouched = touchedField && touchedField[this.props.index] &&
          (touchedField[this.props.index].name || touchedField[this.props.index].mode);

    return (isError && isTouched) ? 'is-error' : '';
  }

  setErrorMessage() {
    const errorField = this.props.errors[this.FORMIK_FIELDSCHEMA];
    const errorMessage = (errorField && errorField[this.props.index]) ?
        errorField[this.props.index].name ||
        errorField[this.props.index].mode : '';
    return errorMessage;
  }

  setActiveTouched() {
    const touchedField = this.props.touched[this.FORMIK_FIELDSCHEMA];
    const touchedIsActive = (touchedField && touchedField[this.props.index]) ?
    touchedField[this.props.index].name ||
    touchedField[this.props.index].mode : '';
    return touchedIsActive;
  }

  onBlur(event) {
    this.ref = event.target;
    this.props.handleBlur(event);
  }

  render() {
    return (
      <React.Fragment>
        <QuestionLayout.Header title={'Pregunta predefinida'} />
        <QuestionLayout.Body
          title={'Pregunta:'}
          input={
            <React.Fragment>
              <SelectForm
                name={`${this.props.schema}.${this.props.index}.name`}
                onChange={this.onChangeSelect}
                onBlur={this.onBlur}
                defaultValue={this.props.defaultValues.name}
                defaultText="Seleccionar"
                data={this.getQuestionList()}
                className={this.setErrorClass()}
                autoFocus
              />
              <TooltipErrorForm error={this.setErrorMessage()}
                touched={this.setActiveTouched()}
                isSubmitting={this.props.isSubmitting} />
            </React.Fragment>
          }
          options={
            <div className="b-controls-bar">
              <Tooltip title="Guardar">
                <Button
                  icon={{ name: 'check3', direction: 'left' , height: '12' }}
                  onClick={this.onAddQuestion}
                  radial={true} secondary width="25px" height="25px"/>
              </Tooltip>&nbsp;
              <Tooltip title="Cancelar">
                <Button
                  icon={{ name: 'cross', direction: 'left', height: '9' }}
                  title="Cancelar"
                  onClick={this.onDeleteQuestion}
                  radial={true} background="#807f7f" width="25px" height="25px"/>
              </Tooltip>
            </div>
          }
        />
      </React.Fragment>
    );
  }
}
