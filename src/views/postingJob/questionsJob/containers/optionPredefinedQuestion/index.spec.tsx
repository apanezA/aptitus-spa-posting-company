import * as React from 'react';
import { shallow, mount, render } from 'enzyme';
import { OptionPredefinedQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionPredefinedQuestion';
import { QuestionsBox }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/dataMockSpec';
import { SelectForm } from 'aptitus-components/Components/SelectForm';

describe('The OptionPredefinedQuestion component', () => {
  describe('Render snapshot', () => {
    const props = {
      index: 0,
      schema: '',
      defaultValues: {
        name: '',
        id: 12312312,
        type: 'suggested',
      },
      questionsList: QuestionsBox.suggested.questions,

      handlerAdd: () => {},
      handlerDelete: () => {},
      isSubmitting: false,
      touched: {},
      errors: {},
    };

    describe('Test Opened Question', () => {
      test('Test structure of question when add question', () => {
        const mockQuestion = {
          id: 231232,
          name: '',
          type: '',
          mode: '',
        };
        const wrapper = shallow(<OptionPredefinedQuestion {...props}/>);
        const instance = wrapper.instance();
        const question = instance.getStructureQuestion();
        expect(mockQuestion).not.toBe(question);
      });
    });
  });
});
