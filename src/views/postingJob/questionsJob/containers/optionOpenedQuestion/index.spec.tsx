import * as React from 'react';
import { shallow, mount, render } from 'enzyme';
import { OptionOpenedQuestion }
  from '@app/src/views/postingJob/questionsJob/containers/optionOpenedQuestion';

describe('The OptionOpenedQuestion component', () => {
  const props = {
    index: 0,
    schema: '',
    isSubmitting: false,
    touched: {},
    errors: {},
    defaultValues: {
      name: 'pregunta',
      id: 231232,
      type: 'open',
      mode: 'temp',
    },

    handlerAdd: () => {},
    handlerDelete: () => {},
  };

  describe('Test Opened Question', () => {
    test('Test structure of question when add question', () => {
      const mockQuestion = {
        id: 231232,
        name: '',
        type: '',
        mode: '',
      };
      const wrapper = shallow(<OptionOpenedQuestion {...props}/>);
      const instance = wrapper.instance();
      const question = instance.getStructureQuestion('titulo de pregunta');
      expect(mockQuestion).not.toBe(question);
    });
  });
});
