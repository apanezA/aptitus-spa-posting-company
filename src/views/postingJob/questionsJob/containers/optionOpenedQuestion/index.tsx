import * as React from 'react';
import '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.scss';
import { Button } from 'aptitus-components/Components/Button';
import { IOptionQuestionProps }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer/index.interfaces';
import { QuestionLayout }
  from '@app/src/views/postingJob/questionsJob/containers/questionLayout';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';
import { Tooltip } from 'aptitus-components/Components/Tooltip';
import { CountWords } from 'aptitus-components/Components/CountWords';

export class OptionOpenedQuestion extends React.Component<IOptionQuestionProps, any> {
  ref: HTMLInputElement;
  refCountOpenedQuestion: any;
  FORMIK_FIELDSCHEMA: string;

  constructor(props: IOptionQuestionProps) {
    super(props);
    this.FORMIK_FIELDSCHEMA = 'questions';
    this.refCountOpenedQuestion = React.createRef();
    this.onAddQuestion = this.onAddQuestion.bind(this);
    this.onDeleteQuestion = this.onDeleteQuestion.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onInputText = this.onInputText.bind(this);
    this.onBlurText = this.onBlurText.bind(this);
  }

  onChange() {
    this.refCountOpenedQuestion &&
    this.refCountOpenedQuestion.current.counter(this.ref);
  }

  onAddQuestion(value: any) {
    if (value !== '') {
      const question = this.getStructureQuestion(this.ref.value);
      this.props.handlerAdd(question);
    }
  }

  getStructureQuestion(value) {
    return {
      id: this.props.defaultValues.id,
      name: value,
      type: this.props.defaultValues.type,
      mode: this.props.defaultValues.mode,
    };
  }

  onDeleteQuestion() {
    this.props.handlerDelete(null);
  }

  onInputText(event) {
    if (event.keyCode === 13) {
      this.onAddQuestion(this.ref.value.trim());
    }
    event.preventDefault();
  }

  onBlurText(event) {
    this.props.handleChange(event);
    this.props.handleBlur(event);
  }

  setErrorClass() {
    const errorField = this.props.errors[this.FORMIK_FIELDSCHEMA];
    const touchedField = this.props.touched[this.FORMIK_FIELDSCHEMA];

    const isError = errorField && errorField[this.props.index] &&
          (errorField[this.props.index].name || errorField[this.props.index].mode);
    const isTouched = touchedField && touchedField[this.props.index] &&
          (touchedField[this.props.index].name || touchedField[this.props.index].mode);

    return (isError && isTouched) ? 'is-error' : '';
  }

  setErrorMessage() {
    const errorField = this.props.errors[this.FORMIK_FIELDSCHEMA];
    const errorMessage = (errorField && errorField[this.props.index]) ?
        errorField[this.props.index].name ||
        errorField[this.props.index].mode : '';
    return errorMessage;
  }

  setActiveTouched() {
    const touchedField = this.props.touched[this.FORMIK_FIELDSCHEMA];
    const touchedIsActive = (touchedField && touchedField[this.props.index]) ?
    touchedField[this.props.index].name ||
    touchedField[this.props.index].mode : '';
    return touchedIsActive;
  }

  render() {
    return (
      <React.Fragment>
        <QuestionLayout.Header title={'Pregunta abierta'} />
        <QuestionLayout.Body
          title={'Pregunta:'}
          input={
            <React.Fragment>
              <input
                type="text"
                name={`questions.${this.props.index}.name`}
                ref={(ref) => { this.ref = ref; }}
                defaultValue={this.props.defaultValues.name}
                onInput={this.onInputText}
                onBlur={this.onBlurText}
                onChange={this.onChange }
                className={this.setErrorClass()}
                placeholder="Ejemplo: ¿Por qué te gustaria trabajar en nuestra empresa?"
                maxLength={100}
                autoFocus />
              <CountWords
                ref={this.refCountOpenedQuestion}
                maxCharacters={100}
                counterAlign="left"
                alternativeText="Quedan "
              />
              <TooltipErrorForm error={this.setErrorMessage()}
                touched={this.setActiveTouched()}
                isSubmitting={this.props.isSubmitting} />
            </React.Fragment>
          }
          options={
            <div className="b-controls-bar">
              <Tooltip title="Guardar">
                <Button
                  icon={{ name: 'check3', direction: 'left' , height: '12' }}
                  onClick={() => this.onAddQuestion(this.ref.value.trim())}
                  radial={true} secondary width="25px" height="25px"/>
              </Tooltip>&nbsp;
              <Tooltip title="Cancelar">
                <Button
                  icon={{ name: 'cross', direction: 'left', height: '9' }}
                  onClick={this.onDeleteQuestion}
                  radial={true} background="#807f7f" width="25px" height="25px"/>
              </Tooltip>
            </div>
          }
        />
      </React.Fragment>
    );
  }
}
