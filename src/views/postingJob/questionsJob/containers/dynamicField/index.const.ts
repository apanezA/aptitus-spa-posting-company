export interface IItem {
  id: string | number;
  value: string;
  checked?: boolean;
}

export interface IItemComponent {
  identifier: string;
  item: IItem;
}
