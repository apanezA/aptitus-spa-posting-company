import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { DynamicField } from './index';
import { DynamicFieldItem } from './DynamicFieldItem/index';

describe('The FieldContainer component', () => {
  describe('Render snapshot', () => {
    it('Render correctly', () => {
    });

    it('Should render one <FieldContainer /> component width 2 FieldItem', () => {
    });

    it(
      'Should render one <FieldContainer /> component with a FieldItem without delete button',
      () => {},
    );

    it(
      'Should render one <FieldContainer /> component with a FieldItem without add button',
      () => {},
    );
  });
});
