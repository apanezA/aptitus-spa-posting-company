import * as React from 'react';
import { shallow, mount } from 'enzyme';
import { DynamicFieldItem } from './index';
import { Icon } from 'aptitus-components/Components/Icon';

describe('The FieldItem component', () => {
  const props = {
    index: 2,
    item: {
      identifier: '212312',
      item: {
        id: 1,
        value: 'Lorem ipsum',
      },
    },
    schema: 'foo',
    isNotLastChild: true,
    isOnlyChild: false,
    fieldSchema: 'fooSchema',
    titleField: '',
    maxLength: 5,
    onAddItem: () => {},
    onRemoveItem: () => {},
    onChangeInpuText: () => {},
    onChangeInputBox: () => {},
  };

  const extraInput = {
    type: 'radio',
    name: 'choices',
  };

  describe('Render snapshot', () => {
    it('Render correctly', () => {
      const wrapper = shallow(<DynamicFieldItem {...props}/>);
      expect(wrapper.html()).toMatchSnapshot();
    });
  });

  describe('shallow test', () => {
    it('Should render one <DynamicFieldItem /> component with prop item as ArrayObject', () => {
      const itemExpect = {
        id: 1,
        value: 'Lorem ipsum',
      };
      const wrapper = mount(<DynamicFieldItem {...props} />);
      expect(wrapper.props().item.item).toMatchObject(itemExpect);
    });

    it('Should render one <DynamicFieldItem /> component width two <Icon />', () => {
      const wrapper = mount(<DynamicFieldItem {...props}/>);
      expect(wrapper.find(Icon)).toHaveLength(2);
    });

    it('Should return snapshot with checkbox <DynamicFieldItem />', () => {
      const wrapper = mount(<DynamicFieldItem {...props} extraInput={extraInput}/>);
      expect(wrapper.html()).toMatchSnapshot();
    });

    it('Should return radio button in <DynamicFieldItem />', () => {
      const wrapper = mount(<DynamicFieldItem {...props} extraInput={extraInput}/>);
      expect(wrapper.find('input[type="radio"]')).toHaveLength(1);
    });

    it('Should not return "extra input" in <DynamicFieldItem />', () => {
      const wrapper = mount(<DynamicFieldItem {...props}/>);
      expect(wrapper.find('input[type="radio"]')).toHaveLength(0);
    });

    it('Should return value with 5 characters', () => {

    });
  });

});
