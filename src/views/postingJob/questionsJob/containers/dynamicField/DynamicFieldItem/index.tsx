import * as React from 'react';
import './index.scss';
import { Button } from 'aptitus-components/Components/Button';
import { IItemComponent }
  from '@app/src/views/postingJob/questionsJob/containers/dynamicField/index.const';
import { Tooltip } from 'aptitus-components/Components/Tooltip';

interface IDynamicFieldItemProps {
  item: IItemComponent;
  isNotLastChild: boolean;
  isOnlyChild: boolean;
  index: number;
  schema: string;
  fieldSchema: string;
  titleField: string;
  extraInput?: object | null;
  textPlaceholder?: string;
  extraActions?: object | null;
  maxLength?: number;
  onAddItem(): void;
  onRemoveItem(identifier: string): void;
  onChangeInpuText(identifier: string, value: string): void;
  onBlurInputText?(event: any): void;
  onChangeInputBox(identifier: string, checked: boolean): void;

  hasErrorClass?(index?: number);
  hasErrorElement?(index?: number);
}

interface IDynamicFieldItemState {
  showTooltip: boolean;
}

export class DynamicFieldItem
  extends React.Component<IDynamicFieldItemProps, IDynamicFieldItemState> {

  extraInputRef: any;
  constructor(props: IDynamicFieldItemProps) {
    super(props);
    this.state = {
      showTooltip: false,
    };
    this.onAdd = this.onAdd.bind(this);
    this.onRemove = this.onRemove.bind(this);
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onChangeInpuText = this.onChangeInpuText.bind(this);
    this.onChangeInputBox = this.onChangeInputBox.bind(this);
    this.onBlurInputText = this.onBlurInputText.bind(this);
    this.onShowTooltip = this.onShowTooltip.bind(this);
    this.onHideTooltip = this.onHideTooltip.bind(this);
    this.extraInputRef = React.createRef();
  }

  onAdd() {
    this.props.onAddItem();
  }

  onRemove() {
    this.props.onRemoveItem(this.props.item.identifier);
  }

  onChangeInpuText(event) {
    this.props.onChangeInpuText(this.props.item.identifier, event);
  }

  onKeyDown(event) {
    event.stopPropagation();
    if (event.keyCode === 13 && this.props.isNotLastChild) {
      this.props.onAddItem();
    }
  }

  onChangeInputBox(event) {
    this.props.onChangeInputBox(this.props.item.identifier, event);
  }

  onBlurInputText(event) {
    this.props.onBlurInputText && this.props.onBlurInputText(event);
  }

  onShowTooltip() {
    if (this.extraInputRef.current.checked) {
      this.setState({
        showTooltip: true,
      });
    }
  }

  onHideTooltip() {
    this.setState({
      showTooltip: false,
    });
  }

  render() {
    const {
      extraInput,
      item,
      textPlaceholder,
      maxLength,
      titleField,
      index,
      schema,
      fieldSchema,
      isNotLastChild,
      isOnlyChild,
      hasErrorClass,
      hasErrorElement,
    } = this.props;
    return (
      <div className="b-controls-fields">
        <Tooltip
          title="Respuesta correcta"
          open={this.state.showTooltip}>
          <label className="g-form_label"
            htmlFor={item.identifier.toString()}
            onMouseEnter={extraInput && this.onShowTooltip}
            onMouseLeave={extraInput && this.onHideTooltip}
            onClick={extraInput && this.onShowTooltip}>
            <span className="b-controls-fields_text">
              { extraInput &&
                <input
                  ref={this.extraInputRef}
                  id={item.identifier.toString()}
                  className="b-controls-fields_extra-input"
                  onChange={this.onChangeInputBox}
                  {...extraInput}
                  defaultChecked={item.item.checked}
                /> }
              {titleField} {index + 1}:
            </span>
          </label>
        </Tooltip>
        <div className="g-form_control">
          <div className="b-controls-fields_input">
            <input
              type="text"
              name={`${schema}.${index}.${fieldSchema}`}
              maxLength={maxLength}
              defaultValue={item.item.value}
              onInput={this.onChangeInpuText}
              placeholder={textPlaceholder}
              onBlur={this.onBlurInputText}
              onKeyDown={this.onKeyDown}
              className={hasErrorClass && hasErrorClass(index)}
            />
            { hasErrorElement && hasErrorElement(index) }
          </div>
          <div className="b-controls-bar">
            { isNotLastChild &&
              <Tooltip title="Agregar">
                <Button
                className="b-controls-bar_button"
                onClick={this.onAdd}
                icon={{
                  name: 'plus-2',
                  height: '12',
                  width: '12',
                  direction: 'left',
                }}
                radial={true}
                width="25px"
                height="25px"
                secondary
                />
              </Tooltip>
            }
            <Tooltip title="Eliminar">
            { !isOnlyChild &&
              <Tooltip title="Eliminar">
                <Button
                  className="b-controls-bar_button"
                  onClick={this.onRemove}
                  icon={{
                    name: 'delete2',
                    height: '12',
                    width: '12',
                    direction: 'left',
                  }}
                  radial={true}
                  width="25px"
                  height="25px"
                  background="#807f7f"
                  />
              </Tooltip>
            }
            </Tooltip>
          </div>
        </div>
      </div>
    );
  }
}
