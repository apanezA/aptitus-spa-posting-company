import * as React from 'react';
import { DynamicFieldItem } from './DynamicFieldItem';
import { IItem, IItemComponent } from './index.const';
import { Utils } from 'aptitus-components/Utils';
import { withFormik, IPropsHOC } from '@app/src/views/postingJob/formPosting/components/withFormik';

interface IProps extends IPropsHOC {
  itemsList: IItem[];
  maxFields: number;
  titleField: string;
  extraInput?: {
    type: string;
    name: string;
    [propName: string]: any;
  };
  schema: string;
  textPlaceholder?: string;
  fieldSchema: string;
  maxLength?: number;
  errors: object;
  touched: object;
  updateErrorsBlur(): void;
  setFieldValue(field: string, value: any, shouldValidate?: boolean) : void;
}

interface IState {
  items: IItemComponent[];
}

export class DynamicField extends React.Component<IProps, IState> {
  public state: IState;

  constructor(props: IProps) {
    super(props);
    this.onAddItem = this.onAddItem.bind(this);
    this.onRemoveItem = this.onRemoveItem.bind(this);
    this.onChangeInpuText = this.onChangeInpuText.bind(this);
    this.onChangeInputBox = this.onChangeInputBox.bind(this);
    this.state = {
      items: this.getInitialItems(this.props.itemsList),
    };
  }

  getInitialItems(itemsList) {
    if (itemsList && itemsList.length) {
      return itemsList.map((element) => {
        return this.getNewItem(element);
      });
    }
    return [this.getNewItem(null, true)];
  }

  getNewItem(element?: IItem, firstElement: boolean = false): IItemComponent {
    const newItem: IItem = {
      id: null,
      value: '',
    };

    if (this.props.extraInput && this.props.extraInput.type === 'radio') {
      // Valido si es el primer input para agregarle el checked por defecto
      newItem.checked = (firstElement) ? true : false;
    }

    return {
      identifier: Utils.uuID(),
      // En caso que no se envie 'element', toma un item vacio por defecto
      item: element || newItem,
    };
  }

  getItems(items = this.state.items) {
    return items.map(element => element.item);
  }

  onAddItem() {
    const items = [...this.state.items];
    items.push(this.getNewItem());
    this.setState({ items });
    this.props.updateSchemaValue && this.props.updateSchemaValue(this.getItems(items), true);
  }

  onRemoveItem(id) {
    let items = [...this.state.items];
    items = items.filter(element => element.identifier !== id);
    this.setState({ items });
    this.props.setFieldValue(this.props.schema, items);
  }

  onChangeInpuText(identifier, event) {
    let items = [...this.state.items];
    items = items.map((element) => {
      if (element.identifier === identifier) {
        element.item.value = event.target.value;
      }
      return element;
    });
    this.setState({ items });
    this.props.updateErrorsChange && this.props.updateErrorsChange(event);
  }

  onChangeInputBox(identifier, event) {
    const items = this.state.items.map((element) => {
      element.item.checked = false;
      if (element.identifier === identifier) {
        element.item.checked = event.target.checked;
      }
      return element;
    });
    this.setState({ items });
    this.props.updateErrorsChange && this.props.updateErrorsChange(event);
  }

  render() {
    const { titleField, maxFields, extraInput, schema, fieldSchema } = this.props;
    const items = this.state.items;
    return items.map((item, index) => {
      const notLastChild = (index + 1 === items.length && index + 1 < maxFields);
      const onlyChild = items.length === 1;
      extraInput.name = extraInput.name + item.identifier;
      return (
        <DynamicFieldItem
          key={item.identifier}
          index={index}
          item={item}
          isNotLastChild={notLastChild}
          isOnlyChild={onlyChild}
          schema={schema}
          fieldSchema={fieldSchema}
          titleField={titleField}
          maxLength={this.props.maxLength}
          textPlaceholder={this.props.textPlaceholder}
          extraInput={extraInput}
          onAddItem={this.onAddItem}
          onRemoveItem={this.onRemoveItem}
          onChangeInpuText={this.onChangeInpuText}
          onChangeInputBox={this.onChangeInputBox}

          onBlurInputText={this.props.updateErrorsBlur}
          hasErrorClass={this.props.hasErrorClass}
          hasErrorElement={this.props.hasErrorElement}
        />
      );
    });
  }
}
export const DynamicFieldFormik = withFormik(DynamicField);
