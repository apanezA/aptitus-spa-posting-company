import * as React from 'react';
import { Accordion } from 'aptitus-components/Components/Accordion';
import { Form } from 'aptitus-components/Components/Form';
import { Title } from '@app/src/views/common/styledCommon';
import { QuestionsJobsContainer }
  from '@app/src/views/postingJob/questionsJob/containers/questionJobContainer';
import { withPostingContext } from '@app/src/views/postingJob/formPosting/state/store';
export const SectionQuestions: React.SFC<any> = (props: any) => (
  <Accordion statePanel={true}
    arrowPositionLeft = {true}
    header={<Title className="b-process-info_title"> Preguntas</Title>}
    body={
      <div className="g-form g-form--posting">
      <Form.Wrapper type="normal">
        <Form.Row>
          <Form.Column>
            <QuestionsJobsContainer {...props} />
          </Form.Column>
        </Form.Row>
      </Form.Wrapper>
      </div>
    }
  />
);

export const SectionQuestionsContainer = withPostingContext(SectionQuestions);
