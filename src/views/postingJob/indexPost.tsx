import * as React from 'react';
import { ThemeProvider } from '@app/config/theme/config';
import { theme } from '@app/config/theme/themes';
import { Eplanning, EplanningProvider, IEplConfig } from 'aptitus-components/Components/Eplanning';
import { PostingFormikContainer }
from '@app/src/views/postingJob/formPosting/containers/postingFormikContainer';
import { NewJobHOC }
from '@app/src/views/postingJob/formPosting/containers/postJobContainer';
import { Wrapper } from '@app/src/views/postingJob/styled';

interface IPostJobContainerState {
  show: boolean;
}

export class PostJobContainer extends React.Component<any, IPostJobContainerState> {
  layoutProperties: {[key: string]: any};
  eplConfig: IEplConfig;
  constructor(props: any) {
    super(props);
    this.state = {
      show: false,
    };
    this.eplConfig = {
      sI: '230e4',
      sec: 'DetalleAviso',
      kVs: {
        tipo_usuario: 'empresa',
      },
      eIs: ['Top', 'Right1', 'Bottom', 'Pestaña'],
    };
  }

  componentWillMount() {
    this.setState({ show: true });
  }

  render() {
    const FormPostingNew = NewJobHOC(PostingFormikContainer);

    return (
      <EplanningProvider eplConfig={this.eplConfig}>
        <Eplanning section="Middle2"/>
        { this.state.show && <ThemeProvider theme={theme}>
          <React.Fragment>
            <Wrapper><FormPostingNew/></Wrapper>
          </React.Fragment>
        </ThemeProvider> }
      </EplanningProvider>
    );
  }
}
