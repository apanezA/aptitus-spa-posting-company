import * as React from 'react';
import { ThemeProvider } from '@app/config/theme/config';
import { ITheme, theme } from '@app/config/theme/themes';
import { Eplanning, EplanningProvider, IEplConfig } from 'aptitus-components/Components/Eplanning';
import { PostingFormikContainer }
from '@app/src/views/postingJob/formPosting/containers/postingFormikContainer';
import { EditJobHOC }
from '@app/src/views/postingJob/formPosting/containers/editJobContainer';
import { Wrapper } from '@app/src/views/postingJob/styled';

interface IEditJobContainerState {
  show: boolean;
}

export class EditJobContainer extends React.Component<any, IEditJobContainerState> {
  layoutProperties: {[key: string]: any};
  theme: ITheme;
  eplConfig: IEplConfig;
  constructor(props: any) {
    super(props);
    this.state = {
      show: false,
    };
    this.eplConfig = {
      sI: '230e4',
      sec: 'DetalleAviso',
      kVs: {
        tipo_usuario: 'empresa',
      },
      eIs: ['Top', 'Right1', 'Bottom', 'Pestaña'],
    };
  }

  componentWillMount() {
    this.setState({ show: true });
  }

  render() {
    const FormPostingEdit = EditJobHOC(PostingFormikContainer);
    return (
      <EplanningProvider eplConfig={this.eplConfig}>
      <Eplanning section="Middle2"/>
        { this.state.show && <ThemeProvider theme={theme}>
          <React.Fragment>
            <Wrapper><FormPostingEdit /></Wrapper>
          </React.Fragment>
        </ThemeProvider> }
      </EplanningProvider>
    );
  }
}
