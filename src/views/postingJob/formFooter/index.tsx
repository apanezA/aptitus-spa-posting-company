import * as React from 'react';
import { Button } from 'aptitus-components/Components/Button';
import { Modal } from 'aptitus-components/Components/Modal';
import './index.scss';
import { ModalBack } from '@app/src/views/common/modalBack';

interface IFormFooterProps {
  changeSchema(schema: string): void;
  textButton: string;
  publishBtnProps: object;
  displayButtonDraft: boolean;
  isSubmitting: boolean;
  isValid: boolean;
}

export class FormFooter extends React.Component<IFormFooterProps, any> {
  modalBack: Modal;
  constructor(props: IFormFooterProps) {
    super(props);
    this.onDraftButton = this.onDraftButton.bind(this);
    this.onAddButton = this.onAddButton.bind(this);
    this.onShowModal = this.onShowModal.bind(this);
    this.onCloseModal = this.onCloseModal.bind(this);
    this.onRedirectPage = this.onRedirectPage.bind(this);
  }

  onDraftButton() {
    if (this.props.changeSchema) {
      this.props.changeSchema('schemaDraft');
    }
  }
  onAddButton() {
    if (this.props.changeSchema) {
      this.props.changeSchema('schemaAdd');
    }
  }

  onShowModal() {
    this.modalBack.open();

  }
  onCloseModal() {
    this.modalBack.close();
  }

  onRedirectPage() {
    window.location.href = '/empresa/mis-procesos/';
  }

  showButtonDraft() {
    if (this.props.displayButtonDraft) {
      return (
        <Button
          buttonLink
          text="Guardar Borrador"
          type="submit"
          color="#989898"
          onClick={this.onDraftButton} />
      );
    }
  }

  render() {
    return (
      <div className="b-process-info_footer">
        <div className="b-process-info_footer-column">
          <Button
            background="#807f7f"
            text="Regresar"
            type="button"
            icon={{ name: 'previous_page', direction: 'left' }}
            onClick={() => { this.onShowModal(); }} />
          {this.showButtonDraft()}
        </div>
        <div className="b-process-info_footer-column">
          <Button primary
            text={this.props.textButton}
            type="submit"
            icon={{ name: 'next_page', direction: 'right' }}
            onClick={this.onAddButton}
            {...this.props.publishBtnProps} />
        </div>
        <Modal ref={(input) => { this.modalBack = input; }}>
          <ModalBack closeModal={this.onCloseModal} redirect={this.onRedirectPage} />
        </Modal>
      </div>
    );
  }
}
