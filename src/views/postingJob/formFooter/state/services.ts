import { Utils } from 'aptitus-components/Utils';
import axios from 'axios';

const idPost = Utils.getUrlByAlias(window.location.href, 'id');

export const jobOfferService = {
  async createAds(data) {
    try {
      const request = await axios
        .post(`${process.env.APP_URL}/empresa/publica-aviso-web/save-ajax`, data);
      return request;
    } catch (e) {
      throw new Error();
    }
  },

  async createDraft(data) {
    try {
      const request = await axios
        .post(`${process.env.APP_URL}/empresa/publica-aviso-web/save-draft-ajax`, data);
      return request;
    } catch (e) {
      throw new Error();
    }
  },

  async getAds() {
    try {
      const { request } = await axios
        .get(`${process.env.APP_URL}/empresa/publica-aviso-web/jobs/${idPost}/`);
      return request;
    } catch (e) {
      throw new Error();
    }
  },

  async updateAds(data) {
    try {
      const request = await axios
        .put(`${process.env.APP_URL}/empresa/publica-aviso-web/edit-ajax/${idPost}`, data);
      return request;
    } catch (e) {
      throw new Error();
    }
  },

  async republish(data) {
    try {
      const { request } = await axios
        .post(`${process.env.APP_URL}/empresa/publica-aviso-web/save-ajax`, data);
      return request;
    } catch (e) {
      throw new Error();
    }
  },

};
