import styled from 'styled-components';

export const Wrapper = styled.div`
  margin: 25px 0;
  min-height: calc(100vh - 155px);
  min-width: 1200px;
`;
