import * as React from 'react';
import './index.scss';
import { Title } from '@app/src/views/common/styledCommon';

interface IFormHeaderProps {
  title: string;
}

export const FormHeader: React.SFC<IFormHeaderProps> = ({ title, children }) => {
  return (
    <div className="b-process-header">
      <div className="b-process-header_column">
        <Title className="b-process-header_title">{title}</Title>
      </div>
      {children}
    </div>
  );
};
