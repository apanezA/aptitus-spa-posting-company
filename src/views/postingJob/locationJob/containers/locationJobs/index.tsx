import * as React from 'react';
import { Field } from 'formik';
import { Autocomplete } from 'aptitus-components/Components/Autocomplete';
import { Form } from 'aptitus-components/Components/Form';
import { TooltipErrorForm } from 'aptitus-components/Components/TooltipErrorForm';

interface Props {
  aliasId: string;
  aliasDisplay: string;
  values: object;
  onAfterChange(value): void;
  getSuggestions(location): void;
  handleSelected(suggestion: object, formikHelpers: object): void;
}

interface State {
  locationId: number | string;
  locationName: string;
}

export class LocationJobs extends React.Component<Props, State> {
  public state: State = {
    locationId: this.props.values[this.props.aliasId],
    locationName: this.props.values[this.props.aliasDisplay],
  };

  onChangeLocationDisplay = (form, newValue) => {
    this.setState({ locationName: (newValue) ? newValue : '' });
    if (this.state.locationId) {
      this.setState({ locationId: 0 });
      form.setFieldValue(this.props.aliasId, 0);
    }
  }

  onBlurLocationDisplay = (form) => {
    if (!this.state.locationId) {
      form.setFieldTouched(this.props.aliasId, true, true);
    }
  }

  onSuggestionSelected = (form, suggestion) => {
    suggestion.id !== this.state.locationId && this.props.handleSelected(suggestion, form);
    this.setState({ locationId: suggestion.id });
    form.setFieldValue(this.props.aliasId, suggestion.id);
  }

  render () {
    return (
      <React.Fragment>
        <Field
          name={this.props.aliasId}
          render={({ field }) => (
            <input
              className="test-file-hide"
              name={field.name}
              value={field.value}
              type="hidden"
            />
          )}
        />
        <Field
          name={this.props.aliasDisplay}
          render={({ field, form }) => (
            <Form.Column isError={(form.touched[this.props.aliasId] &&
              form.errors[this.props.aliasId]) ? true : false}>
              <Form.Control title="Ubicación: " required>
                <Autocomplete
                  onAfterChange={this.props.onAfterChange}
                  onSuggestionSelected={(suggestion) => {
                    this.onSuggestionSelected(form, suggestion);
                  }}
                  customSuggestion={'display'}
                  properties={{
                    name: field.name,
                    value: this.state.locationName,
                    className: form.touched[this.props.aliasId] &&
                      form.errors[this.props.aliasId] && 'is-error',
                    onBlur: () => { this.onBlurLocationDisplay(form); },
                    onChange: (event, { newValue }) => {
                      this.onChangeLocationDisplay(form, newValue);
                    },
                  }}
                  getSuggestions={this.props.getSuggestions}
                />
                <TooltipErrorForm error={form.errors[this.props.aliasId]}
                  touched={form.touched[this.props.aliasId]}
                  value={form.values[this.props.aliasId]}
                  isSubmitting={form.isSubmitting} />
              </Form.Control>
            </Form.Column>
          )}
        />
      </React.Fragment>
    );
  }
}
