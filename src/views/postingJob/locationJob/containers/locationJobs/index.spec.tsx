import * as React from 'react';
import { withFormik } from 'formik';
import { mount } from 'enzyme';
import { LocationJobs } from './index';

describe('Location Jobs component', () => {
  const InnerForm = (props) => {
    return (
      <form>
        <LocationJobs
          aliasId="locationId"
          aliasDisplay="locationDisplay"
          values={props.values}
          onAfterChange={() => {}}
          handleSelected={() => {}}
          getSuggestions={() => {
            return {
              data: {
                data: [
                  { id: 3285, display: 'Demo1' },
                  { id: 3286, display: 'Demo2' },
                  { id: 3287, display: 'Demo3' },
                ],
              },
            };
          }}
        />
        <button type="submit"></button>
      </form>
    );
  };

  const MyForm = withFormik({
    mapPropsToValues: props => ({
      locationId: 12,
      locationDisplay: 'Lorem Ipsum',
    }),
    handleSubmit: (values) => { },
  })(InnerForm);

  describe('Render snapshot', () => {
    describe('Render Row correctly', () => {
      it('Should return 0 if I change <input />', () => {
        const wrapper = mount(<MyForm/>);
        const inputText = wrapper.find('input[name="locationDisplay"]');
        inputText.simulate('change', { target: { value: 'foo' } });
        const inputHidden = wrapper.find('input[name="locationId"]');
        expect(inputHidden.props().value).toBe(0);
      });
    });
  });
});
