import * as React from 'react';
import { shallow } from 'enzyme';
import { LocationJobs } from '@app/src/views/postingJob/locationJob/containers/locationJobs';
import { LocationJobsContainer } from './index';

describe('RequerimentPrograms component test', () => {
  const props = {
    values: {},
    store: {
      formData: {
        requirements: {
          location: [],
        },
      },
    },
    reducers: {
      setUserData: jest.fn((formData) => {}),
    },
  };

  afterEach(() => {
    props.reducers.setUserData.mockClear();
  });

  const wrapper = shallow(<LocationJobsContainer {...props} />);

  test('Correct rendering', () => {
    const actual = wrapper.find(LocationJobs).length;
    expect(actual).toBe(1);
  });

  describe('Method setUserData test', () => {

    test('When a value change is made in the element', () => {
      const suggestion = {
        country_id: 2533,
        country_name: 'Perú',
        department_id: 2456,
        department_name: 'Ancash',
        district_id: 0,
        district_name: '',
        province_id: 2723,
        province_name: 'Huaylas',
      };
      const formikHelpers = {
        setValues: jest.fn((formikValues) => {}),
      };
      const instance = wrapper.instance();
      instance.setUserData(suggestion, formikHelpers);
      expect(props.reducers.setUserData).toBeCalled();
    });

  });

  describe('Method setPlaceOfResidenceData test', () => {

    const location = {
      country_id: 2533,
      country_name: 'Perú',
      department_id: 2456,
      department_name: 'Ancash',
      district_id: 2536,
      district_name: 'Surquillo',
      province_id: 2723,
      province_name: 'Huaylas',
    };

    test('When all location values ​​are correct', () => {
      const instance = wrapper.instance();
      const residenceData = instance.setPlaceOfResidenceData(location);
      const country: object =
        instance.itemBuilder(location.country_id, location.country_name, 'País');
      const department: object =
        instance.itemBuilder(location.department_id, location.department_name, 'Departamento');
      const province: object =
        instance.itemBuilder(location.province_id, location.province_name, 'Provincia');
      const district: object =
        instance.itemBuilder(location.district_id, location.district_name, 'Distrito');
      expect(Array.isArray(residenceData)).toBeTruthy();
      expect(residenceData.length).toBe(4);
      residenceData.map((item: object) => {
        expect(typeof item).toBe('object');
      });
      expect(residenceData[0]).toEqual(country);
      expect(residenceData[1]).toEqual(department);
      expect(residenceData[2]).toEqual(province);
      expect(residenceData[3]).toEqual(district);
    });

    test('When a location element contains the value of 0', () => {
      location['district_id'] = 0;
      const instance = wrapper.instance();
      const residenceData = instance.setPlaceOfResidenceData(location);
      expect(Array.isArray(residenceData)).toBeTruthy();
      expect(residenceData.length).toBe(3);
      residenceData.map((item: object) => {
        expect(typeof item).toBe('object');
        expect(item['district_id']).toBe(undefined);
        expect(item['district_name']).toBe(undefined);
      });
    });

    test('When a location element contains the empty value', () => {
      location['district_name'] = '';
      const instance = wrapper.instance();
      const residenceData = instance.setPlaceOfResidenceData(location);
      expect(Array.isArray(residenceData)).toBeTruthy();
      expect(residenceData.length).toBe(3);
      residenceData.map((item: object) => {
        expect(typeof item).toBe('object');
        expect(item['district_id']).toBe(undefined);
        expect(item['district_name']).toBe(undefined);
      });
    });

  });

  describe('Method itemBuilder test', () => {

    test('When a new element is created', () => {
      const instance = wrapper.instance();
      const itemLocation = instance.itemBuilder(1234, 'Perú', 'País');
      expect(typeof itemLocation).toBe('object');
      expect(itemLocation).toEqual({ value: 1234, label: 'Perú (País)' });
    });

  });

  describe('Method cleanPlaceOfResidence test', () => {

    test('When a value change is made in the element', () => {
      const instance = wrapper.instance();
      const formikHelpers = {
        setValues: jest.fn((formikValues) => {}),
      };
      instance.cleanPlaceOfResidence(formikHelpers);
      expect(formikHelpers.setValues).toBeCalled();
    });

  });

});
