import * as React from 'react';
import { locationName } from '@app/src/views/postingJob/locationJob/state/services';
import { LocationJobs } from '@app/src/views/postingJob/locationJob/containers/locationJobs';
import { withPostingContext } from '@app/src/views/postingJob/formPosting/state/store';

interface Props {
  values: object;
  store: {
    formData: object;
  };
  reducers: {
    setUserData(formData: object): void;
  };
}

export class LocationJobsContainer extends React.Component<Props, {}> {

  onAfterChange = (value: string) => {
    if (value === '') {
      locationName.cancelRequest();
    }
  }

  getSuggestions = (location: string) => {
    return locationName.getLocation(location);
  }

  setUserData = (suggestion: object, formikHelpers: object) => {
    const formData = { ...this.props.store.formData };
    formData['requirements']['location'] = this.setPlaceOfResidenceData(suggestion);
    this.props.reducers.setUserData(formData);
    this.cleanPlaceOfResidence(formikHelpers);
  }

  setPlaceOfResidenceData(location: object): object[] {
    const country =
      this.itemBuilder(location['country_id'], location['country_name'], 'País');
    const department =
      this.itemBuilder(location['department_id'], location['department_name'], 'Departamento');
    const province =
      this.itemBuilder(location['province_id'], location['province_name'], 'Provincia');
    const district =
      this.itemBuilder(location['district_id'], location['district_name'], 'Distrito');
    const formDataLocation = [country, department, province, district];
    return formDataLocation.filter(item => item['value'] !== 0 && item['label'] !== '');
  }

  itemBuilder(value: number, label: string, labelText: string): object {
    return  { value, label: `${label} (${labelText})` };
  }

  cleanPlaceOfResidence(formikHelpers) {
    const formikValues = { ...this.props.values };
    formikValues['residenceId'] = 0;
    formikValues['residenceIsExcluding'] = false;
    formikHelpers.setValues(formikValues);
  }

  render(): JSX.Element {
    return (
      <LocationJobs
        aliasId="locationId"
        aliasDisplay="locationDisplay"
        values={this.props.values}
        onAfterChange={this.onAfterChange}
        getSuggestions={this.getSuggestions}
        handleSelected={this.setUserData}
      />
    );
  }
}

export const LocationJobsContainerEnhanced = withPostingContext(LocationJobsContainer);
