import * as React from 'react';
import { Accordion } from 'aptitus-components/Components/Accordion';
import { LocationJobsContainerEnhanced }
  from '@app/src/views/postingJob/locationJob/containers/locationJobsContainer';
import { Form } from 'aptitus-components/Components/Form';
import { Title } from '@app/src/views/common/styledCommon';

export const SectionLocation: React.SFC<any> = ({ values }) => (
  <Accordion statePanel={true}
    arrowPositionLeft={true}
    header={<Title className="b-process-info_title">Ubicación del Puesto</Title>}
    body={
      <div className="g-form g-form--posting">
        <Form.Wrapper type="normal">
          <Form.Row><LocationJobsContainerEnhanced values={values} /></Form.Row>
        </Form.Wrapper>
      </div>
    }
  />
);
