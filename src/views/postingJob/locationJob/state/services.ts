import axios from 'axios';

let cancelToken = null;
let source = null;

export const locationName = {
  async getLocation(values) {
    try {
      if (source) {
        source.cancel();
      }
      cancelToken = axios.CancelToken;
      source = cancelToken.source();
      const data = {};
      const params = {
        q: values,
      };
      const response = await axios
        .get(`${process.env.APP_URL}/empresa/publica-aviso-web/search-location-ajax`, {
          params,
          cancelToken: source.token,
        });
      data['data'] = response.data;
      return data;
    } catch (e) {
      throw new Error();
    }
  },

  cancelRequest() {
    return source.cancel();
  },
};
